Name:       qtmir
Version:    1.0
Release:    3%{?dist}
Summary:    Qt plugin for Unity specific Mir APIs
License:    FIXME
URL:        https://github.com/ubports/qtmir/
Source0:    https://github.com/ubports/qtmir/archive/1194eeb622b710c79175c29b634f0f99a20f1592/qtmir.tar.gz
Patch0:     0001-moc.patch
Patch1:     0002-nowerror.patch
Patch2:     0003-Revert-Revert-Remove-need-for-cgmanager-we-got-no-ne.patch
Patch3:     0004-maliit-service.patch.patch
Patch4:     0005-replace-ubuntu-app-launch-with-lomiri.patch
Patch5:     0006-fix-odd-compilation-errors.patch

%ifarch aarch64
BuildRequires: mir-devel = 1.8.0
%endif
%ifarch x86_64
BuildRequires: mir-devel
%endif
BuildRequires: boost-devel
BuildRequires: gsettings-qt-devel = 1.0
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: glm-devel
BuildRequires: liblomiri-api2-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: liblomiri-qt-dbusmock-devel
BuildRequires: liblomiri-url-dispatcher-devel
BuildRequires: lomiri-cmake-extras
BuildRequires: lomiri-content-hub-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtbase-static
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: qt5-qtsensors-devel
BuildRequires: systemtap-sdt-devel
BuildRequires: pkgconfig(xkbcommon)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(process-cpp)
BuildRequires: pkgconfig(lomiri-app-launch-0)
BuildRequires: pkgconfig(url-dispatcher-1)
BuildRequires: pkgconfig(egl)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(lttng-ust)
BuildRequires: pkgconfig(libqtdbustest-1)
BuildRequires: pkgconfig(libqtdbusmock-1)
BuildRequires: pkgconfig(valgrind)
BuildRequires: pkgconfig(wayland-client)
BuildRequires: pkgconfig(wayland-server)
BuildRequires: pkgconfig(gtest)
BuildRequires: pkgconfig(fontconfig)
BuildRequires: pkgconfig(unity-shell-application)

%description
Qt plugin for Unity specific Mir APIs
QtMir provides Qt/QML bindings for Mir features that are exposed through the
qtmir-desktop or qtmir-android QPA plugin such as Application management
(start/stop/suspend/resume) and surface management.
Qt plugin for Unity specific Mir APIs
QtMir provides Qt/QML bindings for Mir features that are exposed through the
qtmir-desktop or qtmir-android QPA plugin such as Application management
(start/stop/suspend/resume) and surface management.
Qt platform abstraction (QPA) plugin for a Mir server (desktop)
QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
a Mir server. It also exposes some internal Mir functionality.
.
This variant of the package is for GNU-based desktops.
Qt platform abstraction (QPA) plugin for a Mir server (desktop)
QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
a Mir server. It also exposes some internal Mir functionality.
.
This variant of the package is for GNU-based desktops.

%prep
%autosetup -p1 -n qtmir-1194eeb622b710c79175c29b634f0f99a20f1592

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_INSTALL_PREFIX:PATH=/usr .. ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
cd build
%make_install
cd ..

%files
%{_datadir}/applications/*
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/qtmir/qtmir-demo-shell/*
%{_datadir}/qtmir/benchmarks/*
%{_datadir}/qtmir/qtmir-demo-client/*
%{_bindir}/qtmir-demo-shell
%{_bindir}/qtmir-demo-client
%{_libdir}/qt5/plugins/platforms/libqpa-mirserver.so
%{_libdir}/qt5/qml/Unity/Application/libunityapplicationplugin.so
%{_libdir}/qt5/qml/Unity/Application/qmldir
%{_libdir}/qt5/qml/Unity/Screens/libunityscreensplugin.so
%{_libdir}/qt5/qml/Unity/Screens/qmldir
