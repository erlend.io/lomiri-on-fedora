From b523efe5f4ace35e50ec241fb7caef6d2916417a Mon Sep 17 00:00:00 2001
From: "erlend.io" <me@erlend.io>
Date: Mon, 25 Jan 2021 04:20:20 -0500
Subject: [PATCH 5/6] replace ubuntu-app-launch with lomiri

---
 CMakeLists.txt                                |  2 +-
 .../Application/upstart/applicationinfo.cpp   |  4 +-
 .../Application/upstart/applicationinfo.h     |  6 +-
 .../Application/upstart/taskcontroller.cpp    | 58 +++++++++----------
 4 files changed, 35 insertions(+), 35 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index c7d2dc4b..2b523854 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -83,7 +83,7 @@ pkg_check_modules(MIRAL miral>=1.1.0 REQUIRED)
 pkg_check_modules(XKBCOMMON xkbcommon REQUIRED)
 pkg_check_modules(GLIB glib-2.0 REQUIRED)
 pkg_check_modules(PROCESS_CPP process-cpp REQUIRED)
-pkg_check_modules(UBUNTU_APP_LAUNCH ubuntu-app-launch-2 REQUIRED)
+pkg_check_modules(UBUNTU_APP_LAUNCH lomiri-app-launch-0 REQUIRED)
 pkg_check_modules(URL_DISPATCHER url-dispatcher-1)
 pkg_check_modules(EGL egl)
 pkg_check_modules(GIO gio-2.0)
diff --git a/src/modules/Unity/Application/upstart/applicationinfo.cpp b/src/modules/Unity/Application/upstart/applicationinfo.cpp
index b3c466d6..0bb61c2e 100644
--- a/src/modules/Unity/Application/upstart/applicationinfo.cpp
+++ b/src/modules/Unity/Application/upstart/applicationinfo.cpp
@@ -22,7 +22,7 @@ namespace qtmir
 namespace upstart
 {
 
-ApplicationInfo::ApplicationInfo(const QString &appId, std::shared_ptr<ubuntu::app_launch::Application::Info> info)
+ApplicationInfo::ApplicationInfo(const QString &appId, std::shared_ptr<lomiri::app_launch::Application::Info> info)
     : qtmir::ApplicationInfo(),
       m_appId(appId),
       m_info(info)
@@ -102,7 +102,7 @@ bool ApplicationInfo::rotatesWindowContents() const
 
 bool ApplicationInfo::isTouchApp() const
 {
-    return m_info->supportsUbuntuLifecycle().value();
+    return m_info->supportsLomiriLifecycle().value();
 }
 
 } // namespace upstart
diff --git a/src/modules/Unity/Application/upstart/applicationinfo.h b/src/modules/Unity/Application/upstart/applicationinfo.h
index 54320512..fe15b114 100644
--- a/src/modules/Unity/Application/upstart/applicationinfo.h
+++ b/src/modules/Unity/Application/upstart/applicationinfo.h
@@ -20,7 +20,7 @@
 
 #include "../applicationinfo.h"
 
-#include <ubuntu-app-launch/application.h>
+#include <lomiri-app-launch/application.h>
 
 namespace qtmir
 {
@@ -30,7 +30,7 @@ namespace upstart
 class ApplicationInfo : public qtmir::ApplicationInfo
 {
 public:
-    ApplicationInfo(const QString &appId, std::shared_ptr<ubuntu::app_launch::Application::Info> info);
+    ApplicationInfo(const QString &appId, std::shared_ptr<lomiri::app_launch::Application::Info> info);
 
     QString appId() const override;
     QString name() const override;
@@ -49,7 +49,7 @@ public:
 
 private:
     QString m_appId;
-    std::shared_ptr<ubuntu::app_launch::Application::Info> m_info;
+    std::shared_ptr<lomiri::app_launch::Application::Info> m_info;
 };
 
 } // namespace upstart
diff --git a/src/modules/Unity/Application/upstart/taskcontroller.cpp b/src/modules/Unity/Application/upstart/taskcontroller.cpp
index bafbc057..75e38540 100644
--- a/src/modules/Unity/Application/upstart/taskcontroller.cpp
+++ b/src/modules/Unity/Application/upstart/taskcontroller.cpp
@@ -26,11 +26,11 @@
 
 // upstart
 extern "C" {
-    #include "ubuntu-app-launch.h"
+    #include "lomiri-app-launch.h"
 }
-#include <ubuntu-app-launch/registry.h>
+#include <lomiri-app-launch/registry.h>
 
-namespace ual = ubuntu::app_launch;
+namespace ual = lomiri::app_launch;
 
 namespace qtmir
 {
@@ -40,13 +40,13 @@ namespace upstart
 struct TaskController::Private
 {
     std::shared_ptr<ual::Registry> registry;
-    UbuntuAppLaunchAppObserver preStartCallback = nullptr;
-    UbuntuAppLaunchAppObserver startedCallback = nullptr;
-    UbuntuAppLaunchAppObserver stopCallback = nullptr;
-    UbuntuAppLaunchAppObserver focusCallback = nullptr;
-    UbuntuAppLaunchAppObserver resumeCallback = nullptr;
-    UbuntuAppLaunchAppPausedResumedObserver pausedCallback = nullptr;
-    UbuntuAppLaunchAppFailedObserver failureCallback = nullptr;
+    LomiriAppLaunchAppObserver preStartCallback = nullptr;
+    LomiriAppLaunchAppObserver startedCallback = nullptr;
+    LomiriAppLaunchAppObserver stopCallback = nullptr;
+    LomiriAppLaunchAppObserver focusCallback = nullptr;
+    LomiriAppLaunchAppObserver resumeCallback = nullptr;
+    LomiriAppLaunchAppPausedResumedObserver pausedCallback = nullptr;
+    LomiriAppLaunchAppFailedObserver failureCallback = nullptr;
 };
 
 namespace {
@@ -57,7 +57,7 @@ namespace {
  */
 QString toShortAppIdIfPossible(const QString &appId) {
     gchar *package, *application;
-    if (ubuntu_app_launch_app_id_parse(appId.toLatin1().constData(), &package, &application, nullptr)) {
+    if (lomiri_app_launch_app_id_parse(appId.toLatin1().constData(), &package, &application, nullptr)) {
         // is long appId, so assemble its short appId
         QString shortAppId = QStringLiteral("%1_%2").arg(package, application);
         g_free(package);
@@ -123,36 +123,36 @@ TaskController::TaskController()
         Q_EMIT(thiz->processSuspended(toShortAppIdIfPossible(appId)));
     };
 
-    impl->failureCallback = [](const gchar * appId, UbuntuAppLaunchAppFailed failureType, gpointer userData) {
+    impl->failureCallback = [](const gchar * appId, LomiriAppLaunchAppFailed failureType, gpointer userData) {
         TaskController::Error error;
         switch(failureType)
         {
-        case UBUNTU_APP_LAUNCH_APP_FAILED_CRASH: error = TaskController::Error::APPLICATION_CRASHED;                    break;
-        case UBUNTU_APP_LAUNCH_APP_FAILED_START_FAILURE: error = TaskController::Error::APPLICATION_FAILED_TO_START;    break;
+        case LOMIRI_APP_LAUNCH_APP_FAILED_CRASH: error = TaskController::Error::APPLICATION_CRASHED;                    break;
+        case LOMIRI_APP_LAUNCH_APP_FAILED_START_FAILURE: error = TaskController::Error::APPLICATION_FAILED_TO_START;    break;
         }
 
         auto thiz = static_cast<TaskController*>(userData);
         Q_EMIT(thiz->processFailed(toShortAppIdIfPossible(appId), error));
     };
 
-    ubuntu_app_launch_observer_add_app_starting(impl->preStartCallback, this);
-    ubuntu_app_launch_observer_add_app_started(impl->startedCallback, this);
-    ubuntu_app_launch_observer_add_app_stop(impl->stopCallback, this);
-    ubuntu_app_launch_observer_add_app_focus(impl->focusCallback, this);
-    ubuntu_app_launch_observer_add_app_resume(impl->resumeCallback, this);
-    ubuntu_app_launch_observer_add_app_paused(impl->pausedCallback, this);
-    ubuntu_app_launch_observer_add_app_failed(impl->failureCallback, this);
+    lomiri_app_launch_observer_add_app_starting(impl->preStartCallback, this);
+    lomiri_app_launch_observer_add_app_started(impl->startedCallback, this);
+    lomiri_app_launch_observer_add_app_stop(impl->stopCallback, this);
+    lomiri_app_launch_observer_add_app_focus(impl->focusCallback, this);
+    lomiri_app_launch_observer_add_app_resume(impl->resumeCallback, this);
+    lomiri_app_launch_observer_add_app_paused(impl->pausedCallback, this);
+    lomiri_app_launch_observer_add_app_failed(impl->failureCallback, this);
 }
 
 TaskController::~TaskController()
 {
-    ubuntu_app_launch_observer_delete_app_starting(impl->preStartCallback, this);
-    ubuntu_app_launch_observer_delete_app_started(impl->startedCallback, this);
-    ubuntu_app_launch_observer_delete_app_stop(impl->stopCallback, this);
-    ubuntu_app_launch_observer_delete_app_focus(impl->focusCallback, this);
-    ubuntu_app_launch_observer_delete_app_resume(impl->resumeCallback, this);
-    ubuntu_app_launch_observer_delete_app_paused(impl->pausedCallback, this);
-    ubuntu_app_launch_observer_delete_app_failed(impl->failureCallback, this);
+    lomiri_app_launch_observer_delete_app_starting(impl->preStartCallback, this);
+    lomiri_app_launch_observer_delete_app_started(impl->startedCallback, this);
+    lomiri_app_launch_observer_delete_app_stop(impl->stopCallback, this);
+    lomiri_app_launch_observer_delete_app_focus(impl->focusCallback, this);
+    lomiri_app_launch_observer_delete_app_resume(impl->resumeCallback, this);
+    lomiri_app_launch_observer_delete_app_paused(impl->pausedCallback, this);
+    lomiri_app_launch_observer_delete_app_failed(impl->failureCallback, this);
 }
 
 bool TaskController::appIdHasProcessId(const QString& appId, pid_t pid)
@@ -192,7 +192,7 @@ bool TaskController::start(const QString& appId, const QStringList& arguments)
         return false;
     }
 
-    // Convert arguments QStringList into format suitable for ubuntu-app-launch
+    // Convert arguments QStringList into format suitable for lomiri-app-launch
     std::vector<ual::Application::URL> urls;
     for (auto &arg: arguments) {
         urls.emplace_back(ual::Application::URL::from_raw(arg.toStdString()));
-- 
2.26.2

