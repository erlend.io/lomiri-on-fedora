#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial_-_edge_-_wayland https://github.com/ubports/unity8.git
cd unity8
cat $SRC/0001-remove-dependency-to-system-settings.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "remove-dependency-to-system-settings"
cat $SRC/0002-remove-dependency-to-libhybris.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "remove-dependency-to-libhybris"
cat $SRC/0003-removing-unused-hopefully-dependency.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removing-unused-hopefully-dependency"
cat $SRC/0004-fixed-path-to-qmlplugin-dump.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixed-path-to-qmlplugin-dump"
cat $SRC/0005-added-path-to-qdbusxml2cpp.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "added-path-to-qdbusxml2cpp"
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/gsettings-ubuntu-touch-schemas.git
cd gsettings-ubuntu-touch-schemas
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/unity-notifications.git
cd unity-notifications
