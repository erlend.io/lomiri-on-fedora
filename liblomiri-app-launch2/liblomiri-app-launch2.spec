Name:       liblomiri-app-launch2
Version:    1.0
Release:    2%{?dist}
Summary:    library for sending requests to the ubuntu app launch
License:    FIXME
URL:        https://gitlab.com/ubports/core/lomiri-app-launch
Source0:    https://gitlab.com/ubports/core/lomiri-app-launch/-/archive/f9e1931855302c5ddb1fef16d9b79d9cb7cdf020/lomiri-app-launch.tar.gz
Patch0:     0001-removing-external-c.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gobject-introspection-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-dbus-test-devel
BuildRequires: liblomiri-libertine-devel
BuildRequires: lomiri-cmake-extras
BuildRequires: systemtap-sdt-devel
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(json-glib-1.0)
BuildRequires: pkgconfig(zeitgeist-2.0)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(dbustest-1)
BuildRequires: pkgconfig(lttng-ust)
BuildRequires: pkgconfig(mirclient)
BuildRequires: pkgconfig(libertine)
BuildRequires: pkgconfig(libcurl)
BuildRequires: pkgconfig(liblomiri-api)

%description
library for sending requests to the ubuntu app launch
Upstart Job file and associated utilities that is used to launch
applications in a standard and confined way.
.
This package contains shared libraries to be used by applications.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n lomiri-app-launch-f9e1931855302c5ddb1fef16d9b79d9cb7cdf020
sed -i 's/\-Werror//' CMakeLists.txt

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -Denable_tests=OFF ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
cd build
%make_install
cd ..

%files
%{_datadir}/applications/*
%{_datadir}/lomiri-app-launch/*
%{_datadir}/icons/hicolor/scalable/apps/*
%{_datadir}/gir-1.0/*
%{_bindir}/lomiri-app-list-pids
%{_bindir}/lomiri-helper-stop
%{_bindir}/lomiri-app-pid
%{_bindir}/lomiri-helper-start
%{_bindir}/lomiri-app-stop
%{_bindir}/lomiri-app-info
%{_bindir}/lomiri-app-usage
%{_bindir}/lomiri-app-list
%{_bindir}/lomiri-app-triplet
%{_bindir}/lomiri-app-launch
%{_bindir}/lomiri-app-watch
%{_bindir}/lomiri-app-launch-appids
%{_bindir}/lomiri-helper-list
%{_bindir}/app-test/lomiri-app-test
%{_libdir}/liblomiri-app-launch.so.0
%{_libdir}/liblomiri-app-launch.so.0.0.0
%{_libdir}/girepository-1.0/LomiriAppLaunch-3.typelib
%{_libexecdir}/lomiri-app-launch/socket-demangler
%{_libexecdir}/lomiri-app-launch/oom-adjust-setuid-helper
%{_libexecdir}/lomiri-app-launch/systemd-helper-helper

%files devel
%{_libdir}/liblomiri-app-launch.so
%{_libdir}/pkgconfig/lomiri-app-launch-0.pc
%{_includedir}/liblomiri-app-launch-0/*
