Name:       liblomiri-download-manager
Version:    1.0
Release:    3%{?dist}
Summary:    Upload Download Manager - shared common library
License:    FIXME
URL:        https://github.com/ubports/ubuntu-download-manager
Source0:    https://github.com/ubports/ubuntu-download-manager/archive/6d1d305da3f6e9c3bfba278fb7268b80c48ef7fd/ubuntu-download-manager.tar.gz
Patch0:     0001-diable-tests.patch
Patch1:     0002-disabling-rpath.patch

BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtbase-static
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(libglog)
BuildRequires: pkgconfig(libnih-dbus)

%description
Upload Download Manager - shared common library
Upload Download Manager performs uploads and downloads from a centralized
location.
.
This package includes the common shared library between the client lib and the
service lib.
Ubuntu Download Manager - shared common library
Ubuntu Download Manager performs downloads from a centralized location.
.
This package includes the common shared library between the client lib and the
service lib.
Ubuntu Download Manager - shared public library
Ubuntu Download Manager performs downloads from a centralized location.
.
This package includes the public shared library.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n ubuntu-download-manager-6d1d305da3f6e9c3bfba278fb7268b80c48ef7fd
sed -i 's/\-Werror//' CMakeLists.txt

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/ubuntu/download_manager
mkdir -p %{buildroot}%{_includedir}/ubuntu/transfers/errors
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp src/downloads/client/ubuntu/download_manager/download.h   %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/common/ubuntu/download_manager/download_state_struct.h %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/common/ubuntu/download_manager/download_struct.h %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/client/ubuntu/download_manager/downloads_list.h %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/client/ubuntu/download_manager/error.h      %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/client/ubuntu/download_manager/group_download.h %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/common/ubuntu/download_manager/group_download_struct.h %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/client/ubuntu/download_manager/manager.h    %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/downloads/common/ubuntu/download_manager/metatypes.h  %{buildroot}%{_includedir}/ubuntu/download_manager
cp src/common/public/ubuntu/transfers/errors/auth_error_struct.h %{buildroot}%{_includedir}/ubuntu/transfers/errors
cp src/common/public/ubuntu/transfers/errors/hash_error_struct.h %{buildroot}%{_includedir}/ubuntu/transfers/errors
cp src/common/public/ubuntu/transfers/errors/http_error_struct.h %{buildroot}%{_includedir}/ubuntu/transfers/errors
cp src/common/public/ubuntu/transfers/errors/network_error_struct.h %{buildroot}%{_includedir}/ubuntu/transfers/errors
cp src/common/public/ubuntu/transfers/errors/process_error_struct.h %{buildroot}%{_includedir}/ubuntu/transfers/errors
cp src/common/public/ubuntu/transfers/metadata.h             %{buildroot}%{_includedir}/ubuntu/transfers
cp src/common/public/ubuntu/transfers/visibility.h           %{buildroot}%{_includedir}/ubuntu/transfers
ln -s libubuntu-download-manager-client.so.1 %{buildroot}%{_libdir}/libubuntu-download-manager-client.so
ln -s libubuntu-download-manager-client.so.1.2.0 %{buildroot}%{_libdir}/libubuntu-download-manager-client.so.1
install -p -m 755 build/src/downloads/client/libubuntu-download-manager-client.so.1.2.0 %{buildroot}%{_libdir}
ln -s libubuntu-download-manager-common.so.1 %{buildroot}%{_libdir}/libubuntu-download-manager-common.so
ln -s libubuntu-download-manager-common.so.1.2.0 %{buildroot}%{_libdir}/libubuntu-download-manager-common.so.1
install -p -m 755 build/src/downloads/common/libubuntu-download-manager-common.so.1.2.0 %{buildroot}%{_libdir}
ln -s libudm-common.so.1 %{buildroot}%{_libdir}/libudm-common.so
ln -s libudm-common.so.1.2.0 %{buildroot}%{_libdir}/libudm-common.so.1
install -p -m 755 build/src/common/public/libudm-common.so.1.2.0 %{buildroot}%{_libdir}
cp build/src/downloads/client/ubuntu-download-manager-client.pc %{buildroot}%{_libdir}/pkgconfig
cp build/src/downloads/common/ubuntu-download-manager-common.pc %{buildroot}%{_libdir}/pkgconfig
cp build/src/common/public/udm-common.pc                     %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/libubuntu-download-manager-client.so.1
%{_libdir}/libubuntu-download-manager-client.so.1.2.0
%{_libdir}/libubuntu-download-manager-common.so.1
%{_libdir}/libubuntu-download-manager-common.so.1.2.0
%{_libdir}/libudm-common.so.1
%{_libdir}/libudm-common.so.1.2.0

%files devel
%{_includedir}/ubuntu/download_manager/download.h
%{_includedir}/ubuntu/download_manager/download_state_struct.h
%{_includedir}/ubuntu/download_manager/download_struct.h
%{_includedir}/ubuntu/download_manager/downloads_list.h
%{_includedir}/ubuntu/download_manager/error.h
%{_includedir}/ubuntu/download_manager/group_download.h
%{_includedir}/ubuntu/download_manager/group_download_struct.h
%{_includedir}/ubuntu/download_manager/manager.h
%{_includedir}/ubuntu/download_manager/metatypes.h
%{_includedir}/ubuntu/transfers/errors/auth_error_struct.h
%{_includedir}/ubuntu/transfers/errors/hash_error_struct.h
%{_includedir}/ubuntu/transfers/errors/http_error_struct.h
%{_includedir}/ubuntu/transfers/errors/network_error_struct.h
%{_includedir}/ubuntu/transfers/errors/process_error_struct.h
%{_includedir}/ubuntu/transfers/metadata.h
%{_includedir}/ubuntu/transfers/visibility.h
%{_libdir}/libubuntu-download-manager-client.so
%{_libdir}/libubuntu-download-manager-common.so
%{_libdir}/libudm-common.so
%{_libdir}/pkgconfig/ubuntu-download-manager-client.pc
%{_libdir}/pkgconfig/ubuntu-download-manager-common.pc
%{_libdir}/pkgconfig/udm-common.pc
