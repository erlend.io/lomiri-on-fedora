Name:       liblomiri-telefon-service
Version:    1.0
Release:    1%{?dist}
Summary:    Online account plugin for unity8
License:    FIXME
URL:        https://github.com/ubports/telephony-service
Source0:    https://github.com/ubports/telephony-service/archive/cf811709f8e2dce6eecf142055acab84bbf61ca6/telephony-service.tar.gz
Patch0:     0001-disabled-tests.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gettext-devel
BuildRequires: libnotify-devel
BuildRequires: libphonenumber-devel
BuildRequires: protobuf-devel
BuildRequires: qt5-mobility-feedback-devel
BuildRequires: qt5-mobility-pim-devel
BuildRequires: qt5-qtmultimedia-devel
BuildRequires: telepathy-qt5-devel
BuildRequires: pkgconfig(TelepathyQt5)
BuildRequires: pkgconfig(TelepathyQt5Farstream)
BuildRequires: pkgconfig(libnotify)
BuildRequires: pkgconfig(telepathy-farstream)
BuildRequires: pkgconfig(gstreamer-1.0)
BuildRequires: pkgconfig(farstream-0.2)
BuildRequires: pkgconfig(libpulse)

%description
Online account plugin for unity8
Online account plugin for unity8.
.
This package contains the online account plugin providing sip account.
Online account plugin for unity8
Online account plugin for unity8.
.
This package contains the online account plugin providing irc account.
Telephony service components for Ubuntu - QML plugin
Telephony PhoneNumber components for Ubuntu.
.
This package contains the QML plugin providing the features from the telephony
PhoneNumber to applications.
Telephony service components for Ubuntu - QML plugin
Telephony service components for Ubuntu.
.
This package contains the QML plugin providing the features from the telephony
service to applications.

%prep
%autosetup -p1 -n telephony-service-cf811709f8e2dce6eecf142055acab84bbf61ca6

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DWANT_UI_SERVICES=OFF ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber
mkdir -p %{buildroot}/usr/share/accounts/providers
mkdir -p %{buildroot}/usr/share/accounts/qml-plugins/telephony-irc
mkdir -p %{buildroot}/usr/share/accounts/qml-plugins/telephony-sip
mkdir -p %{buildroot}/usr/share/accounts/services
cp build/Ubuntu/Telephony/PhoneNumber/PhoneNumber.js         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber
cp build/Ubuntu/Telephony/PhoneNumber/PhoneNumberField.qml   %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber
cp build/Ubuntu/Telephony/PhoneNumber/PhoneNumberInput.qml   %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber
install -p -m 755 build/Ubuntu/Telephony/PhoneNumber/libtelephonyservice-phonenumber-qml.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber
cp build/Ubuntu/Telephony/PhoneNumber/qmldir                 %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber
install -p -m 755 build/Ubuntu/Telephony/libtelephonyservice-qml.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony
cp build/Ubuntu/Telephony/qmldir                             %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Telephony
cp accounts/irc/data/telephony-irc.provider                  %{buildroot}/usr/share/accounts/providers
cp accounts/sip/data/telephony-sip.provider                  %{buildroot}/usr/share/accounts/providers
cp accounts/sip/qml/AccountInfo.qml                          %{buildroot}/usr/share/accounts/qml-plugins/telephony-irc
cp accounts/common/DynamicField.qml                          %{buildroot}/usr/share/accounts/qml-plugins/telephony-irc
cp accounts/common/Main.qml                                  %{buildroot}/usr/share/accounts/qml-plugins/telephony-irc
cp accounts/irc/qml/NewAccount.qml                           %{buildroot}/usr/share/accounts/qml-plugins/telephony-irc
cp accounts/common/NewAccountInterface.qml                   %{buildroot}/usr/share/accounts/qml-plugins/telephony-irc
cp accounts/sip/qml/AccountInfo.qml                          %{buildroot}/usr/share/accounts/qml-plugins/telephony-sip
cp accounts/common/DynamicField.qml                          %{buildroot}/usr/share/accounts/qml-plugins/telephony-sip
cp accounts/common/Main.qml                                  %{buildroot}/usr/share/accounts/qml-plugins/telephony-sip
cp accounts/sip/qml/NewAccount.qml                           %{buildroot}/usr/share/accounts/qml-plugins/telephony-sip
cp accounts/common/NewAccountInterface.qml                   %{buildroot}/usr/share/accounts/qml-plugins/telephony-sip
cp accounts/irc/data/telephony-irc-im.service                %{buildroot}/usr/share/accounts/services
cp accounts/sip/data/telephony-sip-im.service                %{buildroot}/usr/share/accounts/services

%files
%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber/PhoneNumber.js
%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber/PhoneNumberField.qml
%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber/PhoneNumberInput.qml
%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber/libtelephonyservice-phonenumber-qml.so
%{_libdir}/qt5/qml/Ubuntu/Telephony/PhoneNumber/qmldir
%{_libdir}/qt5/qml/Ubuntu/Telephony/libtelephonyservice-qml.so
%{_libdir}/qt5/qml/Ubuntu/Telephony/qmldir
/usr/share/accounts/providers/telephony-irc.provider
/usr/share/accounts/providers/telephony-sip.provider
/usr/share/accounts/qml-plugins/telephony-irc/AccountInfo.qml
/usr/share/accounts/qml-plugins/telephony-irc/DynamicField.qml
/usr/share/accounts/qml-plugins/telephony-irc/Main.qml
/usr/share/accounts/qml-plugins/telephony-irc/NewAccount.qml
/usr/share/accounts/qml-plugins/telephony-irc/NewAccountInterface.qml
/usr/share/accounts/qml-plugins/telephony-sip/AccountInfo.qml
/usr/share/accounts/qml-plugins/telephony-sip/DynamicField.qml
/usr/share/accounts/qml-plugins/telephony-sip/Main.qml
/usr/share/accounts/qml-plugins/telephony-sip/NewAccount.qml
/usr/share/accounts/qml-plugins/telephony-sip/NewAccountInterface.qml
/usr/share/accounts/services/telephony-irc-im.service
/usr/share/accounts/services/telephony-sip-im.service
