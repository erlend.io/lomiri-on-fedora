#!/bin/bash

# exit when any command fails
set -e

# Source setup
source ../settings/env

echo ---------------------------------------------------------------
echo Building : mir
echo ---------------------------------------------------------------

cp *.patch $RPMBUILD_DIR/SOURCES
spectool -g -R mir.spec
rpmbuild -bs mir.spec
mock -r fedora-33-aarch64 --rebuild --no-clean $RPMBUILD_DIR/SRPMS/mir-1.8.0-1ubports.fc32.src.rpm
cp $RESULTDIR/*.src.rpm     $RPMBUILD_DIR/SRPMS/
cp $RESULTDIR/*.aarch64.rpm $RPMBUILD_DIR/RPMS/aarch64
