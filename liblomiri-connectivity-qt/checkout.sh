#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/indicator-network.git
cd indicator-network
cat $SRC/0001-adding-enable-coverage-report.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-enable-coverage-report"
cat $SRC/0002-find-correct-link-to-network-manager.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "find-correct-link-to-network-manager"
cat $SRC/0003-fix-missing-include-file-functional.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fix-missing-include-file-functional"
cat $SRC/0004-add-missing-cmake-file.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "add-missing-cmake-file"
cat $SRC/0005-add-custom-cmake-directory.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "add-custom-cmake-directory"
cat $SRC/0006-add-missing-Intltool.cmake.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "add-missing-Intltool.cmake"
cat $SRC/0007-add-missing-QmlPluginsConfig.cmake.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "add-missing-QmlPluginsConfig.cmake"
cat $SRC/0008-adding-UseDoxygen.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-UseDoxygen"
cat $SRC/0009-adding-UseGSettings.cmake.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-UseGSettings.cmake"
cat $SRC/0010-disabling-compilation-of-indicator-for-now.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "disabling-compilation-of-indicator-for-now"
cat $SRC/0011-fixed-linker-error-caused-by-constexpr.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixed-linker-error-caused-by-constexpr"
cat $SRC/0012-fixed-path-to-qmlplugindump.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixed-path-to-qmlplugindump"
cat $SRC/0013-removing-ofono-temporary.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removing-ofono-temporary"
