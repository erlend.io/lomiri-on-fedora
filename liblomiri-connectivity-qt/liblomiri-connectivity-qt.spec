Name:       liblomiri-connectivity-qt
Version:    1.0
Release:    1%{?dist}
Summary:    Ubuntu Connectivity Qt API
License:    FIXME
URL:        https://github.com/ubports/indicator-network
Source0:    https://github.com/ubports/indicator-network/archive/4f220221965793ad69dcdb4d568012816d1f3e32/indicator-network.tar.gz
Patch0:     0001-adding-enable-coverage-report.patch
Patch1:     0002-find-correct-link-to-network-manager.patch
Patch2:     0003-fix-missing-include-file-functional.patch
Patch3:     0004-add-missing-cmake-file.patch
Patch4:     0005-add-custom-cmake-directory.patch
Patch5:     0006-add-missing-Intltool.cmake.patch
Patch6:     0007-add-missing-QmlPluginsConfig.cmake.patch
Patch7:     0008-adding-UseDoxygen.patch
Patch8:     0009-adding-UseGSettings.cmake.patch
Patch9:     0010-disabling-compilation-of-indicator-for-now.patch
Patch10:     0011-fixed-linker-error-caused-by-constexpr.patch
Patch11:     0012-fixed-path-to-qmlplugindump.patch
Patch12:     0013-removing-ofono-temporary.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gcovr
BuildRequires: gettext-devel
BuildRequires: intltool
BuildRequires: lcov
BuildRequires: liblomiri-api-devel
BuildRequires: liblomiri-gmenuharness-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: liblomiri-qt-dbusmock-devel
BuildRequires: liblomiri-url-dispatcher-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(libunity-api)
BuildRequires: pkgconfig(libnm)
BuildRequires: pkgconfig(libsecret-1)
BuildRequires: pkgconfig(url-dispatcher-1)
BuildRequires: pkgconfig(libqtdbustest-1)
BuildRequires: pkgconfig(libqtdbusmock-1)
BuildRequires: pkgconfig(libgmenuharness)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Ubuntu Connectivity Qt API
Ubuntu Connectivity API - Qt bindings

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n indicator-network-4f220221965793ad69dcdb4d568012816d1f3e32

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DENABLE_TESTS=OFF ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
mkdir -p %{buildroot}%{_includedir}/connectivity-api/qt1/ubuntu/connectivity
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp src/connectivity-api/connectivity-qt/connectivityqt/connectivity.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/modem.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/modems-list-model.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/openvpn-connection.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/pptp-connection.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/sim.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/sims-list-model.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/vpn-connection.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/connectivityqt/vpn-connections-list-model.h %{buildroot}%{_includedir}/connectivity-api/qt1/connectivityqt
cp src/connectivity-api/connectivity-qt/ubuntu/connectivity/networking-status.h %{buildroot}%{_includedir}/connectivity-api/qt1/ubuntu/connectivity
ln -s libconnectivity-qt1.so.1 %{buildroot}%{_libdir}/libconnectivity-qt1.so
install -p -m 755 build/src/connectivity-api/connectivity-qt/libconnectivity-qt1.so.1 %{buildroot}%{_libdir}
cp build/connectivity-qt1.pc                                 %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/libconnectivity-qt1.so.1

%files devel
%{_includedir}/connectivity-api/qt1/connectivityqt/connectivity.h
%{_includedir}/connectivity-api/qt1/connectivityqt/modem.h
%{_includedir}/connectivity-api/qt1/connectivityqt/modems-list-model.h
%{_includedir}/connectivity-api/qt1/connectivityqt/openvpn-connection.h
%{_includedir}/connectivity-api/qt1/connectivityqt/pptp-connection.h
%{_includedir}/connectivity-api/qt1/connectivityqt/sim.h
%{_includedir}/connectivity-api/qt1/connectivityqt/sims-list-model.h
%{_includedir}/connectivity-api/qt1/connectivityqt/vpn-connection.h
%{_includedir}/connectivity-api/qt1/connectivityqt/vpn-connections-list-model.h
%{_includedir}/connectivity-api/qt1/ubuntu/connectivity/networking-status.h
%{_libdir}/libconnectivity-qt1.so
%{_libdir}/pkgconfig/connectivity-qt1.pc
