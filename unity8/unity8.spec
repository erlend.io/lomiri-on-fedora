Name:       unity8
Version:    1.0
Release:    7%{?dist}
Summary:    Lomiri shell
License:    GPL-3
URL:        https://github.com/ubports/unity8
Source0:    https://github.com/ubports/unity8/archive/e2393228f942a9dfc36f282ce14a29757485d8ca/unity8.tar.gz
Source1:    https://github.com/ubports/gsettings-ubuntu-touch-schemas/archive/b3bdf178e4226c91c567b84f1adf9202b2492ca2/gsettings-ubuntu-touch-schemas.tar.gz
Source2:    https://github.com/ubports/unity-notifications/archive/c6b8354b0c10bc11dc115e5cb4f552b24a074eb8/unity-notifications.tar.gz
Patch0:     0001-Move-indicator-dbus-calls-to-org.ayatana.patch
Patch1:     0002-Use-deviceinfo-for-platform.patch
Patch2:     0003-NoDpkgParse.patch
Patch3:     0004-skip-language-wizard-page.patch
Patch4:     0005-replace-ubuntu-app-launch-with-lomiri-app-launch.patch
Patch5:     0006-Make-fedora-find-the-dqbusxml2cpp.patch
Patch6:     0007-remove-dependency-to-system-settings.patch

%ifarch aarch64
BuildRequires: mir-devel = 1.8.0
%endif
%ifarch x86_64
BuildRequires: mir-devel
%endif
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: cmake
BuildRequires: doxygen
BuildRequires: dpkg-dev
BuildRequires: gcc-c++
BuildRequires: gnome-common
BuildRequires: graphviz
BuildRequires: gsettings-qt-devel
BuildRequires: intltool
BuildRequires: liblomiri-api-devel
BuildRequires: liblomiri-connectivity-qt-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-deviceinfo-devel
BuildRequires: liblomiri-download-manager-devel
BuildRequires: liblomiri-geonames-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: liblomiri-qt-dbusmock-devel
BuildRequires: liblomiri-usermetrics-devel
BuildRequires: lightdm-qt5-devel
BuildRequires: lomiri-cmake-extras
BuildRequires: lomiri-dbus-test-runner
BuildRequires: lomiri-qmenumodel-devel
BuildRequires: lomiri-ui-toolkit-devel
BuildRequires: pam-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: pkgconfig(deviceinfo)
BuildRequires: pkgconfig(unity-shell-application)
BuildRequires: pkgconfig(geonames)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(unity-shell-launcher)
BuildRequires: pkgconfig(qmenumodel)
BuildRequires: pkgconfig(gnome-desktop-3.0)
BuildRequires: pkgconfig(lomiri-app-launch-0)
BuildRequires: pkgconfig(UbuntuGestures)
BuildRequires: pkgconfig(libqtdbustest-1)
BuildRequires: pkgconfig(libqtdbusmock-1)
BuildRequires: pkgconfig(gsettings-qt)
BuildRequires: pkgconfig(libudev)
BuildRequires: pkgconfig(libevdev)
BuildRequires: pkgconfig(libusermetricsoutput-1)
BuildRequires: pkgconfig(liblightdm-qt5-3)
BuildRequires: pkgconfig(udm-common)
BuildRequires: pkgconfig(xcb)
BuildRequires: pkgconfig(connectivity-qt1)
BuildRequires: pkgconfig(unity-shell-notifications)
BuildRequires: pkgconfig(libqtdbustest-1)
Requires: liblomiri-api2
Requires: liblomiri-click
Requires: liblomiri-dbus-test
Requires: liblomiri-gmenuharness
Requires: liblomiri-history
Requires: liblomiri-messing-menu
Requires: liblomiri-qt-dbusmock
Requires: liblomiri-qt-dbus-test
Requires: liblomiri-telefon-service
Requires: liblomiri-thumbnailer
Requires: liblomiri-upstart
Requires: lomiri-biometryd
Requires: lomiri-content-hub
Requires: lomiri-dbus-test-runner
Requires: maliit-framework
Requires: lomiri-hfd
Requires: lomiri-qmenumodel
Requires: lomiri-qt-dbus-test-runner
Requires: lomiri-settings-components
Requires: lomiri-system-compositor
Requires: mir-demos
Requires: qtmir
Requires: qt5-qtgraphicaleffects
Requires: qt5-qtwayland

%description
The Lomiri shell is the primary user interface for Lomiri.

%prep
if [ -e "unity8-e2393228f942a9dfc36f282ce14a29757485d8ca" ]; then
   rm -fR unity8-e2393228f942a9dfc36f282ce14a29757485d8ca
fi
tar -xvf %SOURCE0
cd unity8-e2393228f942a9dfc36f282ce14a29757485d8ca
cat %PATCH0 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH1 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH2 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH3 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH4 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH5 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH6 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cd ..
if [ -e "gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2" ]; then
   rm -fR gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2
fi
tar -xvf %SOURCE1
if [ -e "unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8" ]; then
   rm -fR unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8
fi
tar -xvf %SOURCE2
%debug_package

%build
cd unity8-e2393228f942a9dfc36f282ce14a29757485d8ca
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_CXX_FLAGS="-Og -g3" -DCMAKE_SKIP_RPATH=YES -DNO_TESTS=ON ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd ..
cd gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2
./autogen.sh
make
cd ..
cd unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd ..

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/unity8/qml/AccountsService
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Cursor
mkdir -p %{buildroot}%{_libdir}/unity8/qml/GlobalShortcut
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Greeter/Unity/Launcher
mkdir -p %{buildroot}%{_libdir}/unity8/qml/LightDM/FullLightDM
mkdir -p %{buildroot}%{_libdir}/unity8/qml/LightDM/IntegratedLightDM
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Lights
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Powerd
mkdir -p %{buildroot}%{_libdir}/unity8/qml/ScreenshotDirectory
mkdir -p %{buildroot}%{_libdir}/unity8/qml/SessionBroadcast
mkdir -p %{buildroot}%{_libdir}/unity8/qml/UInput
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Ubuntu/Gestures
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/ApplicationMenu
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Connectivity
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Indicators
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/InputInfo
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Launcher
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Platform
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Session
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Utils
mkdir -p %{buildroot}%{_libdir}/unity8/qml/WindowManager
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Wizard
mkdir -p %{buildroot}/etc/ubuntu
mkdir -p %{buildroot}/etc/ubuntusensors
mkdir -p %{buildroot}/lib/udev/rules.d
mkdir -p %{buildroot}/usr/share/accountsservice/interfaces
mkdir -p %{buildroot}/usr/share/applications
mkdir -p %{buildroot}/usr/share/dbus-1/interfaces
mkdir -p %{buildroot}/usr/share/glib-2.0/schemas
mkdir -p %{buildroot}/usr/share/locale/aa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/am/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ar/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ast/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/az/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/be/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bg/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/br/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bs/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ca/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ca@valencia/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ce/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ckb/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/co/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/cs/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/cy/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/da/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/de/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/el/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_AU/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_GB/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_US/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/eo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/es/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/et/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/eu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ga/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gd/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/he/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hy/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ia/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/id/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/is/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/it/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ja/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/km/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/kn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ko/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ku/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ky/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/lo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/lt/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/lv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ml/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/mr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ms/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/my/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nb/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ne/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/oc/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/or/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt_BR/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ro/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ru/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sc/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/shn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/si/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sq/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sw/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ta/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/te/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/th/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/tr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ug/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/uk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ur/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/uz/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/vi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/wo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/xh/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_CN/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_HK/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_TW/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/polkit-1/actions
mkdir -p %{buildroot}/usr/share/unity8/ApplicationMenus
mkdir -p %{buildroot}/usr/share/unity8/Components/MediaServices
mkdir -p %{buildroot}/usr/share/unity8/Components/PanelState
mkdir -p %{buildroot}/usr/share/unity8/Components/SearchHistoryModel
mkdir -p %{buildroot}/usr/share/unity8/Components/graphics
mkdir -p %{buildroot}/usr/share/unity8/Greeter/graphics/session_icons
mkdir -p %{buildroot}/usr/share/unity8/Launcher/graphics
mkdir -p %{buildroot}/usr/share/unity8/Notifications
mkdir -p %{buildroot}/usr/share/unity8/Panel/Indicators/client
mkdir -p %{buildroot}/usr/share/unity8/Panel/graphics
mkdir -p %{buildroot}/usr/share/unity8/Rotation
mkdir -p %{buildroot}/usr/share/unity8/Stage/Spread
mkdir -p %{buildroot}/usr/share/unity8/Stage/graphics
mkdir -p %{buildroot}/usr/share/unity8/Tutorial/graphics
mkdir -p %{buildroot}/usr/share/unity8/Wizard/Components
mkdir -p %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
mkdir -p %{buildroot}/usr/share/unity8/graphics
mkdir -p %{buildroot}/usr/share/upstart/sessions
mkdir -p %{buildroot}/var/lib/polkit-1/localauthority/10-vendor.d
mkdir -p %{buildroot}/var/lib/unity8
cd unity8-e2393228f942a9dfc36f282ce14a29757485d8ca
cd build
%make_install
rm -fR %{buildroot}/usr/var
cd ..
cd ..
cd gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2
cp accountsservice/com.ubuntu.AccountsService.Input.xml      %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.AccountsService.Sound.xml      %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Phone.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Sound.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.AccountsService.Input.xml      %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.AccountsService.Sound.xml      %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Phone.xml %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Sound.xml %{buildroot}/usr/share/dbus-1/interfaces
cp schemas/com.ubuntu.notifications.hub.gschema.xml          %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.notifications.settings.gschema.xml     %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.phone.gschema.xml                      %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.sound.gschema.xml                      %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.touch.network.gschema.xml              %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.touch.sound.gschema.xml                %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.touch.system.gschema.xml               %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.user-interface.gschema.xml             %{buildroot}/usr/share/glib-2.0/schemas
cp accountsservice/com.ubuntu.AccountsService.policy         %{buildroot}/usr/share/polkit-1/actions
cp accountsservice/50-com.ubuntu.AccountsService.pkla        %{buildroot}/var/lib/polkit-1/localauthority/10-vendor.d
cd ..
cd unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8
install -p -m 755 build/src/libnotifyclientplugin.so         %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
install -p -m 755 build/src/libnotifyplugin.so               %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
cp src/qmldir                                                %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
cd ..

%files
%{_datadir}/unity8/*
%{_datadir}/applications/*
%{_datadir}/dbus-1/interfaces/*
%{_datadir}/accountsservice/interfaces/*
%{_datadir}/locale/is/LC_MESSAGES/*
%{_datadir}/locale/hi/LC_MESSAGES/*
%{_datadir}/locale/xh/LC_MESSAGES/*
%{_datadir}/locale/uk/LC_MESSAGES/*
%{_datadir}/locale/da/LC_MESSAGES/*
%{_datadir}/locale/id/LC_MESSAGES/*
%{_datadir}/locale/gv/LC_MESSAGES/*
%{_datadir}/locale/ca@valencia/LC_MESSAGES/*
%{_datadir}/locale/oc/LC_MESSAGES/*
%{_datadir}/locale/he/LC_MESSAGES/*
%{_datadir}/locale/lt/LC_MESSAGES/*
%{_datadir}/locale/gd/LC_MESSAGES/*
%{_datadir}/locale/es/LC_MESSAGES/*
%{_datadir}/locale/uz/LC_MESSAGES/*
%{_datadir}/locale/en_AU/LC_MESSAGES/*
%{_datadir}/locale/sc/LC_MESSAGES/*
%{_datadir}/locale/sl/LC_MESSAGES/*
%{_datadir}/locale/sw/LC_MESSAGES/*
%{_datadir}/locale/nl/LC_MESSAGES/*
%{_datadir}/locale/km/LC_MESSAGES/*
%{_datadir}/locale/eu/LC_MESSAGES/*
%{_datadir}/locale/vi/LC_MESSAGES/*
%{_datadir}/locale/fa/LC_MESSAGES/*
%{_datadir}/locale/si/LC_MESSAGES/*
%{_datadir}/locale/hu/LC_MESSAGES/*
%{_datadir}/locale/te/LC_MESSAGES/*
%{_datadir}/locale/or/LC_MESSAGES/*
%{_datadir}/locale/kn/LC_MESSAGES/*
%{_datadir}/locale/ku/LC_MESSAGES/*
%{_datadir}/locale/br/LC_MESSAGES/*
%{_datadir}/locale/ce/LC_MESSAGES/*
%{_datadir}/locale/ga/LC_MESSAGES/*
%{_datadir}/locale/ta/LC_MESSAGES/*
%{_datadir}/locale/my/LC_MESSAGES/*
%{_datadir}/locale/ml/LC_MESSAGES/*
%{_datadir}/locale/lv/LC_MESSAGES/*
%{_datadir}/locale/hr/LC_MESSAGES/*
%{_datadir}/locale/be/LC_MESSAGES/*
%{_datadir}/locale/ur/LC_MESSAGES/*
%{_datadir}/locale/it/LC_MESSAGES/*
%{_datadir}/locale/ckb/LC_MESSAGES/*
%{_datadir}/locale/sk/LC_MESSAGES/*
%{_datadir}/locale/gl/LC_MESSAGES/*
%{_datadir}/locale/az/LC_MESSAGES/*
%{_datadir}/locale/bs/LC_MESSAGES/*
%{_datadir}/locale/ast/LC_MESSAGES/*
%{_datadir}/locale/et/LC_MESSAGES/*
%{_datadir}/locale/nn/LC_MESSAGES/*
%{_datadir}/locale/el/LC_MESSAGES/*
%{_datadir}/locale/bn/LC_MESSAGES/*
%{_datadir}/locale/de/LC_MESSAGES/*
%{_datadir}/locale/cy/LC_MESSAGES/*
%{_datadir}/locale/zh_CN/LC_MESSAGES/*
%{_datadir}/locale/ca/LC_MESSAGES/*
%{_datadir}/locale/fo/LC_MESSAGES/*
%{_datadir}/locale/hy/LC_MESSAGES/*
%{_datadir}/locale/co/LC_MESSAGES/*
%{_datadir}/locale/ar/LC_MESSAGES/*
%{_datadir}/locale/ru/LC_MESSAGES/*
%{_datadir}/locale/bg/LC_MESSAGES/*
%{_datadir}/locale/zh_HK/LC_MESSAGES/*
%{_datadir}/locale/ug/LC_MESSAGES/*
%{_datadir}/locale/lo/LC_MESSAGES/*
%{_datadir}/locale/tr/LC_MESSAGES/*
%{_datadir}/locale/ko/LC_MESSAGES/*
%{_datadir}/locale/cs/LC_MESSAGES/*
%{_datadir}/locale/aa/LC_MESSAGES/*
%{_datadir}/locale/en_GB/LC_MESSAGES/*
%{_datadir}/locale/ro/LC_MESSAGES/*
%{_datadir}/locale/th/LC_MESSAGES/*
%{_datadir}/locale/nb/LC_MESSAGES/*
%{_datadir}/locale/en_US/LC_MESSAGES/*
%{_datadir}/locale/ne/LC_MESSAGES/*
%{_datadir}/locale/sv/LC_MESSAGES/*
%{_datadir}/locale/ja/LC_MESSAGES/*
%{_datadir}/locale/mr/LC_MESSAGES/*
%{_datadir}/locale/wo/LC_MESSAGES/*
%{_datadir}/locale/sq/LC_MESSAGES/*
%{_datadir}/locale/gu/LC_MESSAGES/*
%{_datadir}/locale/sr/LC_MESSAGES/*
%{_datadir}/locale/ky/LC_MESSAGES/*
%{_datadir}/locale/pt/LC_MESSAGES/*
%{_datadir}/locale/shn/LC_MESSAGES/*
%{_datadir}/locale/ms/LC_MESSAGES/*
%{_datadir}/locale/am/LC_MESSAGES/*
%{_datadir}/locale/zh_TW/LC_MESSAGES/*
%{_datadir}/locale/ia/LC_MESSAGES/*
%{_datadir}/locale/fi/LC_MESSAGES/*
%{_datadir}/locale/pa/LC_MESSAGES/*
%{_datadir}/locale/fr/LC_MESSAGES/*
%{_datadir}/locale/pl/LC_MESSAGES/*
%{_datadir}/locale/pt_BR/LC_MESSAGES/*
%{_datadir}/locale/eo/LC_MESSAGES/*
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/lightdm/greeters/*
%{_datadir}/lightdm/lightdm.conf.d/*
%{_bindir}/unity8
%{_bindir}/indicators-client
%{_libdir}/libunity8-private.so
%{_libdir}/libunity8-private.so.0.0.0
%{_libdir}/libunity8-private.so.0
%{_libdir}/unity8/qml/Unity/Platform/libPlatform-qml.so
%{_libdir}/unity8/qml/Unity/Platform/qmldir
%{_libdir}/unity8/qml/Unity/Session/libUnitySession-qml.so
%{_libdir}/unity8/qml/Unity/Session/qmldir
%{_libdir}/unity8/qml/Unity/ApplicationMenu/libApplicationMenu-qml.so
%{_libdir}/unity8/qml/Unity/ApplicationMenu/qmldir
%{_libdir}/unity8/qml/Unity/Connectivity/libConnectivity.so
%{_libdir}/unity8/qml/Unity/Connectivity/qmldir
%{_libdir}/unity8/qml/Unity/Launcher/libUnityLauncher-qml.so
%{_libdir}/unity8/qml/Unity/Launcher/qmldir
%{_libdir}/unity8/qml/Unity/InputInfo/libInputInfo.so
%{_libdir}/unity8/qml/Unity/InputInfo/qmldir
%{_libdir}/unity8/qml/Unity/Indicators/libIndicatorsQml.so
%{_libdir}/unity8/qml/Unity/Indicators/qmldir
%{_libdir}/unity8/qml/Greeter/Unity/Launcher/libUnityLauncherAS-qml.so
%{_libdir}/unity8/qml/Greeter/Unity/Launcher/qmldir
%{_libdir}/unity8/qml/ScreenshotDirectory/libScreenshotDirectory-qml.so
%{_libdir}/unity8/qml/ScreenshotDirectory/ScreenGrabber.qmltypes
%{_libdir}/unity8/qml/ScreenshotDirectory/qmldir
%{_libdir}/unity8/qml/SessionBroadcast/libSessionBroadcast-qml.so
%{_libdir}/unity8/qml/SessionBroadcast/qmldir
%{_libdir}/unity8/qml/mocks/LightDM/IntegratedLightDM/demo/liblightdm-qt5-3.so
%{_libdir}/unity8/qml/AccountsService/libAccountsService-qml.so
%{_libdir}/unity8/qml/AccountsService/qmldir
%{_libdir}/unity8/qml/GlobalShortcut/libGlobalShortcut-qml.so
%{_libdir}/unity8/qml/GlobalShortcut/qmldir
%{_libdir}/unity8/qml/Powerd/libPowerd-qml.so
%{_libdir}/unity8/qml/Powerd/qmldir
%{_libdir}/unity8/qml/Cursor/Cursor.qml
%{_libdir}/unity8/qml/Cursor/libCursor-qml.so
%{_libdir}/unity8/qml/Cursor/qmldir
%{_libdir}/unity8/qml/LightDM/IntegratedLightDM/libIntegratedLightDM-qml.so
%{_libdir}/unity8/qml/LightDM/IntegratedLightDM/qmldir
%{_libdir}/unity8/qml/LightDM/FullLightDM/libFullLightDM-qml.so
%{_libdir}/unity8/qml/LightDM/FullLightDM/qmldir
%{_libdir}/unity8/qml/Utils/libUtils-qml.so
%{_libdir}/unity8/qml/Utils/Style.js
%{_libdir}/unity8/qml/Utils/EdgeBarrierSettings.qml
%{_libdir}/unity8/qml/Utils/qmldir
%{_libdir}/unity8/qml/WindowManager/libwindowmanager-qml.so
%{_libdir}/unity8/qml/WindowManager/qmldir
%{_libdir}/unity8/qml/UInput/libUInput-qml.so
%{_libdir}/unity8/qml/UInput/qmldir
%{_libdir}/unity8/qml/Wizard/libWizard-qml.so
%{_libdir}/unity8/qml/Wizard/qmldir
%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener/libDownloadDaemonListener.so
%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener/qmldir
%{_libdir}/unity8/qml/Ubuntu/Gestures/libUbuntuGesturesQml.so
%{_libdir}/unity8/qml/Ubuntu/Gestures/qmldir
%{_libdir}/unity8/qml/Unity/Notifications/libnotifyclientplugin.so
%{_libdir}/unity8/qml/Unity/Notifications/libnotifyplugin.so
%{_libdir}/unity8/qml/Unity/Notifications/qmldir
/usr/share/accountsservice/interfaces/com.ubuntu.AccountsService.Input.xml
/usr/share/accountsservice/interfaces/com.ubuntu.AccountsService.SecurityPrivacy.xml
/usr/share/accountsservice/interfaces/com.ubuntu.AccountsService.Sound.xml
/usr/share/accountsservice/interfaces/com.ubuntu.touch.AccountsService.Phone.xml
/usr/share/accountsservice/interfaces/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml
/usr/share/accountsservice/interfaces/com.ubuntu.touch.AccountsService.Sound.xml
/usr/share/dbus-1/interfaces/com.ubuntu.AccountsService.Input.xml
/usr/share/dbus-1/interfaces/com.ubuntu.AccountsService.SecurityPrivacy.xml
/usr/share/dbus-1/interfaces/com.ubuntu.AccountsService.Sound.xml
/usr/share/dbus-1/interfaces/com.ubuntu.touch.AccountsService.Phone.xml
/usr/share/dbus-1/interfaces/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml
/usr/share/dbus-1/interfaces/com.ubuntu.touch.AccountsService.Sound.xml
/usr/share/glib-2.0/schemas/com.ubuntu.notifications.hub.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.notifications.settings.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.phone.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.sound.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.touch.network.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.touch.sound.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.touch.system.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.user-interface.gschema.xml
/usr/share/polkit-1/actions/com.ubuntu.AccountsService.policy
/var/lib/polkit-1/localauthority/10-vendor.d/50-com.ubuntu.AccountsService.pkla
