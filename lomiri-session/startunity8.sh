#!/bin/bash

export QT_QPA_PLATFORM=wayland
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export QT_WAYLAND_FORCE_DPI=96
# NOT part
export QT_QPA_PLATFORMTHEME=ubuntuappmenu

export MIR_SERVER_WAYLAND_HOST=/tmp/wayland-root
unset MIR_SERVER_HOST_SOCKET

if [ -x "$(command -v device-info)" ]; then
    GRID_UNIT_PX=$(device-info get GridUnit)
    QTWEBKIT_DPR=$(device-info get WebkitDpr)
    NATIVE_ORIENTATION=$(device-info get PrimaryOrientation)
fi
export GRID_UNIT_PX=${GRID_UNIT_PX}
export QTWEBKIT_DPR=${QTWEBKIT_DPR}
export NATIVE_ORIENTATION=${NATIVE_ORIENTATION}

# Set up xdg dirs
# export XDG_CONFIG_DIRS=$SNAP/etc/xdg/xdg-$DESKTOP_SESSION:$XDG_CONFIG_DIRS
# export XDG_DATA_DIRS=$SNAP/usr/share/$DESKTOP_SESSION:$XDG_DATA_DIRS

# Do these exists?
export XDG_CONFIG_DIRS=/etc/xdg/xdg-ubuntu-touch:/etc/xdg/xdg-ubuntu-touch:/etc/xdg
export XDG_DATA_DIRS=/usr/share/ubuntu-touch:/usr/share/ubuntu-touch:/usr/local/share/:/usr/share/:/custom/usr/share/

/usr/bin/unity8 --mode=full-greeter

