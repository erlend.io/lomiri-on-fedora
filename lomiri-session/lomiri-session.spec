Name:       lomiri-session
Version:    0.1
Release:    2%{?dist}
Summary:    FIXME
License:    GPL-3
URL:        https://gitlab.com/erlend.io/lomiri-session
Source0:    https://gitlab.com/erlend.io/lomiri-session/-/archive/a23bf26d81476abb287c76333ccfe6fb06cc1b9c/lomiri-session.tar.gz


%description


%global debug_package %{nil}

%prep
%autosetup -p1 -n lomiri-session-a23bf26d81476abb287c76333ccfe6fb06cc1b9c

%build

%install
mkdir -p %{buildroot}%{_datadir}/wayland-sessions
mkdir -p %{buildroot}%{_bindir}
cp lomiri.desktop                %{buildroot}%{_datadir}/wayland-sessions
install -p -m 755 lomiri-startup %{buildroot}%{_bindir}

%files
%{_datadir}/wayland-sessions/lomiri.desktop
%{_bindir}/lomiri-startup
