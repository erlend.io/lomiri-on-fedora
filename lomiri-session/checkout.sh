#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch master https://gitlab.com/erlend.io/lomiri-session.git
cd lomiri-session
