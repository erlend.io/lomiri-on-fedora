Name:       liblomiri-url-dispatcher
Version:    1.0
Release:    1%{?dist}
Summary:    library for sending requests to the url dispatcher
License:    FIXME
URL:        https://github.com/ubports/url-dispatcher
Source0:    https://github.com/ubports/url-dispatcher/archive/7a22532a70550dedc05469697607d292f8eab92c/url-dispatcher.tar.gz
Patch0:     0001-dummy-patch.patch

BuildRequires: cmake
BuildRequires: dbus-devel
BuildRequires: gcc-c++
BuildRequires: liblomiri-app-launch-devel
BuildRequires: liblomiri-click-devel
BuildRequires: liblomiri-dbus-test-devel
BuildRequires: pkgconfig(ubuntu-app-launch-2)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(json-glib-1.0)
BuildRequires: pkgconfig(dbustest-1)
BuildRequires: pkgconfig(sqlite3)
BuildRequires: pkgconfig(click-0.4)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
library for sending requests to the url dispatcher
Allows applications to request a URL to be opened and handled by another
process without seeing the list of other applications on the system or
starting them inside its own Application Confinement.
.
This package contains shared libraries to be used by applications.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n url-dispatcher-7a22532a70550dedc05469697607d292f8eab92c

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_BUILD_TYPE=FALSE -Denable_tests=OFF -Denable_lcov=OFF -Dwerror=OFF '-DCMAKE_C_FLAGS:STRING=-Og -g3 -DNDEBUG' '-DCMAKE_CXX_FLAGS:STRING=-Og -g3 -DNDEBUG' ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/liburl-dispatcher-1
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp liburl-dispatcher/url-dispatcher.h                        %{buildroot}%{_includedir}/liburl-dispatcher-1
ln -s liburl-dispatcher.so.1 %{buildroot}%{_libdir}/liburl-dispatcher.so
ln -s liburl-dispatcher.so.1.0.0 %{buildroot}%{_libdir}/liburl-dispatcher.so.1
install -p -m 755 build/liburl-dispatcher/liburl-dispatcher.so.1.0.0 %{buildroot}%{_libdir}
cp build/liburl-dispatcher/url-dispatcher-1.pc               %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/liburl-dispatcher.so.1
%{_libdir}/liburl-dispatcher.so.1.0.0

%files devel
%{_includedir}/liburl-dispatcher-1/url-dispatcher.h
%{_libdir}/liburl-dispatcher.so
%{_libdir}/pkgconfig/url-dispatcher-1.pc
