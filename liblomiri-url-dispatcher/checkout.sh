#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial_-_edge https://github.com/ubports/url-dispatcher.git
cd url-dispatcher
cat $SRC/0001-dummy-patch.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "dummy-patch"
