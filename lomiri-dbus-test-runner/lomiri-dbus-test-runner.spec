Name:       lomiri-dbus-test-runner
Version:    1.0
Release:    1%{?dist}
Summary:    Runs tests under a new DBus session
License:    FIXME
URL:        https://github.com/ubports/dbus-test-runner
Source0:    https://github.com/ubports/dbus-test-runner/archive/86d63d119566974bd841cacd3202599b3b1a845d/dbus-test-runner.tar.gz
Patch0:     0001-removing-werror.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gcc-c++
BuildRequires: gnome-common
BuildRequires: intltool
BuildRequires: pkgconfig(dbus-glib-1)

%description
Runs tests under a new DBus session
A simple little executable for running a couple of programs under a
new DBus session.


%global debug_package %{nil}

%prep
%autosetup -p1 -n dbus-test-runner-86d63d119566974bd841cacd3202599b3b1a845d

%build
./autogen.sh
./configure --prefix=/usr
make

%install
mkdir -p %{buildroot}%{_bindir}
install -p -m 755 src/dbus-test-runner                       %{buildroot}%{_bindir}

%files
%{_bindir}/dbus-test-runner
