Name:       qt5-mobility-feedback
Version:    1.0
Release:    1%{?dist}
Summary:    Qt Feedback module
License:    FIXME
URL:        https://github.com/qt/qtfeedback
Source0:    https://github.com/qt/qtfeedback/archive/a14bd0bb1373cde86e09e3619fb9dc70f34c71f2/qtfeedback.tar.gz
Patch0:     0001-set-version-number-to-5.0.0.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtdeclarative-devel

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Qt Feedback module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt Feedback module.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt Feedback module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt Feedback module.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
transitional dummy package for Qt 5 Service Framework QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This is a transitional dummy package for qml-module-qtfeedback
which can be safely removed.
transitional dummy package for Qt 5 Service Framework QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This is a transitional dummy package for qml-module-qtfeedback
which can be safely removed.
Qt 5 Feedback QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the Qt Feedback QML module for Qt Declarative.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt 5 Feedback QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the Qt Feedback QML module for Qt Declarative.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n qtfeedback-a14bd0bb1373cde86e09e3619fb9dc70f34c71f2
mkdir .git

%build
mkdir build
cd build
qmake-qt5 QMAKE_CXXFLAGS_RELEASE="-g3 -Og" ..
%make_build
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/qt5/QtFeedback
mkdir -p %{buildroot}%{_libdir}/cmake/Qt5Feedback
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_libdir}/qt5/mkspecs/modules
mkdir -p %{buildroot}%{_libdir}/qt5/plugins/feedback
mkdir -p %{buildroot}%{_libdir}/qt5/qml/QtFeedback
cp build/include/QtFeedback/QFeedbackActuator                %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QFeedbackEffect                  %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QFeedbackFileEffect              %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QFeedbackFileInterface           %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QFeedbackHapticsEffect           %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QFeedbackHapticsInterface        %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QFeedbackInterface               %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QFeedbackThemeInterface          %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QtFeedback                       %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/QtFeedbackVersion                %{buildroot}%{_includedir}/qt5/QtFeedback
cp src/feedback/qfeedbackactuator.h                          %{buildroot}%{_includedir}/qt5/QtFeedback
cp src/feedback/qfeedbackeffect.h                            %{buildroot}%{_includedir}/qt5/QtFeedback
cp src/feedback/qfeedbackglobal.h                            %{buildroot}%{_includedir}/qt5/QtFeedback
cp src/feedback/qfeedbackplugininterfaces.h                  %{buildroot}%{_includedir}/qt5/QtFeedback
cp src/feedback/qfeedbackpluginsearch.h                      %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/include/QtFeedback/qtfeedbackversion.h              %{buildroot}%{_includedir}/qt5/QtFeedback
cp build/lib/cmake/Qt5Feedback/Qt5FeedbackConfig.cmake       %{buildroot}%{_libdir}/cmake/Qt5Feedback
cp build/lib/cmake/Qt5Feedback/Qt5FeedbackConfigVersion.cmake %{buildroot}%{_libdir}/cmake/Qt5Feedback
cp build/lib/libQt5Feedback.prl                              %{buildroot}%{_libdir}
ln -s libQt5Feedback.so.5.0.0 %{buildroot}%{_libdir}/libQt5Feedback.so
ln -s libQt5Feedback.so.5.0.0 %{buildroot}%{_libdir}/libQt5Feedback.so.5
ln -s libQt5Feedback.so.5.0.0 %{buildroot}%{_libdir}/libQt5Feedback.so.5.0
install -p -m 755 build/lib/libQt5Feedback.so.5.0.0          %{buildroot}%{_libdir}
cp build/lib/pkgconfig/Qt5Feedback.pc                        %{buildroot}%{_libdir}/pkgconfig
cp build/mkspecs/modules-inst/qt_lib_feedback.pri            %{buildroot}%{_libdir}/qt5/mkspecs/modules
install -p -m 755 build/qml/QtFeedback/libdeclarative_feedback.so %{buildroot}%{_libdir}/qt5/qml/QtFeedback
cp build/qml/QtFeedback/plugins.qmltypes                     %{buildroot}%{_libdir}/qt5/qml/QtFeedback
cp build/qml/QtFeedback/qmldir                               %{buildroot}%{_libdir}/qt5/qml/QtFeedback

# Not Found:/usr/lib/aarch64-linux-gnu/cmake/Qt5Feedback/Qt5Feedback_.cmake
# Not Found:/usr/lib/aarch64-linux-gnu/qt5/plugins/feedback/libqtfeedback_mmk.so



%files
%{_libdir}/libQt5Feedback.so.5
%{_libdir}/libQt5Feedback.so.5.0
%{_libdir}/libQt5Feedback.so.5.0.0
%{_libdir}/qt5/qml/QtFeedback/libdeclarative_feedback.so
%{_libdir}/qt5/qml/QtFeedback/plugins.qmltypes
%{_libdir}/qt5/qml/QtFeedback/qmldir
%{_libdir}/libQt5Feedback.so

%files devel
%{_includedir}/qt5/QtFeedback/QFeedbackActuator
%{_includedir}/qt5/QtFeedback/QFeedbackEffect
%{_includedir}/qt5/QtFeedback/QFeedbackFileEffect
%{_includedir}/qt5/QtFeedback/QFeedbackFileInterface
%{_includedir}/qt5/QtFeedback/QFeedbackHapticsEffect
%{_includedir}/qt5/QtFeedback/QFeedbackHapticsInterface
%{_includedir}/qt5/QtFeedback/QFeedbackInterface
%{_includedir}/qt5/QtFeedback/QFeedbackThemeInterface
%{_includedir}/qt5/QtFeedback/QtFeedback
%{_includedir}/qt5/QtFeedback/QtFeedbackVersion
%{_includedir}/qt5/QtFeedback/qfeedbackactuator.h
%{_includedir}/qt5/QtFeedback/qfeedbackeffect.h
%{_includedir}/qt5/QtFeedback/qfeedbackglobal.h
%{_includedir}/qt5/QtFeedback/qfeedbackplugininterfaces.h
%{_includedir}/qt5/QtFeedback/qfeedbackpluginsearch.h
%{_includedir}/qt5/QtFeedback/qtfeedbackversion.h
%{_libdir}/cmake/Qt5Feedback/Qt5FeedbackConfig.cmake
%{_libdir}/cmake/Qt5Feedback/Qt5FeedbackConfigVersion.cmake
%{_libdir}/libQt5Feedback.prl
%{_libdir}/libQt5Feedback.so
%{_libdir}/pkgconfig/Qt5Feedback.pc
%{_libdir}/qt5/mkspecs/modules/qt_lib_feedback.pri
