#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch master https://github.com/qt/qtfeedback.git
cd qtfeedback
cat $SRC/0001-set-version-number-to-5.0.0.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "set-version-number-to-5.0.0"
cd $CHECKOUTDIR
git clone --single-branch --branch xenial_-_upsqtmodules https://github.com/ubports/qtfeedback-opensource-src-packaging.git
cd qtfeedback-opensource-src-packaging
