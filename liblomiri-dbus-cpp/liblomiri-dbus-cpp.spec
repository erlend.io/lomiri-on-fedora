Name:       liblomiri-dbus-cpp
Version:    1.0
Release:    1%{?dist}
Summary:    header-only dbus-binding leveraging C++-11
License:    FIXME
URL:        https://github.com/ubports/dbus-cpp
Source0:    https://github.com/ubports/dbus-cpp/archive/967dc1caf0efe0a1286c308e8e8dd1bf7da5f3ee/dbus-cpp.tar.gz
Patch0:     0001-removing-werror-and-disabling-tests.patch
Patch1:     0002-removing-exe.patch
Patch2:     0003-removing-forward-referenece-that-was-causing-compila.patch

BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: liblomiri-cpp-devel
BuildRequires: libxml2-devel
BuildRequires: redhat-lsb-core
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(process-cpp)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
header-only dbus-binding leveraging C++-11
A header-only dbus-binding leveraging C++-11, relying on compile-time
polymorphism to integrate with arbitrary type systems. Runtime portions to bind
to different event loops.
header-only dbus-binding leveraging C++-11
A header-only dbus-binding leveraging C++-11, relying on compile-time
polymorphism to integrate with arbitrary type systems. Runtime portions to bind
to different event loops.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n dbus-cpp-967dc1caf0efe0a1286c308e8e8dd1bf7da5f3ee

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/core/dbus/asio
mkdir -p %{buildroot}%{_includedir}/core/dbus/helper
mkdir -p %{buildroot}%{_includedir}/core/dbus/impl
mkdir -p %{buildroot}%{_includedir}/core/dbus/interfaces
mkdir -p %{buildroot}%{_includedir}/core/dbus/traits
mkdir -p %{buildroot}%{_includedir}/core/dbus/types/stl
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/dbus-cpp
cp include/core/dbus/announcer.h                             %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/argument_type.h                         %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/asio/executor.h                         %{buildroot}%{_includedir}/core/dbus/asio
cp include/core/dbus/bus.h                                   %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/codec.h                                 %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/compiler.h                              %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/dbus.h                                  %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/error.h                                 %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/executor.h                              %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/fixture.h                               %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/generator.h                             %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/generator_configuration.h               %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/helper/apply_visitor.h                  %{buildroot}%{_includedir}/core/dbus/helper
cp include/core/dbus/helper/is_compound_type.h               %{buildroot}%{_includedir}/core/dbus/helper
cp include/core/dbus/helper/signature.h                      %{buildroot}%{_includedir}/core/dbus/helper
cp include/core/dbus/helper/type_mapper.h                    %{buildroot}%{_includedir}/core/dbus/helper
cp include/core/dbus/impl/message.h                          %{buildroot}%{_includedir}/core/dbus/impl
cp include/core/dbus/impl/object.h                           %{buildroot}%{_includedir}/core/dbus/impl
cp include/core/dbus/impl/property.h                         %{buildroot}%{_includedir}/core/dbus/impl
cp include/core/dbus/impl/signal.h                           %{buildroot}%{_includedir}/core/dbus/impl
cp include/core/dbus/interfaces/introspectable.h             %{buildroot}%{_includedir}/core/dbus/interfaces
cp include/core/dbus/interfaces/object_manager.h             %{buildroot}%{_includedir}/core/dbus/interfaces
cp include/core/dbus/interfaces/properties.h                 %{buildroot}%{_includedir}/core/dbus/interfaces
cp include/core/dbus/introspection_parser.h                  %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/lifetime_constrained_cache.h            %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/macros.h                                %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/match_rule.h                            %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/message.h                               %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/message_factory.h                       %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/message_router.h                        %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/message_streaming_operators.h           %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/object.h                                %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/pending_call.h                          %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/property.h                              %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/resolver.h                              %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/result.h                                %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/service.h                               %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/service_watcher.h                       %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/signal.h                                %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/skeleton.h                              %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/stub.h                                  %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/traits/service.h                        %{buildroot}%{_includedir}/core/dbus/traits
cp include/core/dbus/traits/timeout.h                        %{buildroot}%{_includedir}/core/dbus/traits
cp include/core/dbus/traits/watch.h                          %{buildroot}%{_includedir}/core/dbus/traits
cp include/core/dbus/types/any.h                             %{buildroot}%{_includedir}/core/dbus/types
cp include/core/dbus/types/object_path.h                     %{buildroot}%{_includedir}/core/dbus/types
cp include/core/dbus/types/signature.h                       %{buildroot}%{_includedir}/core/dbus/types
cp include/core/dbus/types/stl/list.h                        %{buildroot}%{_includedir}/core/dbus/types/stl
cp include/core/dbus/types/stl/map.h                         %{buildroot}%{_includedir}/core/dbus/types/stl
cp include/core/dbus/types/stl/string.h                      %{buildroot}%{_includedir}/core/dbus/types/stl
cp include/core/dbus/types/stl/tuple.h                       %{buildroot}%{_includedir}/core/dbus/types/stl
cp include/core/dbus/types/stl/vector.h                      %{buildroot}%{_includedir}/core/dbus/types/stl
cp include/core/dbus/types/struct.h                          %{buildroot}%{_includedir}/core/dbus/types
cp include/core/dbus/types/unix_fd.h                         %{buildroot}%{_includedir}/core/dbus/types
cp include/core/dbus/types/variant.h                         %{buildroot}%{_includedir}/core/dbus/types
cp include/core/dbus/visibility.h                            %{buildroot}%{_includedir}/core/dbus
cp include/core/dbus/well_known_bus.h                        %{buildroot}%{_includedir}/core/dbus
ln -s libdbus-cpp.so.5 %{buildroot}%{_libdir}/libdbus-cpp.so
ln -s libdbus-cpp.so.5.0.0 %{buildroot}%{_libdir}/libdbus-cpp.so.5
install -p -m 755 build/src/core/dbus/libdbus-cpp.so.5.0.0   %{buildroot}%{_libdir}
cp build/data/dbus-cpp.pc                                    %{buildroot}%{_libdir}/pkgconfig
cp data/session.conf                                         %{buildroot}/usr/share/dbus-cpp
cp data/system.conf                                          %{buildroot}/usr/share/dbus-cpp

%files
%{_libdir}/libdbus-cpp.so.5
%{_libdir}/libdbus-cpp.so.5.0.0

%files devel
%{_includedir}/core/dbus/announcer.h
%{_includedir}/core/dbus/argument_type.h
%{_includedir}/core/dbus/asio/executor.h
%{_includedir}/core/dbus/bus.h
%{_includedir}/core/dbus/codec.h
%{_includedir}/core/dbus/compiler.h
%{_includedir}/core/dbus/dbus.h
%{_includedir}/core/dbus/error.h
%{_includedir}/core/dbus/executor.h
%{_includedir}/core/dbus/fixture.h
%{_includedir}/core/dbus/generator.h
%{_includedir}/core/dbus/generator_configuration.h
%{_includedir}/core/dbus/helper/apply_visitor.h
%{_includedir}/core/dbus/helper/is_compound_type.h
%{_includedir}/core/dbus/helper/signature.h
%{_includedir}/core/dbus/helper/type_mapper.h
%{_includedir}/core/dbus/impl/message.h
%{_includedir}/core/dbus/impl/object.h
%{_includedir}/core/dbus/impl/property.h
%{_includedir}/core/dbus/impl/signal.h
%{_includedir}/core/dbus/interfaces/introspectable.h
%{_includedir}/core/dbus/interfaces/object_manager.h
%{_includedir}/core/dbus/interfaces/properties.h
%{_includedir}/core/dbus/introspection_parser.h
%{_includedir}/core/dbus/lifetime_constrained_cache.h
%{_includedir}/core/dbus/macros.h
%{_includedir}/core/dbus/match_rule.h
%{_includedir}/core/dbus/message.h
%{_includedir}/core/dbus/message_factory.h
%{_includedir}/core/dbus/message_router.h
%{_includedir}/core/dbus/message_streaming_operators.h
%{_includedir}/core/dbus/object.h
%{_includedir}/core/dbus/pending_call.h
%{_includedir}/core/dbus/property.h
%{_includedir}/core/dbus/resolver.h
%{_includedir}/core/dbus/result.h
%{_includedir}/core/dbus/service.h
%{_includedir}/core/dbus/service_watcher.h
%{_includedir}/core/dbus/signal.h
%{_includedir}/core/dbus/skeleton.h
%{_includedir}/core/dbus/stub.h
%{_includedir}/core/dbus/traits/service.h
%{_includedir}/core/dbus/traits/timeout.h
%{_includedir}/core/dbus/traits/watch.h
%{_includedir}/core/dbus/types/any.h
%{_includedir}/core/dbus/types/object_path.h
%{_includedir}/core/dbus/types/signature.h
%{_includedir}/core/dbus/types/stl/list.h
%{_includedir}/core/dbus/types/stl/map.h
%{_includedir}/core/dbus/types/stl/string.h
%{_includedir}/core/dbus/types/stl/tuple.h
%{_includedir}/core/dbus/types/stl/vector.h
%{_includedir}/core/dbus/types/struct.h
%{_includedir}/core/dbus/types/unix_fd.h
%{_includedir}/core/dbus/types/variant.h
%{_includedir}/core/dbus/visibility.h
%{_includedir}/core/dbus/well_known_bus.h
%{_libdir}/libdbus-cpp.so
%{_libdir}/pkgconfig/dbus-cpp.pc
/usr/share/dbus-cpp/session.conf
/usr/share/dbus-cpp/system.conf
