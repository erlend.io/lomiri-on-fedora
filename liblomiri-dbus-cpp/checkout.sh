#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch master https://github.com/ubports/dbus-cpp.git
cd dbus-cpp
cat $SRC/0001-removing-werror-and-disabling-tests.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removing-werror-and-disabling-tests"
cat $SRC/0002-removing-exe.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removing-exe"
cat $SRC/0003-removing-forward-referenece-that-was-causing-compila.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removing-forward-referenece-that-was-causing-compila"
