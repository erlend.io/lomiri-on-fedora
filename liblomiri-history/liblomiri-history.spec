Name:       liblomiri-history
Version:    1.0
Release:    1%{?dist}
Summary:    History service to store messages and calls - client library
License:    FIXME
URL:        https://github.com/ubports/history-service
Source0:    https://github.com/ubports/history-service/archive/629165d9f0611004e60095b9b5a88964adc69f1f/history-service.tar.gz
Patch0:     0001-change-name-of-qmake-tool-to-qmake-qt5.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: libphonenumber-devel
BuildRequires: protobuf-devel
BuildRequires: qt5-mobility-pim-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtbase-static
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: pkgconfig(TelepathyQt5)
BuildRequires: pkgconfig(sqlite3)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
History service to store messages and calls - client library
A service to record the messages that are sent and received and the calls that
are made in the Ubuntu Touch platform.
.
This package contains the client library to access the data stored by the
history service.
History service to store messages and calls - QML plugin
A service to record the messages that are sent and received and the calls that
are made in the Ubuntu Touch platform.
.
This package contains a QML plugin that provides data models to access the data
stored by the history service.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n history-service-629165d9f0611004e60095b9b5a88964adc69f1f

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_SKIP_RPATH=YES ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/history-service/History
mkdir -p %{buildroot}%{_libdir}/history-service/plugins
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/History
cp src/Event                                                 %{buildroot}%{_includedir}/history-service/History
cp src/EventView                                             %{buildroot}%{_includedir}/history-service/History
cp src/Filter                                                %{buildroot}%{_includedir}/history-service/History
cp src/IntersectionFilter                                    %{buildroot}%{_includedir}/history-service/History
cp src/Manager                                               %{buildroot}%{_includedir}/history-service/History
cp src/Participant                                           %{buildroot}%{_includedir}/history-service/History
cp src/Plugin                                                %{buildroot}%{_includedir}/history-service/History
cp src/PluginEventView                                       %{buildroot}%{_includedir}/history-service/History
cp src/PluginThreadView                                      %{buildroot}%{_includedir}/history-service/History
cp src/Sort                                                  %{buildroot}%{_includedir}/history-service/History
cp src/TextEvent                                             %{buildroot}%{_includedir}/history-service/History
cp src/TextEventAttachment                                   %{buildroot}%{_includedir}/history-service/History
cp src/Thread                                                %{buildroot}%{_includedir}/history-service/History
cp src/ThreadView                                            %{buildroot}%{_includedir}/history-service/History
cp src/Types                                                 %{buildroot}%{_includedir}/history-service/History
cp src/UnionFilter                                           %{buildroot}%{_includedir}/history-service/History
cp src/VoiceEvent                                            %{buildroot}%{_includedir}/history-service/History
cp src/event.h                                               %{buildroot}%{_includedir}/history-service/History
cp src/eventview.h                                           %{buildroot}%{_includedir}/history-service/History
cp src/filter.h                                              %{buildroot}%{_includedir}/history-service/History
cp src/intersectionfilter.h                                  %{buildroot}%{_includedir}/history-service/History
cp src/manager.h                                             %{buildroot}%{_includedir}/history-service/History
cp src/participant.h                                         %{buildroot}%{_includedir}/history-service/History
cp src/plugin.h                                              %{buildroot}%{_includedir}/history-service/History
cp src/plugineventview.h                                     %{buildroot}%{_includedir}/history-service/History
cp src/pluginthreadview.h                                    %{buildroot}%{_includedir}/history-service/History
cp src/sort.h                                                %{buildroot}%{_includedir}/history-service/History
cp src/textevent.h                                           %{buildroot}%{_includedir}/history-service/History
cp src/texteventattachment.h                                 %{buildroot}%{_includedir}/history-service/History
cp src/thread.h                                              %{buildroot}%{_includedir}/history-service/History
cp src/threadview.h                                          %{buildroot}%{_includedir}/history-service/History
cp src/types.h                                               %{buildroot}%{_includedir}/history-service/History
cp src/unionfilter.h                                         %{buildroot}%{_includedir}/history-service/History
cp src/voiceevent.h                                          %{buildroot}%{_includedir}/history-service/History
install -p -m 755 build/plugins/sqlite/libsqlitehistoryplugin.so %{buildroot}%{_libdir}/history-service/plugins
ln -s libhistoryservice.so.0 %{buildroot}%{_libdir}/libhistoryservice.so
ln -s libhistoryservice.so.0.0.0 %{buildroot}%{_libdir}/libhistoryservice.so.0
install -p -m 755 build/src/libhistoryservice.so.0.0.0       %{buildroot}%{_libdir}
cp build/src/history-service.pc                              %{buildroot}%{_libdir}/pkgconfig
install -p -m 755 build/Ubuntu/History/libhistory-qml.so     %{buildroot}%{_libdir}/qt5/qml/Ubuntu/History
cp Ubuntu/History/qmldir                                     %{buildroot}%{_libdir}/qt5/qml/Ubuntu/History

%files
%{_libdir}/history-service/plugins/libsqlitehistoryplugin.so
%{_libdir}/libhistoryservice.so.0
%{_libdir}/libhistoryservice.so.0.0.0
%{_libdir}/qt5/qml/Ubuntu/History/libhistory-qml.so
%{_libdir}/qt5/qml/Ubuntu/History/qmldir

%files devel
%{_includedir}/history-service/History/Event
%{_includedir}/history-service/History/EventView
%{_includedir}/history-service/History/Filter
%{_includedir}/history-service/History/IntersectionFilter
%{_includedir}/history-service/History/Manager
%{_includedir}/history-service/History/Participant
%{_includedir}/history-service/History/Plugin
%{_includedir}/history-service/History/PluginEventView
%{_includedir}/history-service/History/PluginThreadView
%{_includedir}/history-service/History/Sort
%{_includedir}/history-service/History/TextEvent
%{_includedir}/history-service/History/TextEventAttachment
%{_includedir}/history-service/History/Thread
%{_includedir}/history-service/History/ThreadView
%{_includedir}/history-service/History/Types
%{_includedir}/history-service/History/UnionFilter
%{_includedir}/history-service/History/VoiceEvent
%{_includedir}/history-service/History/event.h
%{_includedir}/history-service/History/eventview.h
%{_includedir}/history-service/History/filter.h
%{_includedir}/history-service/History/intersectionfilter.h
%{_includedir}/history-service/History/manager.h
%{_includedir}/history-service/History/participant.h
%{_includedir}/history-service/History/plugin.h
%{_includedir}/history-service/History/plugineventview.h
%{_includedir}/history-service/History/pluginthreadview.h
%{_includedir}/history-service/History/sort.h
%{_includedir}/history-service/History/textevent.h
%{_includedir}/history-service/History/texteventattachment.h
%{_includedir}/history-service/History/thread.h
%{_includedir}/history-service/History/threadview.h
%{_includedir}/history-service/History/types.h
%{_includedir}/history-service/History/unionfilter.h
%{_includedir}/history-service/History/voiceevent.h
%{_libdir}/libhistoryservice.so
%{_libdir}/pkgconfig/history-service.pc
