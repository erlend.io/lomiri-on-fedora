#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/history-service.git
cd history-service
cat $SRC/0001-change-name-of-qmake-tool-to-qmake-qt5.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "change-name-of-qmake-tool-to-qmake-qt5"
