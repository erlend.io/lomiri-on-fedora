#!/bin/bash
# exit when any command fails
set -e

dnf remove \
gsettings-qt \
gsettings-qt-devel \
liblomiri-api \
liblomiri-api2 \
liblomiri-app-launch \
liblomiri-click \
liblomiri-connectivity-qt \
liblomiri-connectivity-qt-devel \
liblomiri-content-hub \
liblomiri-cpp \
liblomiri-dbus-cpp \
liblomiri-dbus-test \
liblomiri-deviceinfo \
liblomiri-geonames \
liblomiri-gmenuharness \
liblomiri-gmenuharness-devel \
liblomiri-history \
liblomiri-libertine \
liblomiri-messing-menu \
liblomiri-messing-menu-devel \
liblomiri-qmenumodel \
liblomiri-upstart \
liblomiri-url-dispatcher2 \
liblomiri-qt-dbus-test \
liblomiri-qt-dbusmock \
qt5-mobility-feedback \
qt5-mobility-pim \
lomiri-dbus-test-runner \
lomiri-session \
liblomiri-download-manager \
liblomiri-qt-systems \
lomiri-ui-toolkit \
liblomiri-telefon-service \
lomiri-qmenumodel \
lomiri-qt-dbus-test-runner \
liblomiri-metrics liblomiri-metrics-devel \
liblomiri-url-dispatcher \
liblomiri-url-dispatcher-devel \
liblomiri-usermetrics \
liblomiri-usermetrics-devel \
lomiri-settings-components \
lomiri-qml-settings-components \
lomiri-toolkit-theme \
maliit-framework \
mir-client-libs \
mir-common-libs \
mir-demos \
mir-server-libs \
qt5-systems

