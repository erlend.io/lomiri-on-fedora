#!/bin/bash

dnf install gsettings-qt-1.0 \
liblomiri-api2 \
liblomiri-app-launch2 \
liblomiri-click  \
liblomiri-connectivity-qt \
liblomiri-cpp \
liblomiri-dbus-cpp \
liblomiri-dbus-test \
liblomiri-deviceinfo \
liblomiri-download-manager \
liblomiri-geonames \
liblomiri-gmenuharness \
liblomiri-history \
liblomiri-libertine \
liblomiri-messing-menu \
liblomiri-qt-dbusmock \
liblomiri-qt-dbus-test \
liblomiri-telefon-service \
liblomiri-thumbnailer \
liblomiri-upstart \
liblomiri-url-dispatcher2 \
liblomiri-usermetrics \
lomiri2 \
lomiri-biometryd \
lomiri-content-hub \
lomiri-dbus-test-runner \
lomiri-qmenumodel \
lomiri-qt-dbus-test-runner \
lomiri-qtmir2 \
lomiri-settings-components \
lomiri-system-compositor \
lomiri-ui-toolkit \
mir-demos \
qt5-qtgraphicaleffects \
qt5-mobility-feedback \
qt5-mobility-pim \
qt5-systems \
maliit-framework

