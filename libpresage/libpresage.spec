Name:       libpresage
Version:    0.9.1
Release:    2%{?dist}
Summary:    The intelligent predictive text entry system
License:    GPL-2
URL:        https://sourceforge.net/p/presage/presage/ci/0.9.1/tree/
Source0:    https://deac-ams.dl.sourceforge.net/project/presage/presage/0.9.1/presage-0.9.1.tar.gz
Patch0:     0001-up-to-date.patch
Patch1:     0003-fix-exception.patch
Patch2:     0004-move-to-python-3.8.patch
Patch3:     0005-python-fix.patch
Patch4:     0006-change-shebangs-to-python3.patch
Patch5:     0007-fix-compilation-issue-c17-does-not-allow-exception.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: cppunit-devel
BuildRequires: gcc-c++
BuildRequires: libtool
BuildRequires: ncurses-devel
BuildRequires: sqlite-devel
BuildRequires: tinyxml-devel
BuildRequires: python3-devel
BuildRequires: python3-dbus

%description

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n presage-0.9.1

%build
export CFLAGS='-g3 -Og'
export CXXFLAGS='-g3 -Og'
autoreconf -i
./configure --prefix=/usr --libdir=/usr/lib64 --sysconfdir=/etc
make V=1

%install
%make_install
rm -fR %{buildroot}/usr/lib/debug
rm %{buildroot}/usr/bin/presage_demo

%files
%{_sysconfdir}/presage.xml
%{_datadir}/dbus-1/services/*
%{_datadir}/presage/*
%if %{fedora} == 32
/usr/lib/python3.8/site-packages/presage_dbus_service.py
/usr/lib/python3.8/site-packages/presage_dbus_service.pyc
/usr/lib/python3.8/site-packages/presage_dbus_service.pyo
/usr/lib/python3.8/site-packages/__pycache__/presage_dbus_service.cpython-38.opt-1.pyc
/usr/lib/python3.8/site-packages/__pycache__/presage_dbus_service.cpython-38.pyc
%endif
%if %{fedora} > 32
/usr/lib/python3.9/site-packages/presage_dbus_service.py
/usr/lib/python3.9/site-packages/presage_dbus_service.pyc
/usr/lib/python3.9/site-packages/presage_dbus_service.pyo
/usr/lib/python3.9/site-packages/__pycache__/presage_dbus_service.cpython-39.opt-1.pyc
/usr/lib/python3.9/site-packages/__pycache__/presage_dbus_service.cpython-39.pyc
%endif

%{_bindir}/presage_dbus_service
%{_bindir}/presage_demo_text
%{_bindir}/presage_simulator
%{_bindir}/text2ngram
%{_bindir}/presage_dbus_python_demo
%{_libdir}/libpresage.so.1
%{_libdir}/libpresage.so.1.1.1
%{_libdir}/libpresage.a
%{_libdir}/libpresage.la

%files devel
%{_libdir}/libpresage.so
%{_includedir}/*
