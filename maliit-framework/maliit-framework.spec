Name:       maliit-framework
Version:    0.99.1
Release:    2%{?dist}
Summary:    Maliit Input Method Framework
License:    LGPL 2
URL:        https://github.com/maliit/framework
Source0:    maliit-framework_0.99.1+git20151118+62bd54b.orig.tar.gz
Patch0:     0001-rename_input_context.patch
Patch1:     0002-ubuntu_session_type.patch
Patch2:     0003-fix_activationlostevent.patch
Patch3:     0004-testability.patch
Patch4:     0005-fix_orientation_to_angle_mapping.patch
Patch5:     0006-use_host_prefix_for_mkspecs.patch
Patch6:     0007-update-input-region-when-hiding.patch
Patch7:     0008-fix-building-with-g++-4.9.patch
Patch8:     0009-fix-orientation-updates.patch
Patch9:     0010-fix-focus-changes.patch
Patch10:    0011-check-testability-environment-variable.patch
Patch11:    0012-revert-6e2f8b4253f219688d31002aebf010eeabf0079b.patch
Patch12:    0013-qt_override_server_address.patch
Patch13:    0014-obey-unity8-focus.patch
Patch14:    0015-compose-input-platform-in-maliit.patch
Patch15:    0016-set-plugin-path-from-environment.patch
Patch16:    0017-maliit-config-path-environment-variable.patch
Patch17:    0018-focus-object-can-be-null.patch
Provides:   maliit-framework = 2:%{version}-%{release}
Obsoletes:  maliit-framework <= 2:0.94.2-21   # Important: We set the Obsoletes release to 4 to be higher than the previous Release: 3%{?dist}

BuildRequires: glib2-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtwayland-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(libudev)

%description
Maliit Input Method Framework
Maliit provides a flexible and cross-platform input method framework. It has a
plugin-based client-server architecture where applications act as clients and
communicate with the Maliit server via input context plugins. The communication
link currently uses D-Bus. Maliit is an open source framework (LGPL 2) with
open source plugins (BSD).


%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n maliit-framework-0.99.1+git20151118+62bd54b

%build
# DO NOT CREATE "build" directory!! Because it screws up release paths in the build
qmake-qt5 \
      "CONFIG+=qt5-inputcontext glib debug nodoc warn_off notests c++11 nostrip" \
      M_IM_PREFIX=/usr \
      MALIIT_SERVER_ARGUMENTS="-software -bypass-wm-hint" \
      MALIIT_DEFAULT_PLUGIN=libmaliit-keyboard-plugin.so \
      PREFIX=/usr LIBDIR=/usr/lib64 \
      "QMAKE_CXXFLAGS_RELEASE=-g3 -Og" \
      .
make

%install
make INSTALL_ROOT=%{buildroot} install STRIP=/bin/true

%files
%{_bindir}/maliit-server
%{_bindir}/maliit-exampleapp-plainqt
%{_libdir}/libmaliit-plugins.so.0
%{_libdir}/libmaliit-glib.so.0
%{_libdir}/libmaliit-glib.so.0.99
%{_libdir}/libmaliit-plugins.so.0.99
%{_libdir}/libmaliit-glib.so.0.99.1
%{_libdir}/libmaliit-plugins.so.0.99.1
%{_libdir}/qt5/plugins/platforminputcontexts/libmaliitphabletplatforminputcontextplugin.so

%files devel
%{_includedir}/maliit/maliit-glib
%{_includedir}/maliit/plugins/maliit/plugins
%{_includedir}/maliit/server/
%{_includedir}/maliit/framework/maliit/
%{_libdir}/pkgconfig/maliit-server.pc
%{_libdir}/pkgconfig/maliit-framework.pc
%{_libdir}/pkgconfig/maliit-glib.pc
%{_libdir}/pkgconfig/maliit-plugins.pc
%{_libdir}/maliit-framework-tests/plugins/examples/*
%{_libdir}/libmaliit-glib.so
%{_libdir}/libmaliit-plugins.so
%{_libdir}/qt5/mkspecs/features/maliit-defines.prf
%{_libdir}/qt5/mkspecs/features/maliit-framework.prf
%{_libdir}/qt5/mkspecs/features/maliit-plugins.prf
