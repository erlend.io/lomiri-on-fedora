# Checkout:
git clone https://github.com/ubports/hfd-service.git

# Systemd patch
wget https://github.com/ubports/hfd-service/pull/11.patch
git apply 11.patch

# Dependencies
sudo dnf install qt5-mobility-feedback-devel

# build
mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBEXECDIR=lib -DCMAKE_INSTALL_SYSCONFDIR=/etc -DENABLE_LIBHYBRIS=off
make

# Mock install
mkdir ../mockinstall
make DESTDIR=../mockinstall install

