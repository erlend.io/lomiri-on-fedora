Name:       lomiri-hfd
Version:    1.0
Release:    1%{?dist}
Summary:    FIXME
License:    FIXME
URL:        https://github.com/ubports/hfd-service
Source0:    https://github.com/ubports/hfd-service/archive/896ef75ddf3084733061ab3a110205a44ee6ec2c/hfd-service.tar.gz
Patch0:     11.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: lomiri-cmake-extras
BuildRequires: qt5-mobility-feedback-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: pkgconfig(libudev)
BuildRequires: pkgconfig(systemd)

%description

%prep
%autosetup -p1 -n hfd-service-896ef75ddf3084733061ab3a110205a44ee6ec2c

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBEXECDIR=lib -DCMAKE_INSTALL_SYSCONFDIR=/etc -DENABLE_LIBHYBRIS=off ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
cd build
%make_install
cd ..

%files
%{_sysconfdir}/dbus-1/system.d/com.lomiri.hfd.conf
%{_sysconfdir}/init/hfd-service.conf
/usr/lib/systemd/system/hfd-service.service
%{_bindir}/hfd-service-tools-leds
%{_bindir}/hfd-service
%{_bindir}/hfd-service-tools-vibrator
%{_libdir}/qt5/plugins/feedback/libqtfeedback_hfd.so
%{_libdir}/qt5/qml/Hfd/libhfd-qml.so
%{_libdir}/qt5/qml/Hfd/qmldir
