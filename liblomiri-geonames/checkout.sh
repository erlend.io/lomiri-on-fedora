#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/geonames.git
cd geonames
cat $SRC/0001-remove-AM_V_GEN-which-does-not-work-and-is-unneccess.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "remove-AM_V_GEN-which-does-not-work-and-is-unneccess"
cat $SRC/0002-change-name-from-tree_index-to-object_index.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "change-name-from-tree_index-to-object_index"
