Name:       liblomiri-geonames
Version:    1.0
Release:    2%{?dist}
Summary:    Parse and query the geonames database dump
License:    FIXME
URL:        https://github.com/ubports/geonames
Source0:    https://github.com/ubports/geonames/archive/5d77e827cd8800e9738f0e8ac2f865a59a9f68b2/geonames.tar.gz
Source1:    https://erlend.io/repo/alternateNames.txt
Patch0:     0001-remove-AM_V_GEN-which-does-not-work-and-is-unneccess.patch
Patch1:     0002-change-name-from-tree_index-to-object_index.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gcc-c++
BuildRequires: glib2-devel
BuildRequires: gtk-doc
BuildRequires: libtool

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Parse and query the geonames database dump
A library for parsing and querying a local copy of the geonames.org database.
.
This package contains the shared libraries.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n geonames-5d77e827cd8800e9738f0e8ac2f865a59a9f68b2
cp %{SOURCE1} data/

%build
./autogen.sh
./configure --enable-gtk-doc --prefix=/usr
make

%install
mkdir -p %{buildroot}%{_includedir}/geonames
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/gtk-doc/html/geonames
cp src/geonames.h                                            %{buildroot}%{_includedir}/geonames
ln -s libgeonames.so.0.0.0 %{buildroot}%{_libdir}/libgeonames.so
ln -s libgeonames.so.0.0.0 %{buildroot}%{_libdir}/libgeonames.so.0
install -p -m 755 src/.libs/libgeonames.so.0.0.0             %{buildroot}%{_libdir}
cp src/geonames.pc                                           %{buildroot}%{_libdir}/pkgconfig
cp doc/reference/html/annotation-glossary.html               %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/api-index-full.html                    %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/ch01.html                              %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/geonames-Geonames.html                 %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/geonames.devhelp2                      %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/home.png                               %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/index.html                             %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/left-insensitive.png                   %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/left.png                               %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/object-tree.html                       %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/right-insensitive.png                  %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/right.png                              %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/style.css                              %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/up-insensitive.png                     %{buildroot}/usr/share/gtk-doc/html/geonames
cp doc/reference/html/up.png                                 %{buildroot}/usr/share/gtk-doc/html/geonames

# Not Found:/usr/share/gtk-doc/html/geonames/deprecated-api-index.html


%files
%{_libdir}/libgeonames.so.0
%{_libdir}/libgeonames.so.0.0.0

%files devel
%{_includedir}/geonames/geonames.h
%{_libdir}/libgeonames.so
%{_libdir}/pkgconfig/geonames.pc
/usr/share/gtk-doc/html/geonames/annotation-glossary.html
/usr/share/gtk-doc/html/geonames/api-index-full.html
/usr/share/gtk-doc/html/geonames/ch01.html
/usr/share/gtk-doc/html/geonames/geonames-Geonames.html
/usr/share/gtk-doc/html/geonames/geonames.devhelp2
/usr/share/gtk-doc/html/geonames/home.png
/usr/share/gtk-doc/html/geonames/index.html
/usr/share/gtk-doc/html/geonames/left-insensitive.png
/usr/share/gtk-doc/html/geonames/left.png
/usr/share/gtk-doc/html/geonames/object-tree.html
/usr/share/gtk-doc/html/geonames/right-insensitive.png
/usr/share/gtk-doc/html/geonames/right.png
/usr/share/gtk-doc/html/geonames/style.css
/usr/share/gtk-doc/html/geonames/up-insensitive.png
/usr/share/gtk-doc/html/geonames/up.png
