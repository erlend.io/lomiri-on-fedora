Name:       liblomiri-trust-store
Version:    1.0
Release:    %{version}%{?dist}
Summary:    C++11 library for persisting trust requests - runtime library
License:    FIXME
URL:        https://github.com/ubports/trust-store
Source0:    https://github.com/ubports/trust-store/archive/7ab3ddd8920ff80154177632cf65f7c166315371/trust-store.tar.gz
Patch0:     0001-removing-tests-and-werror.patch
Patch1:     0002-removing-apparmor-dependency.patch
Patch2:     0003-add-include-file-functional.patch

%description
C++11 library for persisting trust requests - runtime library
Provides a common implementation of a trust store to be used by trusted
helpers.
.
This package includes the trust-store runtime libraries.



%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

BuildRequires: pkgconfig(libglog)
BuildRequires: pkgconfig(process-cpp)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(dbus-cpp)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(sqlite3)

%prep
%autosetup -p1 -n trust-store-7ab3ddd8920ff80154177632cf65f7c166315371

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make -O -j8 V=1 VERBOSE=1

%install
mkdir -p %{buildroot}%{_includedir}/core/trust
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/doc/libtrust-store-dev
install -p -m 755 build/src/libtrust-store.so.2.0.0 %{buildroot}%{_libdir}
install -p -m 755 build/src/libtrust-store.so.2 %{buildroot}%{_libdir}
cp include/core/trust/agent.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/resolve.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/tagged_integer.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/expose.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/visibility.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/request.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/dbus_agent.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/store.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/cached_agent.h %{buildroot}%{_includedir}/core/trust
cp include/core/trust/mir_agent.h %{buildroot}%{_includedir}/core/trust
cp build/data/trust-store.pc %{buildroot}%{_libdir}/pkgconfig
cp debian/copyright %{buildroot}/usr/share/doc/libtrust-store-dev
install -p -m 755 build/src/libtrust-store.so %{buildroot}%{_libdir}

# Not Found:/usr/share/doc/libtrust-store-dev/changelog.Debian.gz


%files
%{_libdir}/libtrust-store.so.2.0.0
%{_libdir}/libtrust-store.so.2

%files devel
%{_includedir}/core/trust/agent.h
%{_includedir}/core/trust/resolve.h
%{_includedir}/core/trust/tagged_integer.h
%{_includedir}/core/trust/expose.h
%{_includedir}/core/trust/visibility.h
%{_includedir}/core/trust/request.h
%{_includedir}/core/trust/dbus_agent.h
%{_includedir}/core/trust/store.h
%{_includedir}/core/trust/cached_agent.h
%{_includedir}/core/trust/mir_agent.h
%{_libdir}/pkgconfig/trust-store.pc
/usr/share/doc/libtrust-store-dev/copyright
%{_libdir}/libtrust-store.so

