Name:       liblomiri-qmenumodel
Version:    1.0
Release:    1%{?dist}
Summary:    Qt binding for GMenuModel - shared library
License:    LGPL-3
URL:        https://github.com/ubports/qmenumodel
Source0:    https://github.com/ubports/qmenumodel/archive/f2367433aef6e289eb527e342e48b21392dde843/qmenumodel.tar.gz
Patch0:     0001-add-debuginfo.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gio-2.0)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Qt binding for GMenuModel - shared library
Qt binding for GMenuModel that allows connecting to a menu model exposed on
D-Bus and presents it as a list model. It can be used to expose indicator or
application menus for applications using the Qt framework.
.
This package contains the shared library required by applications using
QMenuModel.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n qmenumodel-f2367433aef6e289eb527e342e48b21392dde843

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_SKIP_RPATH=YES ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/qmenumodel
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_libdir}/qt5/qml/QMenuModel
cp libqmenumodel/src/actionstateparser.h                     %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/dbus-enums.h                            %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/qdbusactiongroup.h                      %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/qdbusmenumodel.h                        %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/qdbusobject.h                           %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/qmenumodel.h                            %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/qstateaction.h                          %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/unitymenuaction.h                       %{buildroot}%{_includedir}/qmenumodel
cp libqmenumodel/src/unitymenumodel.h                        %{buildroot}%{_includedir}/qmenumodel
ln -s libqmenumodel.so.0 %{buildroot}%{_libdir}/libqmenumodel.so
ln -s libqmenumodel.so.0.1.1 %{buildroot}%{_libdir}/libqmenumodel.so.0
install -p -m 755 build/libqmenumodel/src/libqmenumodel.so.0.1.1 %{buildroot}%{_libdir}
cp build/libqmenumodel/src/qmenumodel.pc                     %{buildroot}%{_libdir}/pkgconfig
install -p -m 755 build/libqmenumodel/QMenuModel/libqmenumodel-qml.so %{buildroot}%{_libdir}/qt5/qml/QMenuModel
cp build/libqmenumodel/QMenuModel/qmldir                     %{buildroot}%{_libdir}/qt5/qml/QMenuModel

%files
%{_libdir}/libqmenumodel.so.0
%{_libdir}/libqmenumodel.so.0.1.1

%files devel
%{_includedir}/qmenumodel/actionstateparser.h
%{_includedir}/qmenumodel/dbus-enums.h
%{_includedir}/qmenumodel/qdbusactiongroup.h
%{_includedir}/qmenumodel/qdbusmenumodel.h
%{_includedir}/qmenumodel/qdbusobject.h
%{_includedir}/qmenumodel/qmenumodel.h
%{_includedir}/qmenumodel/qstateaction.h
%{_includedir}/qmenumodel/unitymenuaction.h
%{_includedir}/qmenumodel/unitymenumodel.h
%{_libdir}/libqmenumodel.so
%{_libdir}/pkgconfig/qmenumodel.pc
%{_libdir}/qt5/qml/QMenuModel/libqmenumodel-qml.so
%{_libdir}/qt5/qml/QMenuModel/qmldir
