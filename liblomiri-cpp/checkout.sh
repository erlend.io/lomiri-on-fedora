#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
wget https://launchpad.net/ubuntu/+archive/primary/+files/process-cpp_3.0.1.orig.tar.gz
tar -xvf process-cpp_3.0.1.orig.tar.gz
rm process-cpp_3.0.1.orig.tar.gz
cd process-cpp-3.0.1
cat $SRC/process.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "process"
cd $CHECKOUTDIR
wget https://launchpad.net/ubuntu/+archive/primary/+files/persistent-cache-cpp_1.0.4+16.10.20160823.orig.tar.gz
mkdir persistent-cache-cpp
cd persistent-cache-cpp
tar -xvf ../persistent-cache-cpp_1.0.4+16.10.20160823.orig.tar.gz
cd ..
rm persistent-cache-cpp_1.0.4+16.10.20160823.orig.tar.gz
cd persistent-cache-cpp
cat $SRC/0001-persistent-cache-cpp.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "persistent-cache-cpp"
cat $SRC/0002-added-fpic-to-fix-linker-error.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "added-fpic-to-fix-linker-error"
cd $CHECKOUTDIR
wget https://launchpad.net/ubuntu/+archive/primary/+files/properties-cpp_0.0.1+14.10.20140730.orig.tar.gz
tar -xvf properties-cpp_0.0.1+14.10.20140730.orig.tar.gz
rm properties-cpp_0.0.1+14.10.20140730.orig.tar.gz
cd properties-cpp-0.0.1+14.10.20140730
cat $SRC/properties.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "properties"
