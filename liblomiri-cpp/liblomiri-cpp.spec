Name:       liblomiri-cpp
Version:    1.0
Release:    1%{?dist}
Summary:    Contains the debian packages needed to build unity8
License:    FIXME
URL:        https://launchpad.net/ubuntu/+archive/primary/+files/process-cpp_3.0.1.orig.tar.gz
Source0:    https://launchpad.net/ubuntu/+archive/primary/+files/process-cpp_3.0.1.orig.tar.gz
Source1:    https://launchpad.net/ubuntu/+archive/primary/+files/persistent-cache-cpp_1.0.4+16.10.20160823.orig.tar.gz
Source2:    https://launchpad.net/ubuntu/+archive/primary/+files/properties-cpp_0.0.1+14.10.20140730.orig.tar.gz
Patch0:     process.patch
Patch1:     0001-persistent-cache-cpp.patch
Patch2:     0002-added-fpic-to-fix-linker-error.patch
Patch3:     properties.patch

BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: leveldb-devel

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Contains the debian packages needed to build unity8
Packages: libprocess-cpp3, libprocess-cpp-dev, persistent-cache-cpp-dev, libproperties-cpp-dev

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
if [ -e "process-cpp-3.0.1" ]; then
   rm -fR process-cpp-3.0.1
fi
tar -xvf %SOURCE0
cd process-cpp-3.0.1
cat %PATCH0 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cd ..
if [ -e "persistent-cache-cpp" ]; then
  rm -fR persistent-cache-cpp
fi
mkdir persistent-cache-cpp
cd persistent-cache-cpp
tar -xvf %SOURCE1
cat %PATCH1 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH2 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cd ..
if [ -e "properties-cpp-0.0.1+14.10.20140730" ]; then
   rm -fR properties-cpp-0.0.1+14.10.20140730
fi
tar -xvf %SOURCE2
cd properties-cpp-0.0.1+14.10.20140730
cat %PATCH3 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cd ..

%build
cd process-cpp-3.0.1
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd ..
cd persistent-cache-cpp
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd ..
cd properties-cpp-0.0.1+14.10.20140730
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/core/posix/linux/proc/process
mkdir -p %{buildroot}%{_includedir}/core/testing
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cd process-cpp-3.0.1
cp include/core/posix/child_process.h                        %{buildroot}%{_includedir}/core/posix
cp include/core/posix/exec.h                                 %{buildroot}%{_includedir}/core/posix
cp include/core/posix/exit.h                                 %{buildroot}%{_includedir}/core/posix
cp include/core/posix/fork.h                                 %{buildroot}%{_includedir}/core/posix
cp include/core/posix/linux/proc/process/oom_adj.h           %{buildroot}%{_includedir}/core/posix/linux/proc/process
cp include/core/posix/linux/proc/process/oom_score.h         %{buildroot}%{_includedir}/core/posix/linux/proc/process
cp include/core/posix/linux/proc/process/oom_score_adj.h     %{buildroot}%{_includedir}/core/posix/linux/proc/process
cp include/core/posix/linux/proc/process/stat.h              %{buildroot}%{_includedir}/core/posix/linux/proc/process
cp include/core/posix/linux/proc/process/state.h             %{buildroot}%{_includedir}/core/posix/linux/proc/process
cp include/core/posix/process.h                              %{buildroot}%{_includedir}/core/posix
cp include/core/posix/process_group.h                        %{buildroot}%{_includedir}/core/posix
cp include/core/posix/signal.h                               %{buildroot}%{_includedir}/core/posix
cp include/core/posix/signalable.h                           %{buildroot}%{_includedir}/core/posix
cp include/core/posix/standard_stream.h                      %{buildroot}%{_includedir}/core/posix
cp include/core/posix/this_process.h                         %{buildroot}%{_includedir}/core/posix
cp include/core/posix/visibility.h                           %{buildroot}%{_includedir}/core/posix
cp include/core/posix/wait.h                                 %{buildroot}%{_includedir}/core/posix
cp include/core/testing/cross_process_sync.h                 %{buildroot}%{_includedir}/core/testing
cp include/core/testing/fork_and_run.h                       %{buildroot}%{_includedir}/core/testing
ln -s libprocess-cpp.so.3 %{buildroot}%{_libdir}/libprocess-cpp.so
ln -s libprocess-cpp.so.3.0.0 %{buildroot}%{_libdir}/libprocess-cpp.so.3
install -p -m 755 build/src/libprocess-cpp.so.3.0.0          %{buildroot}%{_libdir}
cp build/data/process-cpp.pc                                 %{buildroot}%{_libdir}/pkgconfig
cd ..
cd persistent-cache-cpp
cp include/core/cache_codec.h                                %{buildroot}%{_includedir}/core
cp include/core/cache_discard_policy.h                       %{buildroot}%{_includedir}/core
cp include/core/cache_events.h                               %{buildroot}%{_includedir}/core
cp include/core/optional.h                                   %{buildroot}%{_includedir}/core
cp include/core/persistent_cache.h                           %{buildroot}%{_includedir}/core
cp include/core/persistent_cache_stats.h                     %{buildroot}%{_includedir}/core
cp include/core/persistent_string_cache.h                    %{buildroot}%{_includedir}/core
cp build/src/core/libpersistent-cache-cpp.a                  %{buildroot}%{_libdir}
cp build/data/libpersistent-cache-cpp.pc                     %{buildroot}%{_libdir}/pkgconfig
cd ..
cd properties-cpp-0.0.1+14.10.20140730
cp include/core/connection.h                                 %{buildroot}%{_includedir}/core
cp include/core/property.h                                   %{buildroot}%{_includedir}/core
cp include/core/signal.h                                     %{buildroot}%{_includedir}/core
cp build/data/properties-cpp.pc                              %{buildroot}%{_libdir}/pkgconfig
cd ..

%files
%{_libdir}/libprocess-cpp.so.3
%{_libdir}/libprocess-cpp.so.3.0.0

%files devel
%{_includedir}/core/cache_codec.h
%{_includedir}/core/cache_discard_policy.h
%{_includedir}/core/cache_events.h
%{_includedir}/core/connection.h
%{_includedir}/core/optional.h
%{_includedir}/core/persistent_cache.h
%{_includedir}/core/persistent_cache_stats.h
%{_includedir}/core/persistent_string_cache.h
%{_includedir}/core/posix/child_process.h
%{_includedir}/core/posix/exec.h
%{_includedir}/core/posix/exit.h
%{_includedir}/core/posix/fork.h
%{_includedir}/core/posix/linux/proc/process/oom_adj.h
%{_includedir}/core/posix/linux/proc/process/oom_score.h
%{_includedir}/core/posix/linux/proc/process/oom_score_adj.h
%{_includedir}/core/posix/linux/proc/process/stat.h
%{_includedir}/core/posix/linux/proc/process/state.h
%{_includedir}/core/posix/process.h
%{_includedir}/core/posix/process_group.h
%{_includedir}/core/posix/signal.h
%{_includedir}/core/posix/signalable.h
%{_includedir}/core/posix/standard_stream.h
%{_includedir}/core/posix/this_process.h
%{_includedir}/core/posix/visibility.h
%{_includedir}/core/posix/wait.h
%{_includedir}/core/property.h
%{_includedir}/core/signal.h
%{_includedir}/core/testing/cross_process_sync.h
%{_includedir}/core/testing/fork_and_run.h
%{_libdir}/libpersistent-cache-cpp.a
%{_libdir}/libprocess-cpp.so
%{_libdir}/pkgconfig/libpersistent-cache-cpp.pc
%{_libdir}/pkgconfig/process-cpp.pc
%{_libdir}/pkgconfig/properties-cpp.pc
