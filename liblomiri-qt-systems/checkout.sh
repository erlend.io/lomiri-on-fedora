#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/qtsystems-opensource-src.git
cd qtsystems-opensource-src
cat $SRC/0001-dummy-patch.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "dummy-patch"
cat $SRC/0002-fix-bug-in-getnetworkname.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fix-bug-in-getnetworkname"
