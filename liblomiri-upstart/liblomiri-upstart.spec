Name:       liblomiri-upstart
Version:    1.0
Release:    1%{?dist}
Summary:    Upstart Client Library
License:    FIXME
URL:        https://github.com/ubports/upstart
Source0:    https://github.com/ubports/upstart/archive/47a2d086b8c1758e1ca4f91f52fad18a21222cab/upstart.tar.gz
Patch0:     0001-disable-init-util-extra.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: dbus-devel
BuildRequires: gcc-c++
BuildRequires: gettext-devel
BuildRequires: libnih-devel
BuildRequires: libtool
BuildRequires: pkgconfig(json-c)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Upstart Client Library
The libupstart library allows access to services provided by the Upstart init
daemon without having to use low-level D-Bus calls.
.
This package contains the shared library.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n upstart-47a2d086b8c1758e1ca4f91f52fad18a21222cab

%build
autoreconf -i
./configure --prefix=/usr
make VERBOSE=1

%install
mkdir -p %{buildroot}%{_includedir}/upstart
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp lib/upstart.h                                             %{buildroot}%{_includedir}
cp lib/upstart/com.ubuntu.Upstart.Instance.h                 %{buildroot}%{_includedir}/upstart
cp lib/upstart/com.ubuntu.Upstart.Job.h                      %{buildroot}%{_includedir}/upstart
cp lib/upstart/com.ubuntu.Upstart.h                          %{buildroot}%{_includedir}/upstart
cp lib/upstart/upstart-dbus.h                                %{buildroot}%{_includedir}/upstart
cp lib/.libs/libupstart.a                                    %{buildroot}%{_libdir}
ln -s libupstart.so.1.0.0 %{buildroot}%{_libdir}/libupstart.so
ln -s libupstart.so.1.0.0 %{buildroot}%{_libdir}/libupstart.so.1
install -p -m 755 lib/.libs/libupstart.so.1.0.0              %{buildroot}%{_libdir}
cp lib/libupstart.pc                                         %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/libupstart.so.1
%{_libdir}/libupstart.so.1.0.0

%files devel
%{_includedir}/upstart.h
%{_includedir}/upstart/com.ubuntu.Upstart.Instance.h
%{_includedir}/upstart/com.ubuntu.Upstart.Job.h
%{_includedir}/upstart/com.ubuntu.Upstart.h
%{_includedir}/upstart/upstart-dbus.h
%{_libdir}/libupstart.a
%{_libdir}/libupstart.so
%{_libdir}/pkgconfig/libupstart.pc
