Name:       liblomiri-dbus-test
Version:    0.9
Release:    1%{?dist}
Summary:    Runs tests under a new DBus session
License:    GPL-3
URL:        https://github.com/ubports/dbus-test-runner
Source0:    https://github.com/ubports/dbus-test-runner/archive/86d63d119566974bd841cacd3202599b3b1a845d/dbus-test-runner.tar.gz
Patch0:     0001-removing-werror.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gcc-c++
BuildRequires: gnome-common
BuildRequires: intltool
BuildRequires: pkgconfig(dbus-glib-1)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Runs tests under a new DBus session
A simple little executable for running a couple of programs under a
new DBus session.
.
This package contains shared libraries.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n dbus-test-runner-86d63d119566974bd841cacd3202599b3b1a845d

%build
./autogen.sh
./configure --prefix=/usr
make

%install
mkdir -p %{buildroot}%{_includedir}/libdbustest-1/libdbustest
mkdir -p %{buildroot}%{_libdir}/dbus-test-runner
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/dbus-test-runner
cp libdbustest/bustle.h                                      %{buildroot}%{_includedir}/libdbustest-1/libdbustest
cp libdbustest/dbus-mock.h                                   %{buildroot}%{_includedir}/libdbustest-1/libdbustest
cp libdbustest/dbus-test.h                                   %{buildroot}%{_includedir}/libdbustest-1/libdbustest
cp libdbustest/process.h                                     %{buildroot}%{_includedir}/libdbustest-1/libdbustest
cp libdbustest/service.h                                     %{buildroot}%{_includedir}/libdbustest-1/libdbustest
cp libdbustest/task.h                                        %{buildroot}%{_includedir}/libdbustest-1/libdbustest
cp libdbustest/dbus-test-watchdog                            %{buildroot}%{_libdir}/dbus-test-runner
ln -s libdbustest.so.1.0.0 %{buildroot}%{_libdir}/libdbustest.so
ln -s libdbustest.so.1.0.0 %{buildroot}%{_libdir}/libdbustest.so.1
install -p -m 755 libdbustest/.libs/libdbustest.so.1.0.0     %{buildroot}%{_libdir}
cp libdbustest/dbustest-1.pc                                 %{buildroot}%{_libdir}/pkgconfig
cp libdbustest/dbus-test-bustle-handler                      %{buildroot}/usr/share/dbus-test-runner
cp data/session.conf                                         %{buildroot}/usr/share/dbus-test-runner
cp data/system.conf                                          %{buildroot}/usr/share/dbus-test-runner

%files
%{_libdir}/dbus-test-runner/dbus-test-watchdog
%{_libdir}/libdbustest.so.1
%{_libdir}/libdbustest.so.1.0.0
/usr/share/dbus-test-runner/dbus-test-bustle-handler
/usr/share/dbus-test-runner/session.conf
/usr/share/dbus-test-runner/system.conf

%files devel
%{_includedir}/libdbustest-1/libdbustest/bustle.h
%{_includedir}/libdbustest-1/libdbustest/dbus-mock.h
%{_includedir}/libdbustest-1/libdbustest/dbus-test.h
%{_includedir}/libdbustest-1/libdbustest/process.h
%{_includedir}/libdbustest-1/libdbustest/service.h
%{_includedir}/libdbustest-1/libdbustest/task.h
%{_libdir}/libdbustest.so
%{_libdir}/pkgconfig/dbustest-1.pc
