#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/libqtdbustest.git
cd libqtdbustest
cat $SRC/0001-fixing-EnableCoverageReport-and-GMock-GTest-dependen.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixing-EnableCoverageReport-and-GMock-GTest-dependen"
cat $SRC/0002-attemping-to-solve-rpath-problem-in-linking.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "attemping-to-solve-rpath-problem-in-linking"
cat $SRC/0003-chaning-back-deps.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "chaning-back-deps"
cat $SRC/0004-chaning-back-deps-2.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "chaning-back-deps-2"
cat $SRC/0005-returning-to-org.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "returning-to-org"
cat $SRC/0006-made-compiling-of-runner-optional.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "made-compiling-of-runner-optional"
cat $SRC/0007-jklfjsdlk.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "jklfjsdlk"
cat $SRC/0008-jklfsdsfdjlk.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "jklfsdsfdjlk"
cat $SRC/0009-Comment-in-the-runner.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "Comment-in-the-runner"
