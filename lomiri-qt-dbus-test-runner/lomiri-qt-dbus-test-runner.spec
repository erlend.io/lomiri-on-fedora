Name:       lomiri-qt-dbus-test-runner
Version:    1.0
Release:    1%{?dist}
Summary:    Library for testing DBus interactions using Qt
License:    FIXME
URL:        https://github.com/ubports/libqtdbustest
Source0:    https://github.com/ubports/libqtdbustest/archive/24e410ea77c9fa08894365c60bf08811a3b60bc0/libqtdbustest.tar.gz
Patch0:     0001-fixing-EnableCoverageReport-and-GMock-GTest-dependen.patch
Patch1:     0002-attemping-to-solve-rpath-problem-in-linking.patch
Patch2:     0003-chaning-back-deps.patch
Patch3:     0004-chaning-back-deps-2.patch
Patch4:     0005-returning-to-org.patch
Patch5:     0006-made-compiling-of-runner-optional.patch
Patch6:     0007-jklfjsdlk.patch
Patch7:     0008-jklfsdsfdjlk.patch
Patch8:     0009-Comment-in-the-runner.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gcovr
BuildRequires: gnome-common
BuildRequires: lcov
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: pkgconfig(gmock)
BuildRequires: pkgconfig(gtest)
BuildRequires: pkgconfig(libqtdbustest-1)

%description
Library for testing DBus interactions using Qt
Simple executable for running a test script under a private
DBus environment.

%prep
%autosetup -p1 -n libqtdbustest-24e410ea77c9fa08894365c60bf08811a3b60bc0

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_bindir}
install -p -m 755 build/src/qdbus-test-runner/qdbus-simple-test-runner %{buildroot}%{_bindir}

%files
%{_bindir}/qdbus-simple-test-runner
