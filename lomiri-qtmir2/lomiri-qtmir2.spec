Name:       lomiri-qtmir2
Version:    1.0
Release:    2%{?dist}
Summary:    Qt plugin for Unity specific Mir APIs
License:    FIXME
URL:        https://github.com/ubports/qtmir
Source0:    https://github.com/ubports/qtmir/archive/be21a0e23c95541c6255a90d4c88f37ac862c3ba/qtmir.tar.gz
Patch0:     0001-adding-fixes.patch
Patch1:     0002-add-pthreads-to-qpa-mirserver.patch
Patch2:     0003-attempt-to-fix-missing-context.patch
Patch3:     0004-rename-unity-to-lomiri.patch

%ifarch aarch64
BuildRequires: mir-devel = 1.8.0
%endif
%ifarch x86_64
BuildRequires: mir-devel
%endif
BuildRequires: gsettings-qt-devel = 1.0
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: glm-devel
BuildRequires: liblomiri-api2-devel
BuildRequires: liblomiri-app-launch2-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: liblomiri-qt-dbusmock-devel
BuildRequires: liblomiri-url-dispatcher2-devel
BuildRequires: lomiri-content-hub-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtbase-static
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: qt5-qtsensors-devel
BuildRequires: systemtap-sdt-devel
BuildRequires: pkgconfig(wayland-client)
BuildRequires: pkgconfig(wayland-server)
BuildRequires: pkgconfig(xkbcommon)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(process-cpp)
BuildRequires: pkgconfig(egl)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(lttng-ust)
BuildRequires: pkgconfig(libqtdbustest-1)
BuildRequires: pkgconfig(libqtdbusmock-1)
BuildRequires: pkgconfig(valgrind)
BuildRequires: pkgconfig(fontconfig)

%description
Qt plugin for Unity specific Mir APIs
QtMir provides Qt/QML bindings for Mir features that are exposed through the
qtmir-desktop or qtmir-android QPA plugin such as Application management
(start/stop/suspend/resume) and surface management.
Qt platform abstraction (QPA) plugin for a Mir server (desktop)
QtMir is a set of Qt5 components to enable one to write a Mir server with Qt.
It contains a QPA (Qt Platform Abstraction) plugin which creates and manages
a Mir server. It also exposes some internal Mir functionality.
.
This variant of the package is for GNU-based desktops.

%prep
%autosetup -p1 -n qtmir-be21a0e23c95541c6255a90d4c88f37ac862c3ba

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DNO_TESTS=true ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_libdir}/qt5/plugins/platforms
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Unity/Application
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Unity/Screens
mkdir -p %{buildroot}/usr/share/glib-2.0/schemas
install -p -m 755 build/src/platforms/mirserver/libqpa-mirserver.so %{buildroot}%{_libdir}/qt5/plugins/platforms
install -p -m 755 build/src/modules/Unity/Application/libunityapplicationplugin.so %{buildroot}%{_libdir}/qt5/qml/Unity/Application
cp src/modules/Unity/Application/qmldir                      %{buildroot}%{_libdir}/qt5/qml/Unity/Application
install -p -m 755 build/src/modules/Unity/Screens/libunityscreensplugin.so %{buildroot}%{_libdir}/qt5/qml/Unity/Screens
cp src/modules/Unity/Screens/qmldir                          %{buildroot}%{_libdir}/qt5/qml/Unity/Screens
cp src/modules/Unity/Application/com.canonical.qtmir.gschema.xml %{buildroot}/usr/share/glib-2.0/schemas

%files
%{_libdir}/qt5/plugins/platforms/libqpa-mirserver.so
%{_libdir}/qt5/qml/Unity/Application/libunityapplicationplugin.so
%{_libdir}/qt5/qml/Unity/Application/qmldir
%{_libdir}/qt5/qml/Unity/Screens/libunityscreensplugin.so
%{_libdir}/qt5/qml/Unity/Screens/qmldir
/usr/share/glib-2.0/schemas/com.canonical.qtmir.gschema.xml
