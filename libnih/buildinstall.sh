#!/bin/bash
# exit when any command fails
set -e

# Source setup
if [ "$#" -eq 0 ]; then
    source ../settings/env
else
    source ./$1
fi

# Get package list
rpm -qa > $TEMP_FILE

function is_installed()
{
for pkg in $(cat $TEMP_FILE)
do
    if [ $pkg = $1 ]; then
        return 0
    fi
done
return 1
}

cd $RPMBUILD_DIR/SPECS
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/libnih-1.0.3-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : libnih
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/libnih/libnih.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/libnih/*.patch      $RPMBUILD_DIR/SOURCES
    spectool -g -R libnih.spec
    # echo $PASSWORD | sudo -S dnf install -y intltool qt5-qtbase-devel qt5-qtdeclarative-devel glib2-devel python3-devel cmake gcc-c++
    rpmbuild -ba -vv libnih.spec
fi

if is_installed libnih-1.0.3-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed libnih
else
    echo installing:libnih
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/libnih-1.0.3-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed libnih-devel-1.0.3-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed libnih-devel
else
    echo installing:libnih-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/libnih-devel-1.0.3-1.$FEDORA_VERSION.$ARCH.rpm
fi
