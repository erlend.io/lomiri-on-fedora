#!/bin/bash

# exit when any command fails
set -e

# Source setup
source ../settings/env

echo ---------------------------------------------------------------
echo Building : libnih
echo ---------------------------------------------------------------

cp *.patch $RPMBUILD_DIR/SOURCES
spectool -g -R libnih.spec
rpmbuild -bs libnih.spec
mock -r fedora-33-aarch64 --rebuild --no-clean $RPMBUILD_DIR/SRPMS/libnih-1.0.3-1.fc32.src.rpm
cp $RESULTDIR/*.src.rpm     $RPMBUILD_DIR/SRPMS/
cp $RESULTDIR/*.aarch64.rpm $RPMBUILD_DIR/RPMS/aarch64
