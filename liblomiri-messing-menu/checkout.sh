#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/indicator-messages.git
cd indicator-messages
cat $SRC/0001-disabling-werror.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "disabling-werror"
