Name:       liblomiri-messing-menu
Version:    1.0
Release:    1%{?dist}
Summary:    Messaging Menu - shared library
License:    FIXME
URL:        https://github.com/ubports/indicator-messages
Source0:    https://github.com/ubports/indicator-messages/archive/634a57c513d483aba3fbe5bee9c215e856e2e1f7/indicator-messages.tar.gz
Patch0:     0001-disabling-werror.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gcc-c++
BuildRequires: gnome-common
BuildRequires: gobject-introspection-devel
BuildRequires: gtk-doc
BuildRequires: intltool
BuildRequires: liblomiri-dbus-test-devel
BuildRequires: vala
BuildRequires: pkgconfig(dbustest-1)
BuildRequires: pkgconfig(accountsservice)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Messaging Menu - shared library
This library contains information to build status providers to go into
the messaging menu.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n indicator-messages-634a57c513d483aba3fbe5bee9c215e856e2e1f7

%build
./autogen.sh
make

%install
mkdir -p %{buildroot}%{_includedir}/messaging-menu
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/gir-1.0
mkdir -p %{buildroot}/usr/share/gtk-doc/html/messaging-menu
mkdir -p %{buildroot}/usr/share/vala/vapi
cp libmessaging-menu/messaging-menu-app.h                    %{buildroot}%{_includedir}/messaging-menu
cp libmessaging-menu/messaging-menu-message.h                %{buildroot}%{_includedir}/messaging-menu
cp libmessaging-menu/messaging-menu.h                        %{buildroot}%{_includedir}/messaging-menu
ln -s libmessaging-menu.so.0.0.0 %{buildroot}%{_libdir}/libmessaging-menu.so
ln -s libmessaging-menu.so.0.0.0 %{buildroot}%{_libdir}/libmessaging-menu.so.0
install -p -m 755 libmessaging-menu/.libs/libmessaging-menu.so.0.0.0 %{buildroot}%{_libdir}
cp libmessaging-menu/messaging-menu.pc                       %{buildroot}%{_libdir}/pkgconfig
cp libmessaging-menu/MessagingMenu-1.0.gir                   %{buildroot}/usr/share/gir-1.0
cp libmessaging-menu/MessagingMenu-1.0.vapi                  %{buildroot}/usr/share/vala/vapi

# Not Found:/usr/share/gtk-doc/html/messaging-menu/MessagingMenuApp.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/MessagingMenuMessage.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/annotation-glossary.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/api-index-full.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/ch01.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/deprecated-api-index.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/home.png
# Not Found:/usr/share/gtk-doc/html/messaging-menu/index.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/left-insensitive.png
# Not Found:/usr/share/gtk-doc/html/messaging-menu/left.png
# Not Found:/usr/share/gtk-doc/html/messaging-menu/messaging-menu.devhelp2
# Not Found:/usr/share/gtk-doc/html/messaging-menu/object-tree.html
# Not Found:/usr/share/gtk-doc/html/messaging-menu/right-insensitive.png
# Not Found:/usr/share/gtk-doc/html/messaging-menu/right.png
# Not Found:/usr/share/gtk-doc/html/messaging-menu/style.css
# Not Found:/usr/share/gtk-doc/html/messaging-menu/up-insensitive.png
# Not Found:/usr/share/gtk-doc/html/messaging-menu/up.png


%files
%{_libdir}/libmessaging-menu.so.0
%{_libdir}/libmessaging-menu.so.0.0.0

%files devel
%{_includedir}/messaging-menu/messaging-menu-app.h
%{_includedir}/messaging-menu/messaging-menu-message.h
%{_includedir}/messaging-menu/messaging-menu.h
%{_libdir}/libmessaging-menu.so
%{_libdir}/pkgconfig/messaging-menu.pc
/usr/share/gir-1.0/MessagingMenu-1.0.gir
/usr/share/vala/vapi/MessagingMenu-1.0.vapi
