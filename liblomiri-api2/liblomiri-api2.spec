Name:       liblomiri-api2
Version:    0.1
Release:    1%{?dist}
Summary:    API for Lomiri shell integration
License:    LGPL-3
URL:        https://gitlab.com/ubports/core/lomiri-api
Source0:    https://gitlab.com/ubports/core/lomiri-api/-/archive/a207c222c7c82db90ce826068aa451d715d28217/lomiri-api.tar.gz
Patch0:     0001-change-name-of-library-directory.patch

BuildRequires: cmake
BuildRequires: cppcheck
BuildRequires: doxygen
BuildRequires: gcc-c++
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: lomiri-cmake-extras
BuildRequires: valgrind
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(libqtdbustest-1)

%description
API for Lomiri shell integration
Library to integrate with the Lomiri shell

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n lomiri-api-a207c222c7c82db90ce826068aa451d715d28217

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DNO_TESTS=ON ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
cd build
%make_install

%files
%{_libdir}/liblomiri-api.so.0
%{_libdir}/liblomiri-api.so.0.1

%files devel
%{_datadir}/doc/liblomiri-api/*
%{_datadir}/doc/liblomiri-api/search/*
%{_libdir}/liblomiri-api.so
%{_libdir}/pkgconfig/lomiri-shell-application.pc
%{_libdir}/pkgconfig/lomiri-shell-launcher.pc
%{_libdir}/pkgconfig/liblomiri-api.pc
%{_libdir}/pkgconfig/lomiri-shell-notifications.pc
%{_libdir}/pkgconfig/lomiri-shell-api.pc
%{_includedir}/lomiri/*
%{_includedir}/lomiri/shell/application/*
%{_includedir}/lomiri/shell/notifications/*
%{_includedir}/lomiri/shell/launcher/*
%{_includedir}/lomiri/api/*
%{_includedir}/lomiri/util/*
