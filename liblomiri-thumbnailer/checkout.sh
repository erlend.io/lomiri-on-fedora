#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/thumbnailer.git
cd thumbnailer
cat $SRC/0001-disable-apparmor.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "disable-apparmor"
cat $SRC/0002-fix-rpath.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fix-rpath"
cat $SRC/0003-fixed-rpath-in-qml-plugin.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixed-rpath-in-qml-plugin"
cat $SRC/0004-fixed-linker-error-multiple-definition-do_close.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixed-linker-error-multiple-definition-do_close"
cat $SRC/0005-change-to-inline.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "change-to-inline"
cat $SRC/0006-removed-werror.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removed-werror"
