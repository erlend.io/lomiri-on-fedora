Name:       liblomiri-thumbnailer
Version:    1.0
Release:    1%{?dist}
Summary:    transitional dummy package for QML interface for the thumbnailer.
License:    FIXME
URL:        https://github.com/ubports/thumbnailer
Source0:    https://github.com/ubports/thumbnailer/archive/a4387b8ff1e70261d73020e19d28af2136a68946/thumbnailer.tar.gz
Patch0:     0001-disable-apparmor.patch
Patch1:     0002-fix-rpath.patch
Patch2:     0003-fixed-rpath-in-qml-plugin.patch
Patch3:     0004-fixed-linker-error-multiple-definition-do_close.patch
Patch4:     0005-change-to-inline.patch
Patch5:     0006-removed-werror.patch

BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: leveldb-devel
BuildRequires: liblomiri-api-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: valgrind
BuildRequires: pkgconfig(gstreamer-1.0)
BuildRequires: pkgconfig(gstreamer-plugins-base-1.0)
BuildRequires: pkgconfig(gstreamer-tag-1.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(gdk-pixbuf-2.0)
BuildRequires: pkgconfig(libexif)
BuildRequires: pkgconfig(libunity-api)
BuildRequires: pkgconfig(taglib)
BuildRequires: pkgconfig(libpersistent-cache-cpp)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(libqtdbustest-1)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
transitional dummy package for QML interface for the thumbnailer.
This package provides image providers that allow access to the
thumbnailer from Qt Quick 2 / QML applications
.
This is a transitional dummy package for qml-module-ubuntu-thumbnailer0.1
which can be safely removed.
QML interface for the thumbnailer.
This package provides image providers that allow access to the
thumbnailer from Qt Quick 2 / QML applications.
Qt/C++ API to obtain thumbnails
Library to obtain thumbnails

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n thumbnailer-a4387b8ff1e70261d73020e19d28af2136a68946

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_SKIP_RPATH=YES ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/thumbnailer-qt-1.0/unity/thumbnailer/qt
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Thumbnailer.0.1
mkdir -p %{buildroot}/usr/share/lintian/overrides
cp include/unity/thumbnailer/qt/thumbnailer-qt.h             %{buildroot}%{_includedir}/thumbnailer-qt-1.0/unity/thumbnailer/qt
ln -s libthumbnailer-qt.so.1.0 %{buildroot}%{_libdir}/libthumbnailer-qt.so
ln -s libthumbnailer-qt.so.1.0.0 %{buildroot}%{_libdir}/libthumbnailer-qt.so.1.0
install -p -m 755 build/src/libthumbnailer-qt/libthumbnailer-qt.so.1.0.0 %{buildroot}%{_libdir}
cp build/src/libthumbnailer-qt/libthumbnailer-qt.pc          %{buildroot}%{_libdir}/pkgconfig
install -p -m 755 build/plugins/Ubuntu/Thumbnailer.0.1/libthumbnailer-qml.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Thumbnailer.0.1
cp plugins/Ubuntu/Thumbnailer.0.1/qmldir                     %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Thumbnailer.0.1

# Not Found:/usr/share/lintian/overrides/qml-module-ubuntu-thumbnailer0.1


%files
%{_libdir}/libthumbnailer-qt.so.1.0
%{_libdir}/libthumbnailer-qt.so.1.0.0
%{_libdir}/qt5/qml/Ubuntu/Thumbnailer.0.1/libthumbnailer-qml.so
%{_libdir}/qt5/qml/Ubuntu/Thumbnailer.0.1/qmldir

%files devel
%{_includedir}/thumbnailer-qt-1.0/unity/thumbnailer/qt/thumbnailer-qt.h
%{_libdir}/libthumbnailer-qt.so
%{_libdir}/pkgconfig/libthumbnailer-qt.pc
