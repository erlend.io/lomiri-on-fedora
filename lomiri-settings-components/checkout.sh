#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/settings-components.git
cd settings-components
cat $SRC/0001-adding-cmake-FindQmlPlugins.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-cmake-FindQmlPlugins"
cat $SRC/0002-set-path-to-qmlscanner.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "set-path-to-qmlscanner"
cat $SRC/0003-adding-dep.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-dep"
cat $SRC/0004-changing-add-gui.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "changing-add-gui"
