Name:       liblomiri-app-launch
Version:    1.0
Release:    1%{?dist}
Summary:    library for sending requests to the ubuntu app launch
License:    FIXME
URL:        https://github.com/ubports/ubuntu-app-launch
Source0:    https://github.com/ubports/ubuntu-app-launch/archive/9ffa5f76711d600c10c20795cdaeb2c0abfa7cde/ubuntu-app-launch.tar.gz
Patch0:     0001-Remove-upstart.patch
Patch1:     0002-removing-libcgmanager.patch
Patch2:     0003-adding-pkgconfig-for-gtest.patch
Patch3:     0004-disable-build-test.patch
Patch4:     0005-removing-werror.patch
Patch5:     0006-adding-click.patch
Patch6:     0007-fixing-app-lanuch.patch
Patch7:     0008-fixed-extern-c-overreach.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gobject-introspection-devel
BuildRequires: liblomiri-click-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-dbus-test-devel
BuildRequires: liblomiri-libertine-devel
BuildRequires: liblomiri-upstart-devel
BuildRequires: libnih-devel
BuildRequires: systemtap-sdt-devel
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(json-glib-1.0)
BuildRequires: pkgconfig(zeitgeist-2.0)
BuildRequires: pkgconfig(click-0.4)
BuildRequires: pkgconfig(libupstart)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(dbustest-1)
BuildRequires: pkgconfig(lttng-ust)
BuildRequires: pkgconfig(mirclient)
BuildRequires: pkgconfig(libertine)
BuildRequires: pkgconfig(libcurl)
BuildRequires: pkgconfig(gtest)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
library for sending requests to the ubuntu app launch
Upstart Job file and associated utilities that is used to launch
applications in a standard and confined way.
.
This package contains shared libraries to be used by applications.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n ubuntu-app-launch-9ffa5f76711d600c10c20795cdaeb2c0abfa7cde

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/gir-1.0
cp libubuntu-app-launch/ubuntu-app-launch.h                  %{buildroot}%{_includedir}/libubuntu-app-launch-2
cp libubuntu-app-launch/appid.h                              %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch
cp libubuntu-app-launch/application.h                        %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch
cp libubuntu-app-launch/helper.h                             %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch
cp libubuntu-app-launch/oom.h                                %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch
cp libubuntu-app-launch/registry.h                           %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch
cp libubuntu-app-launch/type-tagger.h                        %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch
ln -s libubuntu-app-launch.so.3 %{buildroot}%{_libdir}/libubuntu-app-launch.so
cp build/libubuntu-app-launch/ubuntu-app-launch-2.pc         %{buildroot}%{_libdir}/pkgconfig
cp build/libubuntu-app-launch/UbuntuAppLaunch-2.gir          %{buildroot}/usr/share/gir-1.0
mkdir -p %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/
cp libubuntu-app-launch/application.h %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/
cp libubuntu-app-launch/appid.h       %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/
cp libubuntu-app-launch/oom.h         %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/
cp libubuntu-app-launch/registry.h    %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/
cp libubuntu-app-launch/helper.h      %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/
cp libubuntu-app-launch/type-tagger.h %{buildroot}%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/
cp build/libubuntu-app-launch/libubuntu-app-launch.so.3 %{buildroot}%{_libdir}
cp build/libubuntu-app-launch/libubuntu-app-launch.so.3.0.0 %{buildroot}%{_libdir}

# Not Found:/usr/lib/aarch64-linux-gnu/libubuntu-app-launch.so.2
# Not Found:/usr/lib/aarch64-linux-gnu/libubuntu-app-launch.so.2.0.0


%files
%{_libdir}/libubuntu-app-launch.so.3
%{_libdir}/libubuntu-app-launch.so.3.0.0

%files devel
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/appid.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/application.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/helper.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/oom.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/registry.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/type-tagger.h
%{_libdir}/libubuntu-app-launch.so
%{_libdir}/pkgconfig/ubuntu-app-launch-2.pc
/usr/share/gir-1.0/UbuntuAppLaunch-2.gir
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/application.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/appid.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/oom.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/registry.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/helper.h
%{_includedir}/libubuntu-app-launch-2/ubuntu-app-launch/type-tagger.h
