#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial_-_edge https://github.com/ubports/ubuntu-app-launch.git
cd ubuntu-app-launch
cat $SRC/0001-Remove-upstart.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "Remove-upstart"
cat $SRC/0002-removing-libcgmanager.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removing-libcgmanager"
cat $SRC/0003-adding-pkgconfig-for-gtest.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-pkgconfig-for-gtest"
cat $SRC/0004-disable-build-test.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "disable-build-test"
cat $SRC/0005-removing-werror.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "removing-werror"
cat $SRC/0006-adding-click.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-click"
cat $SRC/0007-fixing-app-lanuch.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixing-app-lanuch"
cat $SRC/0008-fixed-extern-c-overreach.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fixed-extern-c-overreach"
