Name:       qt5-systems
Version:    5.0
Release:    2%{?dist}
Summary:    Qt Systems module - system info
License:    FIXME
URL:        https://github.com/ubports/qtsystems-opensource-src
Source0:    qtsystems.tar.gz
Patch0:     0001-dummy-patch.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtdeclarative-devel

%description
Qt Systems module - system info
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the System Info part of the Qt Systems module.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt Systems module - system info
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the System Info part of the Qt Systems module.
change in a binary incompatible way, and no guarantees are given.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n qtsystems

%build
mkdir .git
mkdir build
cd build
qmake-qt5 ..
%make_build

%install
mkdir -p %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
mkdir -p %{buildroot}%{_includedir}/qt5/QtServiceFramework
mkdir -p %{buildroot}%{_includedir}/qt5/QtSystemInfo
mkdir -p %{buildroot}%{_libdir}/cmake/Qt5PublishSubscribe
mkdir -p %{buildroot}%{_libdir}/cmake/Qt5ServiceFramework
mkdir -p %{buildroot}%{_libdir}/cmake/Qt5SystemInfo
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_libdir}/qt5/mkspecs/modules
mkdir -p %{buildroot}%{_libdir}/qt5/qml/QtPublishSubscribe
mkdir -p %{buildroot}%{_libdir}/qt5/qml/QtServiceFramework
mkdir -p %{buildroot}%{_libdir}/qt5/qml/QtSystemInfo
cp build/include/QtPublishSubscribe/QValueSpacePublisher     %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp build/include/QtPublishSubscribe/QValueSpaceSubscriber    %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp build/include/QtPublishSubscribe/QtPublishSubscribe       %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp build/include/QtPublishSubscribe/QtPublishSubscribeDepends %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp build/include/QtPublishSubscribe/QtPublishSubscribeVersion %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp src/publishsubscribe/qpublishsubscribeglobal.h            %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp build/include/QtPublishSubscribe/qtpublishsubscribeversion.h %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp src/publishsubscribe/qvaluespace.h                        %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp src/publishsubscribe/qvaluespacepublisher.h               %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp src/publishsubscribe/qvaluespacesubscriber.h              %{buildroot}%{_includedir}/qt5/QtPublishSubscribe
cp build/include/QtServiceFramework/QRemoteServiceRegister   %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QServiceClientCredentials %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QServiceFilter           %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QServiceInterfaceDescriptor %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QServiceManager          %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QServicePluginInterface  %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QServiceReply            %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QServiceReplyBase        %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QtServiceFramework       %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QtServiceFrameworkDepends %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/QtServiceFrameworkVersion %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qremoteserviceregister.h             %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qservice.h                           %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qserviceclientcredentials.h          %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qservicefilter.h                     %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qserviceframeworkglobal.h            %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qserviceinterfacedescriptor.h        %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qservicemanager.h                    %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qserviceplugininterface.h            %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp src/serviceframework/qservicereply.h                      %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtServiceFramework/qtserviceframeworkversion.h %{buildroot}%{_includedir}/qt5/QtServiceFramework
cp build/include/QtSystemInfo/QBatteryInfo                   %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QDeviceInfo                    %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QInputDevice                   %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QInputInfoManager              %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QNetworkInfo                   %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QScreenSaver                   %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QtSystemInfo                   %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QtSystemInfoDepends            %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/QtSystemInfoVersion            %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp src/systeminfo/qbatteryinfo.h                             %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp src/systeminfo/qdeviceinfo.h                              %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp src/systeminfo/qinputinfo.h                               %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp src/systeminfo/qnetworkinfo.h                             %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp src/systeminfo/qscreensaver.h                             %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp src/systeminfo/qsysteminfoglobal.h                        %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/include/QtSystemInfo/qtsysteminfoversion.h          %{buildroot}%{_includedir}/qt5/QtSystemInfo
cp build/lib/cmake/Qt5PublishSubscribe/Qt5PublishSubscribeConfig.cmake %{buildroot}%{_libdir}/cmake/Qt5PublishSubscribe
cp build/lib/cmake/Qt5PublishSubscribe/Qt5PublishSubscribeConfigVersion.cmake %{buildroot}%{_libdir}/cmake/Qt5PublishSubscribe
cp build/lib/cmake/Qt5ServiceFramework/Qt5ServiceFrameworkConfig.cmake %{buildroot}%{_libdir}/cmake/Qt5ServiceFramework
cp build/lib/cmake/Qt5ServiceFramework/Qt5ServiceFrameworkConfigVersion.cmake %{buildroot}%{_libdir}/cmake/Qt5ServiceFramework
cp build/lib/cmake/Qt5SystemInfo/Qt5SystemInfoConfig.cmake   %{buildroot}%{_libdir}/cmake/Qt5SystemInfo
cp build/lib/cmake/Qt5SystemInfo/Qt5SystemInfoConfigVersion.cmake %{buildroot}%{_libdir}/cmake/Qt5SystemInfo
cp build/lib/libQt5PublishSubscribe.prl                      %{buildroot}%{_libdir}
ln -s libQt5PublishSubscribe.so.5.4.0 %{buildroot}%{_libdir}/libQt5PublishSubscribe.so
ln -s libQt5PublishSubscribe.so.5.4.0 %{buildroot}%{_libdir}/libQt5PublishSubscribe.so.5
ln -s libQt5PublishSubscribe.so.5.4.0 %{buildroot}%{_libdir}/libQt5PublishSubscribe.so.5.4
install -p -m 755 build/lib/libQt5PublishSubscribe.so.5.4.0  %{buildroot}%{_libdir}
cp build/lib/libQt5ServiceFramework.prl                      %{buildroot}%{_libdir}
ln -s libQt5ServiceFramework.so.5.4.0 %{buildroot}%{_libdir}/libQt5ServiceFramework.so
ln -s libQt5ServiceFramework.so.5.4.0 %{buildroot}%{_libdir}/libQt5ServiceFramework.so.5
ln -s libQt5ServiceFramework.so.5.4.0 %{buildroot}%{_libdir}/libQt5ServiceFramework.so.5.4
install -p -m 755 build/lib/libQt5ServiceFramework.so.5.4.0  %{buildroot}%{_libdir}
cp build/lib/libQt5SystemInfo.prl                            %{buildroot}%{_libdir}
ln -s libQt5SystemInfo.so.5.4.0 %{buildroot}%{_libdir}/libQt5SystemInfo.so
ln -s libQt5SystemInfo.so.5.4.0 %{buildroot}%{_libdir}/libQt5SystemInfo.so.5
ln -s libQt5SystemInfo.so.5.4.0 %{buildroot}%{_libdir}/libQt5SystemInfo.so.5.4
install -p -m 755 build/lib/libQt5SystemInfo.so.5.4.0        %{buildroot}%{_libdir}
cp build/lib/pkgconfig/Qt5PublishSubscribe.pc                %{buildroot}%{_libdir}/pkgconfig
cp build/lib/pkgconfig/Qt5ServiceFramework.pc                %{buildroot}%{_libdir}/pkgconfig
cp build/lib/pkgconfig/Qt5SystemInfo.pc                      %{buildroot}%{_libdir}/pkgconfig
cp build/mkspecs/modules-inst/qt_lib_publishsubscribe.pri    %{buildroot}%{_libdir}/qt5/mkspecs/modules
cp build/mkspecs/modules-inst/qt_lib_publishsubscribe_private.pri %{buildroot}%{_libdir}/qt5/mkspecs/modules
cp build/mkspecs/modules-inst/qt_lib_serviceframework.pri    %{buildroot}%{_libdir}/qt5/mkspecs/modules
cp build/mkspecs/modules-inst/qt_lib_serviceframework_private.pri %{buildroot}%{_libdir}/qt5/mkspecs/modules
cp build/mkspecs/modules-inst/qt_lib_systeminfo.pri          %{buildroot}%{_libdir}/qt5/mkspecs/modules
cp build/mkspecs/modules-inst/qt_lib_systeminfo_private.pri  %{buildroot}%{_libdir}/qt5/mkspecs/modules
install -p -m 755 build/qml/QtPublishSubscribe/libdeclarative_publishsubscribe.so %{buildroot}%{_libdir}/qt5/qml/QtPublishSubscribe
cp build/qml/QtPublishSubscribe/plugins.qmltypes             %{buildroot}%{_libdir}/qt5/qml/QtPublishSubscribe
cp build/qml/QtPublishSubscribe/qmldir                       %{buildroot}%{_libdir}/qt5/qml/QtPublishSubscribe
install -p -m 755 build/qml/QtServiceFramework/libdeclarative_serviceframework.so %{buildroot}%{_libdir}/qt5/qml/QtServiceFramework
cp build/qml/QtServiceFramework/plugins.qmltypes             %{buildroot}%{_libdir}/qt5/qml/QtServiceFramework
cp build/qml/QtServiceFramework/qmldir                       %{buildroot}%{_libdir}/qt5/qml/QtServiceFramework
install -p -m 755 build/qml/QtSystemInfo/libdeclarative_systeminfo.so %{buildroot}%{_libdir}/qt5/qml/QtSystemInfo
cp build/qml/QtSystemInfo/plugins.qmltypes                   %{buildroot}%{_libdir}/qt5/qml/QtSystemInfo
cp build/qml/QtSystemInfo/qmldir                             %{buildroot}%{_libdir}/qt5/qml/QtSystemInfo

%files
%{_libdir}/libQt5PublishSubscribe.so.5
%{_libdir}/libQt5PublishSubscribe.so.5.4
%{_libdir}/libQt5PublishSubscribe.so.5.4.0
%{_libdir}/libQt5ServiceFramework.so.5
%{_libdir}/libQt5ServiceFramework.so.5.4
%{_libdir}/libQt5ServiceFramework.so.5.4.0
%{_libdir}/libQt5SystemInfo.so.5
%{_libdir}/libQt5SystemInfo.so.5.4
%{_libdir}/libQt5SystemInfo.so.5.4.0
%{_libdir}/qt5/qml/QtPublishSubscribe/libdeclarative_publishsubscribe.so
%{_libdir}/qt5/qml/QtPublishSubscribe/plugins.qmltypes
%{_libdir}/qt5/qml/QtPublishSubscribe/qmldir
%{_libdir}/qt5/qml/QtServiceFramework/libdeclarative_serviceframework.so
%{_libdir}/qt5/qml/QtServiceFramework/plugins.qmltypes
%{_libdir}/qt5/qml/QtServiceFramework/qmldir
%{_libdir}/qt5/qml/QtSystemInfo/libdeclarative_systeminfo.so
%{_libdir}/qt5/qml/QtSystemInfo/plugins.qmltypes
%{_libdir}/qt5/qml/QtSystemInfo/qmldir

%files devel
%{_includedir}/qt5/QtPublishSubscribe/QValueSpacePublisher
%{_includedir}/qt5/QtPublishSubscribe/QValueSpaceSubscriber
%{_includedir}/qt5/QtPublishSubscribe/QtPublishSubscribe
%{_includedir}/qt5/QtPublishSubscribe/QtPublishSubscribeDepends
%{_includedir}/qt5/QtPublishSubscribe/QtPublishSubscribeVersion
%{_includedir}/qt5/QtPublishSubscribe/qpublishsubscribeglobal.h
%{_includedir}/qt5/QtPublishSubscribe/qtpublishsubscribeversion.h
%{_includedir}/qt5/QtPublishSubscribe/qvaluespace.h
%{_includedir}/qt5/QtPublishSubscribe/qvaluespacepublisher.h
%{_includedir}/qt5/QtPublishSubscribe/qvaluespacesubscriber.h
%{_includedir}/qt5/QtServiceFramework/QRemoteServiceRegister
%{_includedir}/qt5/QtServiceFramework/QServiceClientCredentials
%{_includedir}/qt5/QtServiceFramework/QServiceFilter
%{_includedir}/qt5/QtServiceFramework/QServiceInterfaceDescriptor
%{_includedir}/qt5/QtServiceFramework/QServiceManager
%{_includedir}/qt5/QtServiceFramework/QServicePluginInterface
%{_includedir}/qt5/QtServiceFramework/QServiceReply
%{_includedir}/qt5/QtServiceFramework/QServiceReplyBase
%{_includedir}/qt5/QtServiceFramework/QtServiceFramework
%{_includedir}/qt5/QtServiceFramework/QtServiceFrameworkDepends
%{_includedir}/qt5/QtServiceFramework/QtServiceFrameworkVersion
%{_includedir}/qt5/QtServiceFramework/qremoteserviceregister.h
%{_includedir}/qt5/QtServiceFramework/qservice.h
%{_includedir}/qt5/QtServiceFramework/qserviceclientcredentials.h
%{_includedir}/qt5/QtServiceFramework/qservicefilter.h
%{_includedir}/qt5/QtServiceFramework/qserviceframeworkglobal.h
%{_includedir}/qt5/QtServiceFramework/qserviceinterfacedescriptor.h
%{_includedir}/qt5/QtServiceFramework/qservicemanager.h
%{_includedir}/qt5/QtServiceFramework/qserviceplugininterface.h
%{_includedir}/qt5/QtServiceFramework/qservicereply.h
%{_includedir}/qt5/QtServiceFramework/qtserviceframeworkversion.h
%{_includedir}/qt5/QtSystemInfo/QBatteryInfo
%{_includedir}/qt5/QtSystemInfo/QDeviceInfo
%{_includedir}/qt5/QtSystemInfo/QInputDevice
%{_includedir}/qt5/QtSystemInfo/QInputInfoManager
%{_includedir}/qt5/QtSystemInfo/QNetworkInfo
%{_includedir}/qt5/QtSystemInfo/QScreenSaver
%{_includedir}/qt5/QtSystemInfo/QtSystemInfo
%{_includedir}/qt5/QtSystemInfo/QtSystemInfoDepends
%{_includedir}/qt5/QtSystemInfo/QtSystemInfoVersion
%{_includedir}/qt5/QtSystemInfo/qbatteryinfo.h
%{_includedir}/qt5/QtSystemInfo/qdeviceinfo.h
%{_includedir}/qt5/QtSystemInfo/qinputinfo.h
%{_includedir}/qt5/QtSystemInfo/qnetworkinfo.h
%{_includedir}/qt5/QtSystemInfo/qscreensaver.h
%{_includedir}/qt5/QtSystemInfo/qsysteminfoglobal.h
%{_includedir}/qt5/QtSystemInfo/qtsysteminfoversion.h
%{_libdir}/cmake/Qt5PublishSubscribe/Qt5PublishSubscribeConfig.cmake
%{_libdir}/cmake/Qt5PublishSubscribe/Qt5PublishSubscribeConfigVersion.cmake
%{_libdir}/cmake/Qt5ServiceFramework/Qt5ServiceFrameworkConfig.cmake
%{_libdir}/cmake/Qt5ServiceFramework/Qt5ServiceFrameworkConfigVersion.cmake
%{_libdir}/cmake/Qt5SystemInfo/Qt5SystemInfoConfig.cmake
%{_libdir}/cmake/Qt5SystemInfo/Qt5SystemInfoConfigVersion.cmake
%{_libdir}/libQt5PublishSubscribe.prl
%{_libdir}/libQt5PublishSubscribe.so
%{_libdir}/libQt5ServiceFramework.prl
%{_libdir}/libQt5ServiceFramework.so
%{_libdir}/libQt5SystemInfo.prl
%{_libdir}/libQt5SystemInfo.so
%{_libdir}/pkgconfig/Qt5PublishSubscribe.pc
%{_libdir}/pkgconfig/Qt5ServiceFramework.pc
%{_libdir}/pkgconfig/Qt5SystemInfo.pc
%{_libdir}/qt5/mkspecs/modules/qt_lib_publishsubscribe.pri
%{_libdir}/qt5/mkspecs/modules/qt_lib_publishsubscribe_private.pri
%{_libdir}/qt5/mkspecs/modules/qt_lib_serviceframework.pri
%{_libdir}/qt5/mkspecs/modules/qt_lib_serviceframework_private.pri
%{_libdir}/qt5/mkspecs/modules/qt_lib_systeminfo.pri
%{_libdir}/qt5/mkspecs/modules/qt_lib_systeminfo_private.pri
