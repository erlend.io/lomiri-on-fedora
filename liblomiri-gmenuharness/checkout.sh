#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/gmenuharness.git
cd gmenuharness
cat $SRC/0001-fix-compiliation-errors.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "fix-compiliation-errors"
cat $SRC/0002-adding-include-files.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-include-files"
