Name:       liblomiri-gmenuharness
Version:    1.0
Release:    1%{?dist}
Summary:    GMenu harness library
License:    FIXME
URL:        https://github.com/ubports/gmenuharness
Source0:    https://github.com/ubports/gmenuharness/archive/dd11bfbdc0efac14a957321944a67865e6de4903/gmenuharness.tar.gz
Patch0:     0001-fix-compiliation-errors.patch
Patch1:     0002-adding-include-files.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: liblomiri-api-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(libunity-api)
BuildRequires: pkgconfig(gmock)
BuildRequires: pkgconfig(gtest)
BuildRequires: pkgconfig(libqtdbustest-1)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
GMenu harness library
Library containing GMenu harness

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n gmenuharness-dd11bfbdc0efac14a957321944a67865e6de4903

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/gmenuharness-0.1/unity/gmenuharness
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp include/unity/gmenuharness/MatchResult.h                  %{buildroot}%{_includedir}/gmenuharness-0.1/unity/gmenuharness
cp include/unity/gmenuharness/MatchUtils.h                   %{buildroot}%{_includedir}/gmenuharness-0.1/unity/gmenuharness
cp include/unity/gmenuharness/MenuItemMatcher.h              %{buildroot}%{_includedir}/gmenuharness-0.1/unity/gmenuharness
cp include/unity/gmenuharness/MenuMatcher.h                  %{buildroot}%{_includedir}/gmenuharness-0.1/unity/gmenuharness
ln -s libgmenuharness.so.0.1 %{buildroot}%{_libdir}/libgmenuharness.so
ln -s libgmenuharness.so.0.1.0 %{buildroot}%{_libdir}/libgmenuharness.so.0.1
install -p -m 755 build/src/libgmenuharness.so.0.1.0         %{buildroot}%{_libdir}
cp build/src/libgmenuharness.pc                              %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/libgmenuharness.so.0.1
%{_libdir}/libgmenuharness.so.0.1.0

%files devel
%{_includedir}/gmenuharness-0.1/unity/gmenuharness/MatchResult.h
%{_includedir}/gmenuharness-0.1/unity/gmenuharness/MatchUtils.h
%{_includedir}/gmenuharness-0.1/unity/gmenuharness/MenuItemMatcher.h
%{_includedir}/gmenuharness-0.1/unity/gmenuharness/MenuMatcher.h
%{_libdir}/libgmenuharness.so
%{_libdir}/pkgconfig/libgmenuharness.pc
