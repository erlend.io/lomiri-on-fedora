Name:       lomiri-qml-settings-components
Version:    1.0
Release:    1%{?dist}
Summary:    transitional dummy package for Ubuntu settings components
License:    FIXME
URL:        https://github.com/ubports/settings-components
Source0:    https://github.com/ubports/settings-components/archive/91a0fb8203d244b3e9479dc38e00f8efbf249fa9/settings-components.tar.gz
Patch0:     0001-adding-cmake-FindQmlPlugins.patch
Patch1:     0002-set-path-to-qmlscanner.patch
Patch2:     0003-adding-dep.patch
Patch3:     0004-changing-add-gui.patch

BuildRequires: accountsservice-devel
BuildRequires: apt-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gettext-devel
BuildRequires: gsettings-qt-devel
BuildRequires: liblomiri-thumbnailer-devel
BuildRequires: polkit-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: upower-devel
BuildRequires: pkgconfig(QtGui)

%description
transitional dummy package for Ubuntu settings components
Ubuntu settings components for Unity8
.
This is a transitional dummy package which can be safely removed.
Ubuntu settings Components
Ubuntu settings components for Unity8

%prep
%autosetup -p1 -n settings-components-91a0fb8203d244b3e9479dc38e00f8efbf249fa9

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DENABLE_TESTS=OFF ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd build
make qmltypes

%install
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Openvpn
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Pptp
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
mkdir -p %{buildroot}/usr/share/locale/ar/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/be/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ca/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/cy/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/de/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/el/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/es/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gd/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ia/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/id/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/it/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nb/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt_BR/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ru/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/tr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/uk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_CN/LC_MESSAGES
cp plugins/Ubuntu/Settings/Components/ActionTextField.qml    %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
cp plugins/Ubuntu/Settings/Components/Calendar.js            %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
cp plugins/Ubuntu/Settings/Components/Calendar.qml           %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
cp plugins/Ubuntu/Settings/Components/MessageHeader.qml      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
cp plugins/Ubuntu/Settings/Components/UbuntuShapeForItem.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
cp plugins/Ubuntu/Settings/Components/dateExt.js             %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
install -p -m 755 build/plugins/Ubuntu/Settings/Components/libUbuntuSettingsComponentsQml.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
cp plugins/Ubuntu/Settings/Components/qmldir                 %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Components
cp plugins/Ubuntu/Settings/Fingerprint/CircularSegment.qml   %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/DirectionVisual.qml   %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/Fingerprint.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/FingerprintVisual.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/Fingerprints.qml      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/SegmentBoundingBoxes.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/SegmentRenderer.qml   %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/SegmentedImage.qml    %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/Setup.qml             %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/StatusLabel.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
install -p -m 755 build/plugins/Ubuntu/Settings/Fingerprint/libUbuntuSettingsFingerprintQml.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Fingerprint/qmldir                %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint
cp plugins/Ubuntu/Settings/Menus/AccessPointMenu.qml         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/BaseLayoutMenu.qml          %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/BaseMenu.qml                %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/ButtonMenu.qml              %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/CalendarMenu.qml            %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/CheckableMenu.qml           %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/EventMenu.qml               %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/GroupedMessageMenu.qml      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/MediaPlayerMenu.qml         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/ModemInfoItem.qml           %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/PlaybackButton.qml          %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/PlaybackItemMenu.qml        %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/ProgressBarMenu.qml         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/ProgressValueMenu.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/RadioMenu.qml               %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/SectionMenu.qml             %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/SeparatorMenu.qml           %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/SimpleMessageMenu.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/SliderMenu.qml              %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/SnapDecisionMenu.qml        %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/StandardMenu.qml            %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/Style/BaseStyle.qml         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style
cp plugins/Ubuntu/Settings/Menus/Style/MenuPaddings.qml      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style
cp plugins/Ubuntu/Settings/Menus/Style/PointerStyle.qml      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style
cp plugins/Ubuntu/Settings/Menus/Style/TouchStyle.qml        %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style
cp plugins/Ubuntu/Settings/Menus/Style/qmldir                %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style
cp plugins/Ubuntu/Settings/Menus/StyledSlotsLayout.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/SwitchMenu.qml              %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/TextMessageMenu.qml         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/TimeZoneMenu.qml            %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/TransferMenu.qml            %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/UserSessionMenu.qml         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
install -p -m 755 build/plugins/Ubuntu/Settings/Menus/libUbuntuSettingsMenusQml.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Menus/qmldir                      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Menus
cp plugins/Ubuntu/Settings/Vpn/DialogFile.qml                %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/DialogFileProperties.qml      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/FileSelector.qml              %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/Openvpn/AuthTypeField.qml     %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Openvpn
cp plugins/Ubuntu/Settings/Vpn/Openvpn/Editor.qml            %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Openvpn
cp plugins/Ubuntu/Settings/Vpn/Openvpn/Preview.qml           %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Openvpn
cp plugins/Ubuntu/Settings/Vpn/Pptp/Editor.qml               %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Pptp
cp plugins/Ubuntu/Settings/Vpn/Pptp/Preview.qml              %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Pptp
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/InvalidCert.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/NoCert.qml      %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/SetUpUnused.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/PreviewDialog/SomeTraffic.qml %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog
cp plugins/Ubuntu/Settings/Vpn/VpnEditor.qml                 %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/VpnList.qml                   %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/VpnPreviewDialog.qml          %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/VpnRoutesField.qml            %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/VpnTypeField.qml              %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
install -p -m 755 build/plugins/Ubuntu/Settings/Vpn/libUbuntuSettingsVpn.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp plugins/Ubuntu/Settings/Vpn/qmldir                        %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn
cp build/po/locale/ar/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/ar/LC_MESSAGES
cp build/po/locale/be/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/be/LC_MESSAGES
cp build/po/locale/ca/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/ca/LC_MESSAGES
cp build/po/locale/cy/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/cy/LC_MESSAGES
cp build/po/locale/de/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/de/LC_MESSAGES
cp build/po/locale/el/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/el/LC_MESSAGES
cp build/po/locale/es/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/es/LC_MESSAGES
cp build/po/locale/fi/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/fi/LC_MESSAGES
cp build/po/locale/fr/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/fr/LC_MESSAGES
cp build/po/locale/gd/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/gd/LC_MESSAGES
cp build/po/locale/gl/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/gl/LC_MESSAGES
cp build/po/locale/hu/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/hu/LC_MESSAGES
cp build/po/locale/ia/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/ia/LC_MESSAGES
cp build/po/locale/id/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/id/LC_MESSAGES
cp build/po/locale/it/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/it/LC_MESSAGES
cp build/po/locale/nb/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/nb/LC_MESSAGES
cp build/po/locale/nl/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/nl/LC_MESSAGES
cp build/po/locale/pl/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/pl/LC_MESSAGES
cp build/po/locale/pt/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/pt/LC_MESSAGES
cp build/po/locale/pt_BR/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/pt_BR/LC_MESSAGES
cp build/po/locale/ru/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/ru/LC_MESSAGES
cp build/po/locale/sk/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/sk/LC_MESSAGES
cp build/po/locale/sl/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/sl/LC_MESSAGES
cp build/po/locale/tr/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/tr/LC_MESSAGES
cp build/po/locale/uk/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/uk/LC_MESSAGES
cp build/po/locale/zh_CN/LC_MESSAGES/ubuntu-settings-components.mo %{buildroot}/usr/share/locale/zh_CN/LC_MESSAGES

%files
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/ActionTextField.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/Calendar.js
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/Calendar.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/MessageHeader.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/UbuntuShapeForItem.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/dateExt.js
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/libUbuntuSettingsComponentsQml.so
%{_libdir}/qt5/qml/Ubuntu/Settings/Components/qmldir
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/CircularSegment.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/DirectionVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/Fingerprint.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/FingerprintVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/Fingerprints.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/SegmentBoundingBoxes.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/SegmentRenderer.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/SegmentedImage.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/Setup.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/StatusLabel.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/libUbuntuSettingsFingerprintQml.so
%{_libdir}/qt5/qml/Ubuntu/Settings/Fingerprint/qmldir
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/AccessPointMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/BaseLayoutMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/BaseMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/ButtonMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/CalendarMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/CheckableMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/EventMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/GroupedMessageMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/MediaPlayerMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/ModemInfoItem.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/PlaybackButton.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/PlaybackItemMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/ProgressBarMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/ProgressValueMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/RadioMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/SectionMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/SeparatorMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/SimpleMessageMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/SliderMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/SnapDecisionMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/StandardMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style/BaseStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style/MenuPaddings.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style/PointerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style/TouchStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/Style/qmldir
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/StyledSlotsLayout.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/SwitchMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/TextMessageMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/TimeZoneMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/TransferMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/UserSessionMenu.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/libUbuntuSettingsMenusQml.so
%{_libdir}/qt5/qml/Ubuntu/Settings/Menus/qmldir
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/DialogFile.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/DialogFileProperties.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/FileSelector.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Openvpn/AuthTypeField.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Openvpn/Editor.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Openvpn/Preview.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Pptp/Editor.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/Pptp/Preview.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/InvalidCert.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/NoCert.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/SetUpUnused.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/PreviewDialog/SomeTraffic.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/VpnEditor.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/VpnList.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/VpnPreviewDialog.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/VpnRoutesField.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/VpnTypeField.qml
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/libUbuntuSettingsVpn.so
%{_libdir}/qt5/qml/Ubuntu/Settings/Vpn/qmldir
/usr/share/locale/ar/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/be/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/ca/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/cy/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/de/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/el/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/es/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/fi/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/fr/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/gd/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/gl/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/hu/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/ia/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/id/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/it/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/nb/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/nl/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/pl/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/pt/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/pt_BR/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/ru/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/sk/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/sl/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/tr/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/uk/LC_MESSAGES/ubuntu-settings-components.mo
/usr/share/locale/zh_CN/LC_MESSAGES/ubuntu-settings-components.mo
