# Installing lomri on fedora

The packages are built on:
https://copr.fedorainfracloud.org/coprs/erlend/lomiri-on-fedora/

```
sudo dnf copr enable erlend/lomiri-on-fedora 
sudo dnf update --refresh
```

```
sudo dnf install unity8
```

# Running lomiri
The solution is currently a work in process so it can not be started in the normal way.


```
ssh pine@<your ip>

# Move to root
sudo su

# Stop current gui
systemctl stop lightdm

# Start Miral-shell
export MIR_SERVER_CURSOR=null
export XDG_RUNTIME_DIR=/tmp/
export WAYLAND_DISPLAY=wayland-root
miral-shell&

# Change access to display driver & wayland socket so that unity8 can run non-root
sudo chmod 777 /dev/dri/card0
sudo chmod 666 /tmp/wayland-root

# return to non-root
exit

# Start Lomiri (unity8)
export MIR_SERVER_WAYLAND_HOST=/tmp/wayland-root
export QT_QPA_PLATFORM=wayland
export QT_IM_MODULE=maliitphablet
unity8
```







