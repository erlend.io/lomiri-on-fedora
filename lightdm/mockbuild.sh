#!/bin/bash

# exit when any command fails
set -e

# Source setup
if [ "$#" -eq 0 ]; then
    source ../settings/env
else
    source ./$1
fi

echo ---------------------------------------------------------------
echo Building : lightdm
echo ---------------------------------------------------------------
cp *.conf                $RPMBUILD_DIR/SOURCES
cp *.patch               $RPMBUILD_DIR/SOURCES
cp lightdm-autologin.pam $RPMBUILD_DIR/SOURCES
cp lightdm.logrotate     $RPMBUILD_DIR/SOURCES
cp lightdm.pam           $RPMBUILD_DIR/SOURCES
cp lightdm.rules         $RPMBUILD_DIR/SOURCES
cp lightdm.service       $RPMBUILD_DIR/SOURCES

spectool -g -R lightdm.spec
rpmbuild -bs lightdm.spec
mock -r fedora-33-aarch64 --rebuild $RPMBUILD_DIR/SRPMS/lightdm-1.30.0-10ubports.fc32.src.rpm
cp $RESULTDIR/*.src.rpm     $RPMBUILD_DIR/SRPMS/
cp $RESULTDIR/*.aarch64.rpm $RPMBUILD_DIR/RPMS/aarch64
