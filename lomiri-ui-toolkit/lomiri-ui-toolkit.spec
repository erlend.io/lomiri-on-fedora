Name:       lomiri-ui-toolkit
Version:    0.9
Release:    5%{?dist}
Summary:    Ubuntu metrics library for Ubuntu UI Toolkit
License:    GPL-3
URL:        https://github.com/ubports/ubuntu-ui-toolkit
Source0:    https://github.com/ubports/ubuntu-ui-toolkit/archive/cfa1f5826bb2c17999fdbfacdc521662d2cf06ef/ubuntu-ui-toolkit.tar.gz
Patch0:     0001-no-warns.patch
Patch1:     0002-dpkg.patch

BuildRequires: bluez-libs
BuildRequires: gcc-c++
BuildRequires: libXi-devel
BuildRequires: libnih
BuildRequires: lttng-ust-devel
BuildRequires: qt5-doctools
BuildRequires: qt5-mobility-feedback-devel
BuildRequires: qt5-mobility-pim-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtbase-static
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: qt5-qtgraphicaleffects
BuildRequires: qt5-qtquickcontrols
BuildRequires: qt5-qtquickcontrols2
BuildRequires: qt5-qtsvg-devel
BuildRequires: qt5-systems-devel
BuildRequires: systemtap-sdt-devel
BuildRequires: pkgconfig(lttng-ust)
BuildRequires: pkgconfig(libnih)
BuildRequires: pkgconfig(Qt5Svg)
BuildRequires: pkgconfig(dbus-1)
Requires: qt5-systems

%description
Lomiri metrics library for Ubuntu UI Toolkit
Library for monitoring QtQuick 2 applications
Ubuntu gestures library for Ubuntu UI Toolkit
Library supporting Gesture recognition for Ubuntu UI Toolkit
Classes for SwipeArea, and for Ubuntu Gestures in general.
Ubuntu toolkit common library for Ubuntu UI Toolkit
Contains components and helper classes that are shared between the
Ubuntu QML plugins
Qt Components for Ubuntu - Components QML plugin
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the Ubuntu Components QML plugin.
Qt Components for Ubuntu - Layouts QML plugin
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the Ubuntu Layouts QML plugin.
Qt Components for Ubuntu - Metrics QML plugin
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the Ubuntu Metrics QML plugin.
Qt Components for Ubuntu - PerformanceMetrics QML plugin
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the Ubuntu PerformanceMetrics QML plugin.
Qt Components for Ubuntu - Test QML plugin
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the Ubuntu Test QML plugin.
Transitional dummy package for Ubuntu UI Toolkit QML plugin
This package contains the Ubuntu Components QML plugin.
Qt Components for Ubuntu - startup time profiling tool
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the application startup time profiling tools.
Test package for Ubuntu UI Toolkit
Autopilot tests for the ubuntu-ui-toolkit package
Qt Components for Ubuntu - demos and examples
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the component demos and example applications.
Qt Components for Ubuntu - Ubuntu Theme
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the Ambiance theme for Qt Ubuntu Components.
Qt Components for Ubuntu - productive tools
Qt Components for Ubuntu offers a set of reusable user interface
components for Qt Quick 2 / QML.
.
This package contains the application launcher, which is a drop-in
replacement for qmlscene, oxideqmlscene, qml and derivatives.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: qt5-systems-devel

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n ubuntu-ui-toolkit-cfa1f5826bb2c17999fdbfacdc521662d2cf06ef

%build
mkdir build
cd build
qmake-qt5 QMAKE_CXXFLAGS_RELEASE="-g3 -Og" CONFIG+=no_docs ..
%make_build
cd ..

%install
mkdir -p ./build/qml/Ubuntu/Components/
ln -s ../build/app-launch-profiler/app-launch-tracepoints app-launch-profiler/app-launch-tracepoints
ln -s ../build/ubuntu-ui-toolkit-launcher/ubuntu-ui-toolkit-launcher ubuntu-ui-toolkit-launcher/ubuntu-ui-toolkit-launcher
ln -s ../build/apicheck/apicheck apicheck/apicheck
mkdir -p /tmp/runtime-builduser
chmod 700 /tmp/runtime-builduser
cd build
XDG_RUNTIME_DIR=/tmp/runtime-builduser make INSTALL_ROOT="%{buildroot}" install
mkdir -p %{buildroot}/usr/lib64/qt5/mkspecs/modules/
cp ./mkspecs/modules-inst/* %{buildroot}/usr/lib64/qt5/mkspecs/modules/

%files
%{_datadir}/locale/hi/LC_MESSAGES/*
%{_datadir}/locale/uk/LC_MESSAGES/*
%{_datadir}/locale/da/LC_MESSAGES/*
%{_datadir}/locale/id/LC_MESSAGES/*
%{_datadir}/locale/ca@valencia/LC_MESSAGES/*
%{_datadir}/locale/oc/LC_MESSAGES/*
%{_datadir}/locale/he/LC_MESSAGES/*
%{_datadir}/locale/lt/LC_MESSAGES/*
%{_datadir}/locale/gd/LC_MESSAGES/*
%{_datadir}/locale/es/LC_MESSAGES/*
%{_datadir}/locale/sd/LC_MESSAGES/*
%{_datadir}/locale/en_AU/LC_MESSAGES/*
%{_datadir}/locale/ln/LC_MESSAGES/*
%{_datadir}/locale/sl/LC_MESSAGES/*
%{_datadir}/locale/nl/LC_MESSAGES/*
%{_datadir}/locale/km/LC_MESSAGES/*
%{_datadir}/locale/eu/LC_MESSAGES/*
%{_datadir}/locale/vi/LC_MESSAGES/*
%{_datadir}/locale/fa/LC_MESSAGES/*
%{_datadir}/locale/hu/LC_MESSAGES/*
%{_datadir}/locale/kn/LC_MESSAGES/*
%{_datadir}/locale/ku/LC_MESSAGES/*
%{_datadir}/locale/br/LC_MESSAGES/*
%{_datadir}/locale/ce/LC_MESSAGES/*
%{_datadir}/locale/ga/LC_MESSAGES/*
%{_datadir}/locale/ta/LC_MESSAGES/*
%{_datadir}/locale/my/LC_MESSAGES/*
%{_datadir}/locale/fr_CA/LC_MESSAGES/*
%{_datadir}/locale/lv/LC_MESSAGES/*
%{_datadir}/locale/hr/LC_MESSAGES/*
%{_datadir}/locale/be/LC_MESSAGES/*
%{_datadir}/locale/it/LC_MESSAGES/*
%{_datadir}/locale/sk/LC_MESSAGES/*
%{_datadir}/locale/gl/LC_MESSAGES/*
%{_datadir}/locale/az/LC_MESSAGES/*
%{_datadir}/locale/bs/LC_MESSAGES/*
%{_datadir}/locale/ast/LC_MESSAGES/*
%{_datadir}/locale/et/LC_MESSAGES/*
%{_datadir}/locale/el/LC_MESSAGES/*
%{_datadir}/locale/de/LC_MESSAGES/*
%{_datadir}/locale/cy/LC_MESSAGES/*
%{_datadir}/locale/zh_CN/LC_MESSAGES/*
%{_datadir}/locale/ca/LC_MESSAGES/*
%{_datadir}/locale/ar/LC_MESSAGES/*
%{_datadir}/locale/ru/LC_MESSAGES/*
%{_datadir}/locale/bg/LC_MESSAGES/*
%{_datadir}/locale/zh_HK/LC_MESSAGES/*
%{_datadir}/locale/ug/LC_MESSAGES/*
%{_datadir}/locale/lo/LC_MESSAGES/*
%{_datadir}/locale/tr/LC_MESSAGES/*
%{_datadir}/locale/ko/LC_MESSAGES/*
%{_datadir}/locale/cs/LC_MESSAGES/*
%{_datadir}/locale/en_GB/LC_MESSAGES/*
%{_datadir}/locale/ro/LC_MESSAGES/*
%{_datadir}/locale/th/LC_MESSAGES/*
%{_datadir}/locale/nb/LC_MESSAGES/*
%{_datadir}/locale/ne/LC_MESSAGES/*
%{_datadir}/locale/sv/LC_MESSAGES/*
%{_datadir}/locale/ja/LC_MESSAGES/*
%{_datadir}/locale/mr/LC_MESSAGES/*
%{_datadir}/locale/sq/LC_MESSAGES/*
%{_datadir}/locale/sr/LC_MESSAGES/*
%{_datadir}/locale/pt/LC_MESSAGES/*
%{_datadir}/locale/ms/LC_MESSAGES/*
%{_datadir}/locale/am/LC_MESSAGES/*
%{_datadir}/locale/zh_TW/LC_MESSAGES/*
%{_datadir}/locale/ia/LC_MESSAGES/*
%{_datadir}/locale/af/LC_MESSAGES/*
%{_datadir}/locale/fi/LC_MESSAGES/*
%{_datadir}/locale/pa/LC_MESSAGES/*
%{_datadir}/locale/fr/LC_MESSAGES/*
%{_datadir}/locale/pl/LC_MESSAGES/*
%{_datadir}/locale/pt_BR/LC_MESSAGES/*
%{_datadir}/locale/eo/LC_MESSAGES/*
/usr/lib/python3/dist-packages/ubuntuuitoolkit/emulators.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/base.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/ubuntu_scenarios.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/__init__.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/fixture_setup.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/units.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_launcher.mainwindow.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_fixture_setup.LaunchFakeApplicationTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_fixture_setup.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_ubuntu_scenarios.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_touchadaptor.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_launcher.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_launcher.window.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_launcher.touch.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_touchadaptor.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/__init__.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_base.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_launcher.testcase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/test_emulators.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_textinput.textfield.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_textinput.textfield_custom.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_header.HeaderActionsOverflowTestCase.tools.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_header.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_popover.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_textinput.header.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_textinput.textarea.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_header.HeaderActionsOverflowTestCase.actions.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/__init__.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_popover.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_units.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_textinput.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/components/test_header.HeaderContentsTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.HideShowTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable_uitk10.IsFlickableTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_sections.SectionsTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable_uitk10.UnityFlickableTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.DeprecatedHeaderSectionsTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.HeaderInCustomMainViewTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listitem.ListViewWithoutLiveDraggingTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_date_picker.DatePickerBaseTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_popups.ActionSelectionPopoverTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_main_view.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_checkbox.ToggleTestCase.test_with_checkbox.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.SectionsTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_actionbar.ActionBarTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/CustomMainView.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_main_view.MainView12TestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_main_view.GoBackTestCase.back_in_header.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listitem.ListViewWithLiveDraggingTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listitems.SwipeToDeleteTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listitem.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_sections.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/MyDialog.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_toolbar.ToolbarTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_tabs.TabsTestCase.deprecated_TabBar.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable.UnityFlickableTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable_uitk10.SwipeFlickableTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_actionbar.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.ToolsTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_qquickgridview.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listitems.ExpandableTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_popups.ComposerSheetTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_dialog.DialogTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_textarea.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_qquicklistview.QQuickListViewOutOfViewTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listview.QQuickListViewTestCase.Uitk10.vertical.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_optionselector.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_tabs.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_main_view.MainView10TestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_qquicklistview.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_pull_to_refresh.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_popups.WindowTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_qquickgridview.QQuickGridViewTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listitems.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_dialog.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_toolbar.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_tabs.TabsTestCase.new_header.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.DeprecatedHeaderTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_optionselector.OptionSelectorTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable.IsFlickableTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_pull_to_refresh.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listview.QQuickListViewTestCase.Uitk13.horizontal.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/__init__.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable_uitk10.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_textarea.TextAreaTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_flickable.SwipeFlickableTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_checkbox.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_actionbar.ScrollingActionBarTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_textfield.1.3.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_textfield.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listitem.ListItemTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_listview.QQuickListViewTestCase.Uitk13.vertical.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_main_view.GoBackTestCase.back_in_toolbar.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_popups.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_header.ActionsTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_tabs.TabsTestCase.new_header.1.3.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_textfield.1.0.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_common.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_checkbox.ToggleTestCase.test_with_switch.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_optionselector.OptionSelectorCustomDelegateTestCase.qml
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/custom_proxy_objects/test_date_picker.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/gallery/test_ubuntulistview.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/gallery/test_optionselector.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/gallery/test_scrollbar.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/gallery/test_gallery.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/gallery/__init__.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/gallery/test_toggles.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/tests/gallery/test_textinput.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_optionselector.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/listitems.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_header.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_sections.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_tabbar.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_tabs.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_uclistitem.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_mainview.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_qquicklistview.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_actionbar.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_toolbar.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_checkbox.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_qquickgridview.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_common.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/pickers.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_textarea.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_ubuntulistview.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/__init__.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/popups.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_textfield.py
/usr/lib/python3/dist-packages/ubuntuuitoolkit/_custom_proxy_objects/_flickable.py
%{_bindir}/app-launch-tracepoints
%{_bindir}/profile_appstart.sh
%{_bindir}/app-launch-profiler-lttng
%{_bindir}/ubuntu-ui-toolkit-launcher
%{_bindir}/appstart_test
%{_libdir}/libUbuntuToolkit.so.5.5
%{_libdir}/libUbuntuMetrics.so.5
%{_libdir}/libUbuntuToolkit.la
%{_libdir}/libUbuntuToolkit.so.5.5.0
%{_libdir}/libUbuntuMetrics.so.5.5
%{_libdir}/libUbuntuGestures.la
%{_libdir}/libUbuntuToolkit.so.5
%{_libdir}/libUbuntuMetrics.la
%{_libdir}/libUbuntuMetrics.prl
%{_libdir}/libUbuntuGestures.so.5.5
%{_libdir}/libUbuntuGestures.prl
%{_libdir}/libUbuntuMetrics.so.5.5.0
%{_libdir}/libUbuntuGestures.so.5.5.0
%{_libdir}/libUbuntuGestures.so.5
%{_libdir}/libUbuntuToolkit.prl
%{_libdir}/ubuntu-ui-toolkit/apicheck
%{_libdir}/qt5/plugins/ubuntu/metrics/libumlttng.so
%{_libdir}/qt5/qml/Extinct/Animals/plugins.qmltypes
%{_libdir}/qt5/qml/Extinct/Animals/Andrewsarchus.qml
%{_libdir}/qt5/qml/Extinct/Animals/Paratriisodon.qml
%{_libdir}/qt5/qml/Extinct/Animals/libExtinctAnimals.so
%{_libdir}/qt5/qml/Extinct/Animals/test.qml
%{_libdir}/qt5/qml/Extinct/Animals/digger.js
%{_libdir}/qt5/qml/Extinct/Animals/Gigantophis.qml
%{_libdir}/qt5/qml/Extinct/Animals/qmldir
%{_libdir}/qt5/qml/Ubuntu/Metrics/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/Metrics/libUbuntuMetrics.so
%{_libdir}/qt5/qml/Ubuntu/Metrics/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/Components/libUbuntuComponents.so
%{_libdir}/qt5/qml/Ubuntu/Components/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/HoursModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/DialerHand.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/PickerRow.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/Dialer.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/DayModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/YearModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/DialerHandGroup.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/PickerModelBase.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/MonthModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/PickerPanel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/SecondsModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/PickerDelegate.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/Picker.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/MinutesModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.3/DatePicker.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/HoursModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/DialerHand.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/PickerRow.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/Dialer.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/DayModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/YearModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/DialerHandGroup.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/PickerModelBase.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/MonthModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/PickerPanel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/SecondsModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/PickerDelegate.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/Picker.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/MinutesModel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Pickers/1.2/DatePicker.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ScrollView.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ActionBar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/InputHandler.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PageStack.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/UbuntuListView.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Toolbar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/TextArea.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PageColumn.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/TextCursor.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/AppHeader.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/OptionSelectorDelegate.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/UbuntuColors.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PageHeadConfiguration.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/pageUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/dateUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PageHeadSections.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ActivityIndicator.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PageColumnsLayout.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/OptionSelector.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ListItemPopover.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/OrientationHelper.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Page.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ComboButton.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ProgressionSlot.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Panel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Sections.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Button.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PageHeader.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/AnimatedItem.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Switch.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ToolbarButton.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Tabs.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Scrollbar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/AdaptivePageLayout.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Slider.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ProgressBar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PageHeadState.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/TextField.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Tab.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/ToolbarItems.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/PullToRefresh.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Icon.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/UbuntuNumberAnimation.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/TabBar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/CrossFadeImage.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/TextInputPopover.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/MainView.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/Captions.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/DraggingArea.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.3/CheckBox.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.1/UbuntuColors.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.1/Haptics.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.1/Button.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.1/ProgressBar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.1/Icon.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/MainView12.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/InputHandler.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Page10.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PageStack.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/UbuntuListView.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/UbuntuListView11.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/AbstractButton.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Toolbar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/TextArea.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/CrossFadeImage10.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/TextCursor.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/AppHeader.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/OptionSelectorDelegate.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PageHeadConfiguration.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/dateUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PageHeadSections.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/ActivityIndicator.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/OptionSelector.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/OrientationHelper.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/sliderUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/ComboButton.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PageWrapper.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Header.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Panel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PageWrapperUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PageTreeNode.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/AnimatedItem.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Switch.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/ToolbarButton.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Tabs.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Scrollbar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/MainViewBase.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Slider.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PageHeadState.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/ActionList.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/TextField.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/stack.js
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Label.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Tab.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/ToolbarItems.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/PullToRefresh.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/CrossFadeImage11.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/UbuntuNumberAnimation.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Page11.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/TabBar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/TextInputPopover.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/MainView.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/Captions.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/DraggingArea.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.2/CheckBox.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/libUbuntuComponentsStyles.so
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.3/ActionBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.3/ActionItemProperties.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.3/PageHeaderStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.3/PageHeadStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.3/SectionsStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.3/ToolbarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.2/PageHeadStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.2/ComboButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Styles/1.2/PullToRefreshStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/ComposerSheet.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/ActionSelectionPopover.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/PopupBase.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/Popover.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/internalPopupUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/SheetBase.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/DefaultSheet.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/popupUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.3/Dialog.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/ComposerSheet.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/ActionSelectionPopover.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/PopupBase.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/Popover.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/internalPopupUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/SheetBase.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/DefaultSheet.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/popupUtils.js
%{_libdir}/qt5/qml/Ubuntu/Components/Popups/1.2/Dialog.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/parent_theme
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.3/SuruDarkSelected.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.3/SuruDarkNormal.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.3/TabBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.3/ButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.3/Palette.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.3/OptionSelectorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.2/TabBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.2/Palette.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.2/MainViewStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/1.2/OptionSelectorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruDark/artwork/chevron@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/1.3/PaletteValues.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/1.3/Palette.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/1.2/PaletteValues.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/1.2/Palette.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/TextFieldStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ActionBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/TextCursorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/BubbleShape.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/TextAreaStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/BottomEdgeHintStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ListItemStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ToolbarButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PageHeadButton.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/TabBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/HighlightMagnifier.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PageHeaderStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/Palette.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PageHeadStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/DialogForegroundStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/BottomEdgeStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PartialColorize.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ActivityIndicatorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ListItemOptionSelectorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/SliderStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/SheetForegroundStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PickerDelegateStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ComboButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/SectionsForPageHeadStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/DialerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ProgressionVisualStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/DatePickerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/MainViewStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/DialerHandStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ScrollbarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/OptionSelectorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/IconButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/SectionsStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/FocusShape.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PopoverForegroundStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ScrollingActionBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ToolbarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ProgressBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/PickerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/SwitchStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/CheckBoxStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/AmbianceSelected.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/OverflowPanel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/ButtonForeground.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/SectionsForPageHead.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.3/AmbianceNormal.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/TextFieldStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/TextCursorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/BubbleShape.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/TextAreaStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ListItemStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ToolbarButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/PageHeadButton.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/TabBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/HighlightMagnifier.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/Palette.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/HeadDividerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/HeaderStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/DeprecatedToolbarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/PageHeadStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/DialogForegroundStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/PartialColorize.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ActivityIndicatorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ListItemOptionSelectorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/SliderStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/SheetForegroundStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/PickerDelegateStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ComboButtonStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/DialerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ProgressionVisualStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/DatePickerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/MainViewStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/DialerHandStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ScrollbarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/OptionSelectorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/PopoverForegroundStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ProgressBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/PickerStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/SwitchStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/CheckBoxStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/OverflowPanel.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/1.2/ButtonForeground.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_shadow@20.sci
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/toolbar_dropshadow@20.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/stroke_button@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_shadow@30.sci
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_arrow@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarBottomPressed@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/background_paper@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarRightIdle@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/tick@30.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/cross@30.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/toolkit_scrollbar-stepper.svg
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarRightPressed@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/spinner@32.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/caret_noshadow@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/PageHeaderBaseDividerLight@18.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/chevron@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/progression_divider@18.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarLeftPressed@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/header_overflow_dropshadow@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarLeftIdle@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarBottomIdle@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/spinner@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/PageHeaderBaseDividerLight@18.sci
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_shadow@20.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/spinner@16.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/PageHeaderBaseDividerBottom@18.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/header_overflow_dropshadow@27.sci
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_shadow@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarTopIdle@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_arrow@30.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_shadow@30.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_shadow@8.sci
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/ScrollbarTopPressed@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/chevron_down@30.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/Ambiance/artwork/bubble_arrow@20.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/deprecated
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/TabBarStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/Palette.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/MainViewStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/OptionSelectorStyle.qml
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/parent_theme
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/artwork/tick@30.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/artwork/chevron@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/Themes/SuruGradient/artwork/chevron_down@30.png
%{_libdir}/qt5/qml/Ubuntu/Components/1.0/UbuntuColors.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.0/Button.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.0/ProgressBar.qml
%{_libdir}/qt5/qml/Ubuntu/Components/1.0/Icon.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/qmldir
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/ThinDivider.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/LabelVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/ListItemHeader.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/ImageWithFallback.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/ProgressionVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/Empty.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/SingleValue.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/IconVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/Expandable.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/ValueSelector.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/MultiValue.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/Divider.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/Standard.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/Subtitled.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/SingleControl.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/Caption.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/ItemSelector.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/Base.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.3/ExpandablesColumn.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/ThinDivider.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/LabelVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/ListItemHeader.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/ImageWithFallback.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/ProgressionVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/Empty.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/SingleValue.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/IconVisual.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/Expandable.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/ValueSelector.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/MultiValue.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/Divider.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/Standard.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/Subtitled.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/SingleControl.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/Caption.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/ItemSelector.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/Base.qml
%{_libdir}/qt5/qml/Ubuntu/Components/ListItems/1.2/ExpandablesColumn.qml
%{_libdir}/qt5/qml/Ubuntu/Components/artwork/delete@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/artwork/ListItemDivider6px@8.png
%{_libdir}/qt5/qml/Ubuntu/Components/artwork/chevron@27.png
%{_libdir}/qt5/qml/Ubuntu/Components/artwork/back@18.png
%{_libdir}/qt5/qml/Ubuntu/Components/Labs/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/Components/Labs/libUbuntuComponentsLabs.so
%{_libdir}/qt5/qml/Ubuntu/Components/Labs/qmldir
%{_libdir}/qt5/qml/Ubuntu/PerformanceMetrics/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/PerformanceMetrics/PerformanceOverlay.qml
%{_libdir}/qt5/qml/Ubuntu/PerformanceMetrics/libUbuntuPerformanceMetrics.so
%{_libdir}/qt5/qml/Ubuntu/PerformanceMetrics/BarGraph.qml
%{_libdir}/qt5/qml/Ubuntu/PerformanceMetrics/qmldir
%{_libdir}/qt5/qml/Ubuntu/Layouts/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/Layouts/libUbuntuLayouts.so
%{_libdir}/qt5/qml/Ubuntu/Layouts/qmldir
%{_libdir}/qt5/qml/Ubuntu/Test/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/Test/libUbuntuTest.so
%{_libdir}/qt5/qml/Ubuntu/Test/UbuntuTestCase.qml
%{_libdir}/qt5/qml/Ubuntu/Test/qmldir
%{_libdir}/qt5/qml/Ubuntu/Test/UbuntuTestCase13.qml
%{_libdir}/qt5/examples/unit-converter/unit-converter.pro
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/unit-converter/conversion.js
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/unit-converter/unit-converter.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/unit-converter/unit-converter.desktop
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/unit-converter/unit-converter.apparmor
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/unit-converter/manifest.json.in
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/unit-converter/unit-converter.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/locale/locale.desktop
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/locale/manifest.json.in
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/locale/locale.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/locale/locale.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/locale/locale.apparmor
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/calculator.apparmor
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/calculator.desktop
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/manifest.json.in
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/calculator.js
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/DefaultLayout.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/calculator.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/Functions.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/calculator.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/components/calculator.js
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/components/DefaultLayout.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/components/Functions.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/calculator/components/components.pro
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/jokes/jokes.desktop
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/jokes/jokes.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/jokes/manifest.json.in
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/jokes/jokes.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/jokes/jokes.apparmor
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/jokes/jokes.wav
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/customtheme/customtheme.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/customtheme/Palette.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/customtheme/manifest.json.in
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/customtheme/customtheme.desktop
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/customtheme/customtheme.apparmor
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/customtheme/parent_theme
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/customtheme/main.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/WidgetsModel.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Navigation.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/SwipeAreaPage.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Colors.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/MainPage.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/demo_image_3.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Template.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/nav_pagestack@8.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/demo_image_2.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Pickers.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/BottomEdgePage.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Sliders.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ListItemLayouts.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/TemplateRow.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ListItems.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ListItemWithLabel.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/UbuntuShape.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Animations.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/NewListItems.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ubuntu-ui-toolkit-gallery.desktop
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/TemplateSectionNavigation.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/nav_expansion@8.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Sections.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/avatar_contacts_list@8.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/manifest.json.in
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/map_icon.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ProportionalShape.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/demo_image.jpg
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Popover.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/small_avatar@8.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/images.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/call_icon@8.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ListItemsSection.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ubuntu-ui-toolkit-gallery.pro
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/AnimationCircle.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Sheet.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/TextInputs.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ActionBars.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Icons.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ubuntu-ui-toolkit-gallery.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/OptionSelectors.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/nav_tabs@8.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Toolbars.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/call@30.png
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Label.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ubuntu-ui-toolkit-gallery-click.pro
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Toggles.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Buttons.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/About.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/TemplateSection.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ubuntu-ui-toolkit-gallery.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/PageHeaders.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/CrossFadeImage.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ubuntu-ui-toolkit-gallery.apparmor
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/Dialog.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/ProgressBars.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/WebLink.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/UbuntuListViews.qml
%{_libdir}/qt5/examples/ubuntu-ui-toolkit/examples/ubuntu-ui-toolkit-gallery/TemplateFlow.qml
%{_libdir}/qt5/examples/locale/locale.pro
%{_libdir}/qt5/examples/calculator/calculator.pro
%{_libdir}/qt5/examples/calculator/components/components.pro
%{_libdir}/qt5/examples/jokes/jokes.pro
%{_libdir}/qt5/examples/customtheme/customtheme.pro
%{_libdir}/qt5/examples/ubuntu-ui-toolkit-gallery/ubuntu-ui-toolkit-gallery.pro
%{_libdir}/qt5/examples/ubuntu-ui-toolkit-gallery/po/po.pro

%files devel
%{_libdir}/libUbuntuGestures.so
%{_libdir}/libUbuntuMetrics.so
%{_libdir}/libUbuntuToolkit.so
%{_libdir}/pkgconfig/UbuntuGestures.pc
%{_libdir}/pkgconfig/UbuntuToolkit.pc
%{_libdir}/pkgconfig/UbuntuMetrics.pc
%{_includedir}/qt5/UbuntuMetrics/*
%{_includedir}/qt5/UbuntuToolkit/*
%{_includedir}/qt5/UbuntuGestures/*
%{_libdir}/qt5/mkspecs/modules/qt_lib_*.pri

