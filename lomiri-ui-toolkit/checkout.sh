#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/ubuntu-ui-toolkit.git
cd ubuntu-ui-toolkit
cat $SRC/0001-no-warns.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "no-warns"
cat $SRC/0002-dpkg.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "dpkg"
cat $SRC/0003-compability-with-5-15.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "compability-with-5-15"
