#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/libertine.git
cd libertine
cat $SRC/0001-remove-tests.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "remove-tests"
cat $SRC/0002-disable-tests.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "disable-tests"
cat $SRC/0003-disable-system-setting-to-fix-dependency-loop.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "disable-system-setting-to-fix-dependency-loop"
cat $SRC/0004-disable-paste-and-po.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "disable-paste-and-po"
