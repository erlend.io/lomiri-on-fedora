Name:       liblomiri-libertine
Version:    1.0
Release:    1%{?dist}
Summary:    runtime for running deb-packaged X11 apps on Ubuntu Personal
License:    FIXME
URL:        https://github.com/ubports/libertine
Source0:    https://github.com/ubports/libertine/archive/c2194c20a4154202ab54a838575308303b6be8e7/libertine.tar.gz
Patch0:     0001-remove-tests.patch
Patch1:     0002-disable-tests.patch
Patch2:     0003-disable-system-setting-to-fix-dependency-loop.patch
Patch3:     0004-disable-paste-and-po.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: intltool
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(python3)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
runtime for running deb-packaged X11 apps on Ubuntu Personal
Runtime library for creating and using the Ubuntu Personal sandbox for legacy
Deb-packaged X11 applicatons.  This library is used by the Libertine tools
and other software interacting with the Libertine container, such as scopes
or application launchers.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n libertine-c2194c20a4154202ab54a838575308303b6be8e7

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/liblibertine
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp liblibertine/libertine.h                                  %{buildroot}%{_includedir}/liblibertine
ln -s liblibertine.so.1 %{buildroot}%{_libdir}/liblibertine.so
ln -s liblibertine.so.1.0.0 %{buildroot}%{_libdir}/liblibertine.so.1
install -p -m 755 build/liblibertine/liblibertine.so.1.0.0   %{buildroot}%{_libdir}
cp build/libertine.pc                                        %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/liblibertine.so.1
%{_libdir}/liblibertine.so.1.0.0

%files devel
%{_includedir}/liblibertine/libertine.h
%{_libdir}/liblibertine.so
%{_libdir}/pkgconfig/libertine.pc
