Name:       lomiri-biometryd
Version:    1.0
Release:    3%{?dist}
Summary:    biometryd mediates/multiplexes to biometric devices - daemon/helper binaries
License:    FIXME
URL:        https://github.com/ubports/biometryd
Source0:    https://github.com/ubports/biometryd/archive/1e82bbef2d9b4010954ac7d0fab975d019902ec6/biometryd.tar.gz
Patch0:     0001-removing-werror-apparmor-tests.patch
Patch1:     0002-fixing-cpp-compilation-errors.patch
Patch2:     0003-correct-path-to-qmlplugindump.patch
Patch3:     0004-remove-rpath.patch
Patch4:     0005-adding-missing-header-ofstream-stdexprt.patch
Patch5:     0006-removing-werror-from-qml.patch

BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: elfutils-libelf-devel
BuildRequires: gcc-c++
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-dbus-cpp-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: redhat-lsb-core
BuildRequires: pkgconfig(dbus-cpp)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(process-cpp)
BuildRequires: pkgconfig(sqlite3)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
biometryd mediates/multiplexes to biometric devices - daemon/helper binaries
biometryd mediates and multiplexes access to biometric devices present on the system,
enabling applications and system components to leverage them for identification and verification
of users.
.
Daemon and helper binaries to be used by services.
biometryd mediates/multiplexes to biometric devices - QML bindings
biometryd mediates and multiplexes access to biometric devices present on the system,
enabling applications and system components to leverage them for identification and verification
of users.
.
This package contains the qtdeclarative bindings for biometryd.
biometryd mediates/multiplexes to biometric devices - runtime library
biometryd mediates and multiplexes access to biometric devices present on the system,
enabling applications and system components to leverage them for identification and verification
of users.
.
This package includes the biometryd runtime libraries.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n biometryd-1e82bbef2d9b4010954ac7d0fab975d019902ec6

%build
export LD_LIBRARY_PATH=%{_builddir}/biometryd-1e82bbef2d9b4010954ac7d0fab975d019902ec6/build/src/biometry/
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_includedir}/biometry/dbus
mkdir -p %{buildroot}%{_includedir}/biometry/devices/plugin
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Biometryd
mkdir -p %{buildroot}/etc/dbus-1/system.d
mkdir -p %{buildroot}/etc/init
cp data/com.ubuntu.biometryd.Service.conf                    %{buildroot}/etc/dbus-1/system.d
cp data/biometryd.conf                                       %{buildroot}/etc/init
install -p -m 755 build/src/biometry/biometryd               %{buildroot}%{_bindir}
cp include/biometry/CMakeLists.txt                           %{buildroot}%{_includedir}/biometry
cp include/biometry/application.h                            %{buildroot}%{_includedir}/biometry
cp include/biometry/dbus/service.h                           %{buildroot}%{_includedir}/biometry/dbus
cp include/biometry/device.h                                 %{buildroot}%{_includedir}/biometry
cp include/biometry/devices/fingerprint_reader.h             %{buildroot}%{_includedir}/biometry/devices
cp include/biometry/devices/plugin/interface.h               %{buildroot}%{_includedir}/biometry/devices/plugin
cp include/biometry/dictionary.h                             %{buildroot}%{_includedir}/biometry
cp include/biometry/do_not_copy_or_move.h                    %{buildroot}%{_includedir}/biometry
cp include/biometry/geometry.h                               %{buildroot}%{_includedir}/biometry
cp include/biometry/identifier.h                             %{buildroot}%{_includedir}/biometry
cp include/biometry/operation.h                              %{buildroot}%{_includedir}/biometry
cp include/biometry/optional.h                               %{buildroot}%{_includedir}/biometry
cp include/biometry/percent.h                                %{buildroot}%{_includedir}/biometry
cp include/biometry/progress.h                               %{buildroot}%{_includedir}/biometry
cp include/biometry/reason.h                                 %{buildroot}%{_includedir}/biometry
cp include/biometry/service.h                                %{buildroot}%{_includedir}/biometry
cp include/biometry/template_store.h                         %{buildroot}%{_includedir}/biometry
cp include/biometry/user.h                                   %{buildroot}%{_includedir}/biometry
cp include/biometry/variant.h                                %{buildroot}%{_includedir}/biometry
cp include/biometry/verifier.h                               %{buildroot}%{_includedir}/biometry
cp build/include/biometry/version.h                          %{buildroot}%{_includedir}/biometry
cp include/biometry/version.h.in                             %{buildroot}%{_includedir}/biometry
cp include/biometry/visibility.h                             %{buildroot}%{_includedir}/biometry
cp include/biometry/void.h                                   %{buildroot}%{_includedir}/biometry
ln -s libbiometry.so.1 %{buildroot}%{_libdir}/libbiometry.so
ln -s libbiometry.so.1.0.1 %{buildroot}%{_libdir}/libbiometry.so.1
install -p -m 755 build/src/biometry/libbiometry.so.1.0.1    %{buildroot}%{_libdir}
cp build/data/biometryd.pc                                   %{buildroot}%{_libdir}/pkgconfig
install -p -m 755 build/src/biometry/qml/Biometryd/libbiometryd-qml.so %{buildroot}%{_libdir}/qt5/qml/Biometryd
cp build/src/biometry/qml/Biometryd/plugins.qmltypes         %{buildroot}%{_libdir}/qt5/qml/Biometryd
cp src/biometry/qml/Biometryd/qmldir                         %{buildroot}%{_libdir}/qt5/qml/Biometryd

%files
/etc/dbus-1/system.d/com.ubuntu.biometryd.Service.conf
/etc/init/biometryd.conf
%{_bindir}/biometryd
%{_libdir}/libbiometry.so.1
%{_libdir}/libbiometry.so.1.0.1
%{_libdir}/qt5/qml/Biometryd/libbiometryd-qml.so
%{_libdir}/qt5/qml/Biometryd/plugins.qmltypes
%{_libdir}/qt5/qml/Biometryd/qmldir

%files devel
%{_includedir}/biometry/CMakeLists.txt
%{_includedir}/biometry/application.h
%{_includedir}/biometry/dbus/service.h
%{_includedir}/biometry/device.h
%{_includedir}/biometry/devices/fingerprint_reader.h
%{_includedir}/biometry/devices/plugin/interface.h
%{_includedir}/biometry/dictionary.h
%{_includedir}/biometry/do_not_copy_or_move.h
%{_includedir}/biometry/geometry.h
%{_includedir}/biometry/identifier.h
%{_includedir}/biometry/operation.h
%{_includedir}/biometry/optional.h
%{_includedir}/biometry/percent.h
%{_includedir}/biometry/progress.h
%{_includedir}/biometry/reason.h
%{_includedir}/biometry/service.h
%{_includedir}/biometry/template_store.h
%{_includedir}/biometry/user.h
%{_includedir}/biometry/variant.h
%{_includedir}/biometry/verifier.h
%{_includedir}/biometry/version.h
%{_includedir}/biometry/version.h.in
%{_includedir}/biometry/visibility.h
%{_includedir}/biometry/void.h
%{_libdir}/libbiometry.so
%{_libdir}/pkgconfig/biometryd.pc
