Name:       lomiri-system-compositor
Version:    0.9
Release:    2%{?dist}
Summary:    Mir System Compositor
License:    GPL-3
URL:        https://github.com/ubports/unity-system-compositor
Source0:    https://github.com/ubports/unity-system-compositor/archive/2863cdc034951d387b0a285b8762e5350c700878/unity-system-compositor.tar.gz
Patch0:     0001-removing-pil-and-rewriteing-glesv2-to-pkgconfig.patch
Patch1:     0005-add-missing-include-files.patch
Patch2:     0006-adding-missing-include-file.patch

BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: liblomiri-deviceinfo-devel
BuildRequires: mir-devel = 1.8.0
BuildRequires: python3-pillow-devel
BuildRequires: pkgconfig(glm)
BuildRequires: pkgconfig(glesv2)
BuildRequires: pkgconfig(deviceinfo)
BuildRequires: pkgconfig(gdk-pixbuf-2.0)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(wayland-egl)
BuildRequires: pkgconfig(dbus-1)

%description
Mir System Compositor
This is the system compositor using the Mir display server.  If the Unity
System Compositor can't start, LightDM will fallback to plain Xorg display
server.

%prep
%autosetup -p1 -n unity-system-compositor-2863cdc034951d387b0a285b8762e5350c700878

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DMIR_ENABLE_TESTS=OFF ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}/etc/dbus-1/system.d
mkdir -p %{buildroot}/usr/sbin
mkdir -p %{buildroot}/usr/share/apport/package-hooks
mkdir -p %{buildroot}/usr/share/dbus-1/interfaces
mkdir -p %{buildroot}/usr/share/man/man1
cp src/com.canonical.Unity.conf                              %{buildroot}/etc/dbus-1/system.d
install -p -m 755 build/bin/unity-system-compositor-spinner  %{buildroot}%{_bindir}
install -p -m 755 build/bin/unity-system-compositor          %{buildroot}/usr/sbin
install -p -m 755 debian/unity-system-compositor.sleep       %{buildroot}/usr/sbin
cp debian/source_unity-system-compositor.py                  %{buildroot}/usr/share/apport/package-hooks
cp src/com.canonical.Unity.Display.xml                       %{buildroot}/usr/share/dbus-1/interfaces
cp src/com.canonical.Unity.Input.xml                         %{buildroot}/usr/share/dbus-1/interfaces
cp src/com.canonical.Unity.PowerButton.xml                   %{buildroot}/usr/share/dbus-1/interfaces
cp src/com.canonical.Unity.UserActivity.xml                  %{buildroot}/usr/share/dbus-1/interfaces

# Not Found:/usr/share/man/man1/unity-system-compositor-spinner.1.gz
# Not Found:/usr/share/man/man1/unity-system-compositor.1.gz


%files
/etc/dbus-1/system.d/com.canonical.Unity.conf
%{_bindir}/unity-system-compositor-spinner
/usr/sbin/unity-system-compositor
/usr/sbin/unity-system-compositor.sleep
/usr/share/apport/package-hooks/source_unity-system-compositor.py
/usr/share/dbus-1/interfaces/com.canonical.Unity.Display.xml
/usr/share/dbus-1/interfaces/com.canonical.Unity.Input.xml
/usr/share/dbus-1/interfaces/com.canonical.Unity.PowerButton.xml
/usr/share/dbus-1/interfaces/com.canonical.Unity.UserActivity.xml

