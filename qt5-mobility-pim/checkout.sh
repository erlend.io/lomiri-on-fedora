#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch dev https://github.com/qt/qtpim.git
cd qtpim
cat $SRC/0001-change-version-number.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "change-version-number"
cat $SRC/0002-adding-fPic-flag.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-fPic-flag"
cat $SRC/0003-add-change-fPic-to-fPIC.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "add-change-fPic-to-fPIC"
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/qtpim-opensource-src-packaging.git
cd qtpim-opensource-src-packaging
