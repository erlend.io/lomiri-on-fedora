Name:       qt5-mobility-pim
Version:    1.0
Release:    3%{?dist}
Summary:    Qt PIM module, Contacts library
License:    FIXME
URL:        https://github.com/qt/qtpim
Source0:    https://github.com/qt/qtpim/archive/8fec622c186d254bc9750606d54c32670a9046a5/qtpim.tar.gz
Patch0:     0001-change-version-number.patch
Patch1:     0002-adding-fPic-flag.patch
Patch2:     0003-add-change-fPic-to-fPIC.patch
Patch3:     0004-Add-missing-break-in-switch.patch
Patch4:     0005-Set-PLUGIN_CLASS_NAME-in-.pro-files.patch
Patch5:     0006-workaround-for-bug-in-qt-5.15.2.patch
Patch6:     0007-workaround-for-bug-in-qt-5.15.2-part-II.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qt5-qtdeclarative-devel

%description
Qt PIM module, Contacts library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Contacts library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt PIM module, Contacts library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Contacts library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
transitional dummy package for Qt 5 Contacts QML Module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This is a transitional dummy package for qml-module-qtcontacts
which can be safely removed.
transitional dummy package for Qt 5 Contacts QML Module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This is a transitional dummy package for qml-module-qtcontacts
which can be safely removed.
Qt 5 Contacts QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the Qt Contacts QML module for Qt Declarative.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt 5 Contacts QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the Qt Contacts QML module for Qt Declarative.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt PIM module, Organizer library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Organizer library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt PIM module, Organizer library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Organizer library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
transitional dummy package for Qt 5 Organizer QML Module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This is a transitional dummy package for qml-module-qtorganizer
which can be safely removed.
transitional dummy package for Qt 5 Organizer QML Module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This is a transitional dummy package for qml-module-qtorganizer
which can be safely removed.
Qt 5 Organizer QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the Qt Organizer QML module for Qt Declarative.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt 5 Organizer QML module
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the Qt Organizer QML module for Qt Declarative.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt PIM module, Versit library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Versit library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt PIM module, Versit library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Versit library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt PIM module, Versit Organizer library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Versit Organizer library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt PIM module, Versit Organizer library
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains Qt PIM module's Versit Organizer library.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt 5 PIM documentation
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the documentation for the Qt 5 PIM
module, including Contacts, Organizer and Versit.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt 5 PIM documentation
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the documentation for the Qt 5 PIM
module, including Contacts, Organizer and Versit.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt 5 PIM HTML documentation
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the HTML documentation for the Qt 5 PIM
module, including Contacts, Organizer and Versit.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.
Qt 5 PIM HTML documentation
Qt is a cross-platform C++ application framework. Qt's primary feature
is its rich set of widgets that provide standard GUI functionality.
.
This package contains the HTML documentation for the Qt 5 PIM
module, including Contacts, Organizer and Versit.
.
WARNING: This module is not an official part of Qt 5, but instead a git
snapshot of an ongoing development. The package is very likely to
change in a binary incompatible way, and no guarantees are given.

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n qtpim-8fec622c186d254bc9750606d54c32670a9046a5

%build
mkdir .git
mkdir build
cd build
qmake-qt5 -d ..
%make_build

%install
cd build
make VERBOSE=1 INSTALL_ROOT=%{buildroot} install STRIP=/bin/true
mkdir -p %{buildroot}%{_libdir}/qt5/mkspecs/modules
cp mkspecs/modules-inst/qt_lib*.pri %{buildroot}%{_libdir}/qt5/mkspecs/modules

%files
%{_libdir}/libQt5Versit.prl
%{_libdir}/libQt5Organizer.so.5.0.0
%{_libdir}/libQt5Versit.so.5.0
%{_libdir}/libQt5Contacts.so.5
%{_libdir}/libQt5Organizer.prl
%{_libdir}/libQt5VersitOrganizer.so.5
%{_libdir}/libQt5Versit.so.5
%{_libdir}/libQt5Contacts.so.5.0
%{_libdir}/libQt5Organizer.la
%{_libdir}/libQt5Organizer.so.5.0
%{_libdir}/libQt5Contacts.la
%{_libdir}/libQt5VersitOrganizer.so.5.0.0
%{_libdir}/libQt5Versit.so.5.0.0
%{_libdir}/libQt5Organizer.so.5
%{_libdir}/libQt5VersitOrganizer.la
%{_libdir}/libQt5VersitOrganizer.prl
%{_libdir}/libQt5Contacts.prl
%{_libdir}/libQt5Versit.la
%{_libdir}/libQt5Contacts.so.5.0.0
%{_libdir}/libQt5VersitOrganizer.so.5.0
%{_libdir}/qt5/plugins/contacts/libqtcontacts_memory.so
%{_libdir}/qt5/plugins/versit/libqtversit_vcardpreserver.so
%{_libdir}/qt5/plugins/versit/libqtversit_backuphandler.so
%{_libdir}/qt5/plugins/organizer/libqtorganizer_memory.so
%{_libdir}/qt5/qml/QtContacts/plugins.qmltypes
%{_libdir}/qt5/qml/QtContacts/libdeclarative_contacts.so
%{_libdir}/qt5/qml/QtContacts/qmldir
%{_libdir}/qt5/qml/QtOrganizer/plugins.qmltypes
%{_libdir}/qt5/qml/QtOrganizer/libdeclarative_organizer.so
%{_libdir}/qt5/qml/QtOrganizer/qmldir
%{_libdir}/qt5/examples/contacts/contacts.pro
%{_libdir}/qt5/examples/organizer/organizer.pro
%{_libdir}/qt5/examples/organizer/todo/todo
%{_libdir}/qt5/examples/organizer/todo/window.cpp
%{_libdir}/qt5/examples/organizer/todo/window.h
%{_libdir}/qt5/examples/organizer/todo/todoeditor.cpp
%{_libdir}/qt5/examples/organizer/todo/main.cpp
%{_libdir}/qt5/examples/organizer/todo/todoeditor.h
%{_libdir}/qt5/examples/organizer/todo/todo.pro
%{_libdir}/qt5/examples/organizer/calendardemo/calendardemo
%{_libdir}/qt5/examples/organizer/calendardemo/calendardemo.pro
%{_libdir}/qt5/examples/organizer/calendardemo/src/todoeditpage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/todoeditpage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/monthpage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/journaleditpage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/journaleditpage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/daypage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/editcalendarspage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/eventoccurrenceeditpage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/eventeditpage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/eventoccurrenceeditpage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/daypage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/main.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/editcalendarspage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/eventeditpage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/calendardemo.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/monthpage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/addcalendarpage.h
%{_libdir}/qt5/examples/organizer/calendardemo/src/addcalendarpage.cpp
%{_libdir}/qt5/examples/organizer/calendardemo/src/calendardemo.h

%files devel
%{_libdir}/libQt5Organizer.so
%{_libdir}/libQt5Versit.so
%{_libdir}/libQt5Contacts.so
%{_libdir}/libQt5VersitOrganizer.so
%{_libdir}/cmake/Qt5Versit/Qt5Versit_QVCardPreserverVersitPlugin.cmake
%{_libdir}/cmake/Qt5Versit/Qt5VersitConfig.cmake
%{_libdir}/cmake/Qt5Versit/Qt5Versit_QBackupHandlerVersitPlugin.cmake
%{_libdir}/cmake/Qt5Versit/Qt5VersitConfigVersion.cmake
%{_libdir}/cmake/Qt5Organizer/Qt5OrganizerConfig.cmake
%{_libdir}/cmake/Qt5Organizer/Qt5OrganizerConfigVersion.cmake
%{_libdir}/cmake/Qt5Organizer/Qt5Organizer_QMemoryOrganizerPlugin.cmake
%{_libdir}/cmake/Qt5Contacts/Qt5ContactsConfigVersion.cmake
%{_libdir}/cmake/Qt5Contacts/Qt5Contacts_QMemoryContactsPlugin.cmake
%{_libdir}/cmake/Qt5Contacts/Qt5ContactsConfig.cmake
%{_libdir}/cmake/Qt5VersitOrganizer/Qt5VersitOrganizerConfigVersion.cmake
%{_libdir}/cmake/Qt5VersitOrganizer/Qt5VersitOrganizerConfig.cmake
%{_libdir}/pkgconfig/Qt5Contacts.pc
%{_libdir}/pkgconfig/Qt5Organizer.pc
%{_libdir}/pkgconfig/Qt5Versit.pc
%{_libdir}/pkgconfig/Qt5VersitOrganizer.pc
%{_includedir}/qt5/QtContacts/*
%{_includedir}/qt5/QtVersit/*
%{_includedir}/qt5/QtVersitOrganizer/*
%{_includedir}/qt5/QtOrganizer/*
%{_libdir}/qt5/mkspecs/modules/*.pri
