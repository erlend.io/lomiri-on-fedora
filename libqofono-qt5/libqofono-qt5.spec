Name:       libqofono-qt5

Summary:    A library of Qt 5 bindings for ofono
Version:    0.103
Release:    1
License:    LGPLv2
URL:        https://git.sailfishos.org/mer-core/libqofono
Source0:    https://git.sailfishos.org/mer-core/libqofono/-/archive/0.103/libqofono-0.103.tar.gz
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5Test)

%description
This package contains Qt bindings for ofono cellular service
interfaces.

%package declarative
Summary:    Declarative plugin for libqofono
Requires:   %{name} = %{version}-%{release}
Requires:   %{name} = %{version}

%description declarative
This package contains declarative plugin for libofono.

%package devel
Summary:    Development files for ofono Qt bindings
Requires:   %{name} = %{version}-%{release}
Requires:   %{name} = %{version}

%description devel
This package contains the development header files for the ofono Qt bindings.

%package tests
Summary:    qml test app for the ofono Qt bindings
Requires:   %{name} = %{version}-%{release}
Requires:   blts-tools
Requires:   phonesim
Requires:   mce-tools

%description tests
This package contains qml test for ofono Qt bindings.

%package examples
Summary:    Examples for libqofono
Requires:   %{name} = %{version}-%{release}
Requires:   %{name} = %{version}
Requires:   %{name}-declarative = %{version}

%description examples
This package contains examples for declarative plugin for libofono.

%prep
# %setup -q -n %{name}-%{version}
%autosetup -p1 -n libqofono-0.103

%build
export QT_SELECT=5
qmake-qt5 "CONFIG += debug nostrip" "VERSION=$(sed 's/+.*//' <<<"%{version}")"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
export QT_SELECT=5
make INSTALL_ROOT=%{buildroot} install STRIP=/bin/true

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/%{name}.so.*

%files declarative
%defattr(-,root,root,-)
%{_libdir}/qt5/qml/MeeGo/QOfono/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/%{name}.prl
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/qofono-qt5.pc
%{_includedir}/qofono-qt5/*.h
%{_includedir}/qofono-qt5/dbus/ofono*.xml
%{_datadir}/qt5/mkspecs/features/qofono-qt5.prf

%files tests
%defattr(-,root,root,-)
%{_libdir}/%{name}/tests/*
/opt/tests/%{name}/*

%files examples
%defattr(-,root,root,-)
/opt/examples/%{name}
