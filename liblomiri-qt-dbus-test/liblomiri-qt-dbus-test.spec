Name:       liblomiri-qt-dbus-test
Version:    1.0
Release:    1%{?dist}
Summary:    Library for testing DBus interactions using Qt
License:    FIXME
URL:        https://github.com/ubports/libqtdbustest
Source0:    https://github.com/ubports/libqtdbustest/archive/24e410ea77c9fa08894365c60bf08811a3b60bc0/libqtdbustest.tar.gz
Patch0:     0001-fixing-EnableCoverageReport-and-GMock-GTest-dependen.patch
Patch1:     0002-attemping-to-solve-rpath-problem-in-linking.patch
Patch2:     0003-chaning-back-deps.patch
Patch3:     0004-chaning-back-deps-2.patch
Patch4:     0005-returning-to-org.patch
Patch5:     0006-made-compiling-of-runner-optional.patch
Patch6:     0007-jklfjsdlk.patch
Patch7:     0008-jklfsdsfdjlk.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gcovr
BuildRequires: gnome-common
BuildRequires: lcov
BuildRequires: qt5-qtbase-devel
BuildRequires: pkgconfig(gmock)
BuildRequires: pkgconfig(gtest)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Library for testing DBus interactions using Qt
A simple library for testing Qt based DBus services and clients.
.
This package contains the shared libraries.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n libqtdbustest-24e410ea77c9fa08894365c60bf08811a3b60bc0

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/libqtdbustest-1/libqtdbustest
mkdir -p %{buildroot}%{_libdir}/libqtdbustest
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/libqtdbustest
cp src/libqtdbustest/DBusService.h                           %{buildroot}%{_includedir}/libqtdbustest-1/libqtdbustest
cp src/libqtdbustest/DBusTestRunner.h                        %{buildroot}%{_includedir}/libqtdbustest-1/libqtdbustest
cp src/libqtdbustest/QProcessDBusService.h                   %{buildroot}%{_includedir}/libqtdbustest-1/libqtdbustest
cp src/libqtdbustest/SuicidalProcess.h                       %{buildroot}%{_includedir}/libqtdbustest-1/libqtdbustest
cp build/src/libqtdbustest/config.h                          %{buildroot}%{_includedir}/libqtdbustest-1/libqtdbustest
ln -s libqtdbustest.so.1 %{buildroot}%{_libdir}/libqtdbustest.so
ln -s libqtdbustest.so.1.0.0 %{buildroot}%{_libdir}/libqtdbustest.so.1
install -p -m 755 build/src/libqtdbustest/libqtdbustest.so.1.0.0 %{buildroot}%{_libdir}
cp build/src/watchdog/watchdog                               %{buildroot}%{_libdir}/libqtdbustest
cp build/src/libqtdbustest/libqtdbustest-1.pc                %{buildroot}%{_libdir}/pkgconfig
cp data/session.conf                                         %{buildroot}/usr/share/libqtdbustest
cp data/system.conf                                          %{buildroot}/usr/share/libqtdbustest

%files
%{_libdir}/libqtdbustest.so.1
%{_libdir}/libqtdbustest.so.1.0.0
%{_libdir}/libqtdbustest/watchdog
/usr/share/libqtdbustest/session.conf
/usr/share/libqtdbustest/system.conf

%files devel
%{_includedir}/libqtdbustest-1/libqtdbustest/DBusService.h
%{_includedir}/libqtdbustest-1/libqtdbustest/DBusTestRunner.h
%{_includedir}/libqtdbustest-1/libqtdbustest/QProcessDBusService.h
%{_includedir}/libqtdbustest-1/libqtdbustest/SuicidalProcess.h
%{_includedir}/libqtdbustest-1/libqtdbustest/config.h
%{_libdir}/libqtdbustest.so
%{_libdir}/pkgconfig/libqtdbustest-1.pc
