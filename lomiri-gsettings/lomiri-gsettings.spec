Name:       lomiri-gsettings
Version:    1.0
Release:    1%{?dist}
Summary:    QML Bindings for GSettings
License:    FIXME
URL:        https://github.com/ubports/gsettings-qt
Source0:    https://github.com/ubports/gsettings-qt/archive/e12a1df31013eb940b035602951af11cee16a758/gsettings-qt.tar.gz
Patch0:     0001-dummy-patch.patch
Patch1:     0002-disabling-tests.patch
Patch2:     0003-adding-nostrip.patch

BuildRequires: gcc-c++
BuildRequires: glib2-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtdeclarative
BuildRequires: qt5-qtdeclarative-devel

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
QML Bindings for GSettings
Expose QML bindings for GSettings
Library to access GSettings from Qt
Library to access GSettings from Qt

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n gsettings-qt-e12a1df31013eb940b035602951af11cee16a758

%build
qmake-qt5 QMAKE_CXXFLAGS_RELEASE="-g3 -Og" PREFIX=/usr INSTALLS+=nostrip
make -j1 # Failing racing condition if built with -j > 1

%install
make INSTALL_ROOT="%{buildroot}" install

%files
%{_libdir}/libgsettings-qt.so.1
%{_libdir}/libgsettings-qt.so.1.0
%{_libdir}/libgsettings-qt.so.1.0.0
%{_libdir}/qt5/qml/GSettings.1.0/libGSettingsQmlPlugin.so
%{_libdir}/qt5/qml/GSettings.1.0/plugins.qmltypes
%{_libdir}/qt5/qml/GSettings.1.0/qmldir

%files devel
%{_includedir}/qt5/QGSettings/QGSettings
%{_includedir}/qt5/QGSettings/qgsettings.h
%{_libdir}/libgsettings-qt.so
%{_libdir}/pkgconfig/gsettings-qt.pc
