#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial_-_edge https://github.com/ubports/unity-api.git
cd unity-api
cat $SRC/0001-changed-SOVERSION-back-to-0.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "changed-SOVERSION-back-to-0"
