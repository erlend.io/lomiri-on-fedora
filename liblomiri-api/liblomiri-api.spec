Name:       liblomiri-api
Version:    0.1
Release:    1%{?dist}
Summary:    API for Unity shell integration
License:    LGPL-3
URL:        https://github.com/ubports/unity-api
Source0:    https://github.com/ubports/unity-api/archive/1dbfc7308dd3219bb4dbbb89f104366c7cab1a9d/unity-api.tar.gz
Patch0:     0001-changed-SOVERSION-back-to-0.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(libqtdbustest-1)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
API for Unity shell integration
Library to integrate with the Unity shell

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n unity-api-1dbfc7308dd3219bb4dbbb89f104366c7cab1a9d

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DNO_TESTS=1 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/unity/api
mkdir -p %{buildroot}%{_includedir}/unity/shell/application
mkdir -p %{buildroot}%{_includedir}/unity/shell/launcher
mkdir -p %{buildroot}%{_includedir}/unity/shell/notifications
mkdir -p %{buildroot}%{_includedir}/unity/util
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp include/unity/Exception.h                                 %{buildroot}%{_includedir}/unity
cp include/unity/SymbolExport.h                              %{buildroot}%{_includedir}/unity
cp include/unity/UnityExceptions.h                           %{buildroot}%{_includedir}/unity
cp build/include/unity/api/Version.h                         %{buildroot}%{_includedir}/unity/api
cp include/unity/shell/application/ApplicationInfoInterface.h %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/ApplicationManagerInterface.h %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/Mir.h                     %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/MirMousePointerInterface.h %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/MirPlatformCursor.h       %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/MirSurfaceInterface.h     %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/MirSurfaceItemInterface.h %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/MirSurfaceListInterface.h %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/application/SurfaceManagerInterface.h %{buildroot}%{_includedir}/unity/shell/application
cp include/unity/shell/launcher/AppDrawerModelInterface.h    %{buildroot}%{_includedir}/unity/shell/launcher
cp include/unity/shell/launcher/LauncherItemInterface.h      %{buildroot}%{_includedir}/unity/shell/launcher
cp include/unity/shell/launcher/LauncherModelInterface.h     %{buildroot}%{_includedir}/unity/shell/launcher
cp include/unity/shell/launcher/QuickListModelInterface.h    %{buildroot}%{_includedir}/unity/shell/launcher
cp include/unity/shell/notifications/Enums.h                 %{buildroot}%{_includedir}/unity/shell/notifications
cp include/unity/shell/notifications/ModelInterface.h        %{buildroot}%{_includedir}/unity/shell/notifications
cp include/unity/shell/notifications/NotificationInterface.h %{buildroot}%{_includedir}/unity/shell/notifications
cp include/unity/shell/notifications/SourceInterface.h       %{buildroot}%{_includedir}/unity/shell/notifications
cp include/unity/util/Daemon.h                               %{buildroot}%{_includedir}/unity/util
cp include/unity/util/DefinesPtrs.h                          %{buildroot}%{_includedir}/unity/util
cp include/unity/util/FileIO.h                               %{buildroot}%{_includedir}/unity/util
cp include/unity/util/GObjectMemory.h                        %{buildroot}%{_includedir}/unity/util
cp include/unity/util/GioMemory.h                            %{buildroot}%{_includedir}/unity/util
cp include/unity/util/GlibMemory.h                           %{buildroot}%{_includedir}/unity/util
cp include/unity/util/IniParser.h                            %{buildroot}%{_includedir}/unity/util
cp include/unity/util/NonCopyable.h                          %{buildroot}%{_includedir}/unity/util
cp include/unity/util/ResourcePtr.h                          %{buildroot}%{_includedir}/unity/util
cp include/unity/util/SnapPath.h                             %{buildroot}%{_includedir}/unity/util
ln -s libunity-api.so.0 %{buildroot}%{_libdir}/libunity-api.so
ln -s libunity-api.so.0.1 %{buildroot}%{_libdir}/libunity-api.so.0
install -p -m 755 build/src/libunity-api.so.0.1              %{buildroot}%{_libdir}
cp build/data/libunity-api.pc                                %{buildroot}%{_libdir}/pkgconfig
cp build/data/unity-shell-api.pc                             %{buildroot}%{_libdir}/pkgconfig
cp build/data/unity-shell-application.pc                     %{buildroot}%{_libdir}/pkgconfig
cp build/data/unity-shell-launcher.pc                        %{buildroot}%{_libdir}/pkgconfig
cp build/data/unity-shell-notifications.pc                   %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/libunity-api.so.0
%{_libdir}/libunity-api.so.0.1

%files devel
%{_includedir}/unity/Exception.h
%{_includedir}/unity/SymbolExport.h
%{_includedir}/unity/UnityExceptions.h
%{_includedir}/unity/api/Version.h
%{_includedir}/unity/shell/application/ApplicationInfoInterface.h
%{_includedir}/unity/shell/application/ApplicationManagerInterface.h
%{_includedir}/unity/shell/application/Mir.h
%{_includedir}/unity/shell/application/MirMousePointerInterface.h
%{_includedir}/unity/shell/application/MirPlatformCursor.h
%{_includedir}/unity/shell/application/MirSurfaceInterface.h
%{_includedir}/unity/shell/application/MirSurfaceItemInterface.h
%{_includedir}/unity/shell/application/MirSurfaceListInterface.h
%{_includedir}/unity/shell/application/SurfaceManagerInterface.h
%{_includedir}/unity/shell/launcher/AppDrawerModelInterface.h
%{_includedir}/unity/shell/launcher/LauncherItemInterface.h
%{_includedir}/unity/shell/launcher/LauncherModelInterface.h
%{_includedir}/unity/shell/launcher/QuickListModelInterface.h
%{_includedir}/unity/shell/notifications/Enums.h
%{_includedir}/unity/shell/notifications/ModelInterface.h
%{_includedir}/unity/shell/notifications/NotificationInterface.h
%{_includedir}/unity/shell/notifications/SourceInterface.h
%{_includedir}/unity/util/Daemon.h
%{_includedir}/unity/util/DefinesPtrs.h
%{_includedir}/unity/util/FileIO.h
%{_includedir}/unity/util/GObjectMemory.h
%{_includedir}/unity/util/GioMemory.h
%{_includedir}/unity/util/GlibMemory.h
%{_includedir}/unity/util/IniParser.h
%{_includedir}/unity/util/NonCopyable.h
%{_includedir}/unity/util/ResourcePtr.h
%{_includedir}/unity/util/SnapPath.h
%{_libdir}/libunity-api.so
%{_libdir}/pkgconfig/libunity-api.pc
%{_libdir}/pkgconfig/unity-shell-api.pc
%{_libdir}/pkgconfig/unity-shell-application.pc
%{_libdir}/pkgconfig/unity-shell-launcher.pc
%{_libdir}/pkgconfig/unity-shell-notifications.pc
