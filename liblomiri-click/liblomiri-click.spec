Name:       liblomiri-click
Version:    1.0
Release:    1%{?dist}
Summary:    run-time Click package management library
License:    FIXME
URL:        https://github.com/ubports/click
Source0:    https://github.com/ubports/click/archive/37a6bd0d93273df0b9cc81a0bfb8e5959d9eee49/click.tar.gz
Patch0:     0001-fix.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: gcc-c++
BuildRequires: intltool
BuildRequires: systemd-devel
BuildRequires: vala
BuildRequires: pkgconfig(glib)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(json-glib-1.0)
BuildRequires: pkgconfig(gee-0.8)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
run-time Click package management library
Click is a simplified packaging format that installs in a separate part of
the file system, suitable for third-party applications.
.
This package provides a shared library for managing Click packages.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n click-37a6bd0d93273df0b9cc81a0bfb8e5959d9eee49

%build
./autogen.sh
./configure --disable-packagekit --prefix=/usr
make

%install
mkdir -p %{buildroot}%{_includedir}/click-0.4
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/gir-1.0
cp lib/click/click.h                                         %{buildroot}%{_includedir}/click-0.4
ln -s libclick-0.4.so.0.4.0 %{buildroot}%{_libdir}/libclick-0.4.so
ln -s libclick-0.4.so.0.4.0 %{buildroot}%{_libdir}/libclick-0.4.so.0
install -p -m 755 lib/click/.libs/libclick-0.4.so.0.4.0      %{buildroot}%{_libdir}
cp lib/click/click-0.4.pc                                    %{buildroot}%{_libdir}/pkgconfig
cp lib/click/Click-0.4.gir                                   %{buildroot}/usr/share/gir-1.0

%files
%{_libdir}/libclick-0.4.so.0
%{_libdir}/libclick-0.4.so.0.4.0

%files devel
%{_includedir}/click-0.4/click.h
%{_libdir}/libclick-0.4.so
%{_libdir}/pkgconfig/click-0.4.pc
/usr/share/gir-1.0/Click-0.4.gir
