Name:       lomiri2
Version:    1.0
Release:    2%{?dist}
Summary:    Lomiri shell
License:    GPL-3
URL:        https://github.com/ubports/unity8
Source0:    https://github.com/ubports/unity8/archive/11c06cfd10ce4e8ce22b9945b2eafad830845d9d/unity8.tar.gz
Source1:    https://github.com/ubports/gsettings-ubuntu-touch-schemas/archive/b3bdf178e4226c91c567b84f1adf9202b2492ca2/gsettings-ubuntu-touch-schemas.tar.gz
Source2:    https://github.com/ubports/unity-notifications/archive/c6b8354b0c10bc11dc115e5cb4f552b24a074eb8/unity-notifications.tar.gz
Patch0:     0001-remove-dependency-to-system-settings.patch
Patch1:     0002-remove-dependency-to-libhybris.patch
Patch2:     0003-removing-unused-hopefully-dependency.patch
Patch3:     0004-fixed-path-to-qmlplugin-dump.patch
Patch4:     0005-added-path-to-qdbusxml2cpp.patch
Patch5:     0006-lomri2.patch

BuildRequires: autoconf
BuildRequires: automake
BuildRequires: cmake
BuildRequires: doxygen
BuildRequires: dpkg-dev
BuildRequires: gcc-c++
BuildRequires: gnome-common
BuildRequires: graphviz
BuildRequires: gsettings-qt-devel = 1.0
BuildRequires: intltool
BuildRequires: liblomiri-api2-devel
BuildRequires: liblomiri-app-launch2-devel
BuildRequires: liblomiri-connectivity-qt-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-deviceinfo-devel
BuildRequires: liblomiri-download-manager-devel
BuildRequires: liblomiri-geonames-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: liblomiri-qt-dbusmock-devel
BuildRequires: liblomiri-usermetrics-devel
BuildRequires: lightdm-qt5-devel
BuildRequires: lomiri-dbus-test-runner
BuildRequires: lomiri-qmenumodel-devel
BuildRequires: lomiri-ui-toolkit-devel
BuildRequires: pam-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: pkgconfig(deviceinfo)
BuildRequires: pkgconfig(unity-shell-application)
BuildRequires: pkgconfig(geonames)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(unity-shell-launcher)
BuildRequires: pkgconfig(qmenumodel)
BuildRequires: pkgconfig(gnome-desktop-3.0)
BuildRequires: pkgconfig(UbuntuGestures)
BuildRequires: pkgconfig(libqtdbustest-1)
BuildRequires: pkgconfig(libqtdbusmock-1)
BuildRequires: pkgconfig(libudev)
BuildRequires: pkgconfig(libevdev)
BuildRequires: pkgconfig(libusermetricsoutput-1)
BuildRequires: pkgconfig(liblightdm-qt5-3)
BuildRequires: pkgconfig(udm-common)
BuildRequires: pkgconfig(xcb)
BuildRequires: pkgconfig(connectivity-qt1)
BuildRequires: pkgconfig(unity-shell-notifications)
BuildRequires: pkgconfig(libqtdbustest-1)
Requires: lomiri-biometryd
Requires: lomiri-qtmir2
Requires: liblomiri-telefon-service
Requires: liblomiri-thumbnailer
Requires: qt5-qtwayland

%description
The Lomiri shell is the primary user interface for Lomiri.

%prep
if [ -e "unity8-11c06cfd10ce4e8ce22b9945b2eafad830845d9d" ]; then
   rm -fR unity8-11c06cfd10ce4e8ce22b9945b2eafad830845d9d
fi
tar -xvf %SOURCE0
cd unity8-11c06cfd10ce4e8ce22b9945b2eafad830845d9d
cat %PATCH0 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH1 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH2 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH3 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH4 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cat %PATCH5 | patch -p1 -s --fuzz=0 --no-backup-if-mismatch
cd ..
if [ -e "gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2" ]; then
   rm -fR gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2
fi
tar -xvf %SOURCE1
if [ -e "unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8" ]; then
   rm -fR unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8
fi
tar -xvf %SOURCE2
%debug_package

%build
cd unity8-11c06cfd10ce4e8ce22b9945b2eafad830845d9d
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DCMAKE_CXX_FLAGS="-Og -g3" -DCMAKE_SKIP_RPATH=YES -DNO_TESTS=ON ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd ..
cd gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2
./autogen.sh
make
cd ..
cd unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..
cd ..

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/unity8/qml/AccountsService
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Cursor
mkdir -p %{buildroot}%{_libdir}/unity8/qml/GlobalShortcut
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Greeter/Unity/Launcher
mkdir -p %{buildroot}%{_libdir}/unity8/qml/LightDM/FullLightDM
mkdir -p %{buildroot}%{_libdir}/unity8/qml/LightDM/IntegratedLightDM
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Lights
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Powerd
mkdir -p %{buildroot}%{_libdir}/unity8/qml/ScreenshotDirectory
mkdir -p %{buildroot}%{_libdir}/unity8/qml/SessionBroadcast
mkdir -p %{buildroot}%{_libdir}/unity8/qml/UInput
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Ubuntu/Gestures
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/ApplicationMenu
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Connectivity
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Indicators
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/InputInfo
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Launcher
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Platform
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Unity/Session
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Utils
mkdir -p %{buildroot}%{_libdir}/unity8/qml/WindowManager
mkdir -p %{buildroot}%{_libdir}/unity8/qml/Wizard
mkdir -p %{buildroot}/etc/ubuntu
mkdir -p %{buildroot}/etc/ubuntusensors
mkdir -p %{buildroot}/lib/udev/rules.d
mkdir -p %{buildroot}/usr/share/accountsservice/interfaces
mkdir -p %{buildroot}/usr/share/applications
mkdir -p %{buildroot}/usr/share/dbus-1/interfaces
mkdir -p %{buildroot}/usr/share/glib-2.0/schemas
mkdir -p %{buildroot}/usr/share/locale/aa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/am/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ar/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ast/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/az/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/be/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bg/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/br/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bs/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ca/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ca@valencia/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ce/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ckb/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/co/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/cs/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/cy/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/da/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/de/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/el/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_AU/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_GB/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_US/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/eo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/es/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/et/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/eu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ga/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gd/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/he/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hy/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ia/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/id/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/is/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/it/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ja/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/km/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/kn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ko/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ku/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ky/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/lo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/lt/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/lv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ml/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/mr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ms/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/my/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nb/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ne/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/oc/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/or/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt_BR/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ro/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ru/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sc/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/shn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/si/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sq/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sw/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ta/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/te/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/th/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/tr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ug/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/uk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ur/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/uz/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/vi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/wo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/xh/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_CN/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_HK/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_TW/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/polkit-1/actions
mkdir -p %{buildroot}/usr/share/unity8/ApplicationMenus
mkdir -p %{buildroot}/usr/share/unity8/Components/MediaServices
mkdir -p %{buildroot}/usr/share/unity8/Components/PanelState
mkdir -p %{buildroot}/usr/share/unity8/Components/SearchHistoryModel
mkdir -p %{buildroot}/usr/share/unity8/Components/graphics
mkdir -p %{buildroot}/usr/share/unity8/Greeter/graphics/session_icons
mkdir -p %{buildroot}/usr/share/unity8/Launcher/graphics
mkdir -p %{buildroot}/usr/share/unity8/Notifications
mkdir -p %{buildroot}/usr/share/unity8/Panel/Indicators/client
mkdir -p %{buildroot}/usr/share/unity8/Panel/graphics
mkdir -p %{buildroot}/usr/share/unity8/Rotation
mkdir -p %{buildroot}/usr/share/unity8/Stage/Spread
mkdir -p %{buildroot}/usr/share/unity8/Stage/graphics
mkdir -p %{buildroot}/usr/share/unity8/Tutorial/graphics
mkdir -p %{buildroot}/usr/share/unity8/Wizard/Components
mkdir -p %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
mkdir -p %{buildroot}/usr/share/unity8/graphics
mkdir -p %{buildroot}/usr/share/upstart/sessions
mkdir -p %{buildroot}/var/lib/polkit-1/localauthority/10-vendor.d
mkdir -p %{buildroot}/var/lib/unity8
cd unity8-11c06cfd10ce4e8ce22b9945b2eafad830845d9d
cp data/devices.conf                                         %{buildroot}/etc/ubuntu
cp data/test.sensors                                         %{buildroot}/etc/ubuntusensors
install -p -m 755 build/src/unity8                           %{buildroot}%{_bindir}
ln -s libunity8-private.so.0 %{buildroot}%{_libdir}/libunity8-private.so
ln -s libunity8-private.so.0.0.0 %{buildroot}%{_libdir}/libunity8-private.so.0
install -p -m 755 build/src/libunity8-private/libunity8-private.so.0.0.0 %{buildroot}%{_libdir}
install -p -m 755 build/plugins/AccountsService/libAccountsService-qml.so %{buildroot}%{_libdir}/unity8/qml/AccountsService
cp plugins/AccountsService/qmldir                            %{buildroot}%{_libdir}/unity8/qml/AccountsService
cp plugins/Cursor/Cursor.qml                                 %{buildroot}%{_libdir}/unity8/qml/Cursor
install -p -m 755 build/plugins/Cursor/libCursor-qml.so      %{buildroot}%{_libdir}/unity8/qml/Cursor
cp plugins/Cursor/qmldir                                     %{buildroot}%{_libdir}/unity8/qml/Cursor
install -p -m 755 build/plugins/GlobalShortcut/libGlobalShortcut-qml.so %{buildroot}%{_libdir}/unity8/qml/GlobalShortcut
cp plugins/GlobalShortcut/qmldir                             %{buildroot}%{_libdir}/unity8/qml/GlobalShortcut
install -p -m 755 build/plugins/Greeter/Unity/Launcher/libUnityLauncherAS-qml.so %{buildroot}%{_libdir}/unity8/qml/Greeter/Unity/Launcher
cp plugins/Greeter/Unity/Launcher/qmldir                     %{buildroot}%{_libdir}/unity8/qml/Greeter/Unity/Launcher
install -p -m 755 build/plugins/LightDM/FullLightDM/libFullLightDM-qml.so %{buildroot}%{_libdir}/unity8/qml/LightDM/FullLightDM
cp plugins/LightDM/FullLightDM/qmldir                        %{buildroot}%{_libdir}/unity8/qml/LightDM/FullLightDM
install -p -m 755 build/plugins/LightDM/IntegratedLightDM/libIntegratedLightDM-qml.so %{buildroot}%{_libdir}/unity8/qml/LightDM/IntegratedLightDM
cp plugins/LightDM/IntegratedLightDM/qmldir                  %{buildroot}%{_libdir}/unity8/qml/LightDM/IntegratedLightDM
cp plugins/Powerd/qmldir                                     %{buildroot}%{_libdir}/unity8/qml/Lights
install -p -m 755 build/plugins/Powerd/libPowerd-qml.so      %{buildroot}%{_libdir}/unity8/qml/Powerd
cp plugins/Powerd/qmldir                                     %{buildroot}%{_libdir}/unity8/qml/Powerd
cp plugins/ScreenshotDirectory/ScreenGrabber.qmltypes        %{buildroot}%{_libdir}/unity8/qml/ScreenshotDirectory
install -p -m 755 build/plugins/ScreenshotDirectory/libScreenshotDirectory-qml.so %{buildroot}%{_libdir}/unity8/qml/ScreenshotDirectory
cp plugins/ScreenshotDirectory/qmldir                        %{buildroot}%{_libdir}/unity8/qml/ScreenshotDirectory
install -p -m 755 build/plugins/SessionBroadcast/libSessionBroadcast-qml.so %{buildroot}%{_libdir}/unity8/qml/SessionBroadcast
cp plugins/SessionBroadcast/qmldir                           %{buildroot}%{_libdir}/unity8/qml/SessionBroadcast
install -p -m 755 build/plugins/UInput/libUInput-qml.so      %{buildroot}%{_libdir}/unity8/qml/UInput
cp plugins/UInput/qmldir                                     %{buildroot}%{_libdir}/unity8/qml/UInput
install -p -m 755 build/plugins/Ubuntu/DownloadDaemonListener/libDownloadDaemonListener.so %{buildroot}%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener
cp plugins/Ubuntu/DownloadDaemonListener/qmldir              %{buildroot}%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener
install -p -m 755 build/plugins/Ubuntu/Gestures/libUbuntuGesturesQml.so %{buildroot}%{_libdir}/unity8/qml/Ubuntu/Gestures
cp plugins/Ubuntu/Gestures/qmldir                            %{buildroot}%{_libdir}/unity8/qml/Ubuntu/Gestures
install -p -m 755 build/plugins/Unity/ApplicationMenu/libApplicationMenu-qml.so %{buildroot}%{_libdir}/unity8/qml/Unity/ApplicationMenu
cp plugins/Unity/ApplicationMenu/qmldir                      %{buildroot}%{_libdir}/unity8/qml/Unity/ApplicationMenu
install -p -m 755 build/plugins/Unity/Connectivity/libConnectivity.so %{buildroot}%{_libdir}/unity8/qml/Unity/Connectivity
cp plugins/Unity/Connectivity/qmldir                         %{buildroot}%{_libdir}/unity8/qml/Unity/Connectivity
install -p -m 755 build/plugins/Unity/Indicators/libIndicatorsQml.so %{buildroot}%{_libdir}/unity8/qml/Unity/Indicators
cp plugins/Unity/Indicators/qmldir                           %{buildroot}%{_libdir}/unity8/qml/Unity/Indicators
install -p -m 755 build/plugins/Unity/InputInfo/libInputInfo.so %{buildroot}%{_libdir}/unity8/qml/Unity/InputInfo
cp plugins/Unity/InputInfo/qmldir                            %{buildroot}%{_libdir}/unity8/qml/Unity/InputInfo
install -p -m 755 build/plugins/Unity/Launcher/libUnityLauncher-qml.so %{buildroot}%{_libdir}/unity8/qml/Unity/Launcher
cp plugins/Unity/Launcher/qmldir                             %{buildroot}%{_libdir}/unity8/qml/Unity/Launcher
install -p -m 755 build/plugins/Unity/Platform/libPlatform-qml.so %{buildroot}%{_libdir}/unity8/qml/Unity/Platform
cp plugins/Unity/Platform/qmldir                             %{buildroot}%{_libdir}/unity8/qml/Unity/Platform
install -p -m 755 build/plugins/Unity/Session/libUnitySession-qml.so %{buildroot}%{_libdir}/unity8/qml/Unity/Session
cp plugins/Unity/Session/qmldir                              %{buildroot}%{_libdir}/unity8/qml/Unity/Session
cp plugins/Utils/EdgeBarrierSettings.qml                     %{buildroot}%{_libdir}/unity8/qml/Utils
cp plugins/Utils/Style.js                                    %{buildroot}%{_libdir}/unity8/qml/Utils
install -p -m 755 build/plugins/Utils/libUtils-qml.so        %{buildroot}%{_libdir}/unity8/qml/Utils
cp plugins/Utils/qmldir                                      %{buildroot}%{_libdir}/unity8/qml/Utils
install -p -m 755 build/plugins/WindowManager/libwindowmanager-qml.so %{buildroot}%{_libdir}/unity8/qml/WindowManager
cp plugins/WindowManager/qmldir                              %{buildroot}%{_libdir}/unity8/qml/WindowManager
install -p -m 755 build/plugins/Wizard/libWizard-qml.so      %{buildroot}%{_libdir}/unity8/qml/Wizard
cp plugins/Wizard/qmldir                                     %{buildroot}%{_libdir}/unity8/qml/Wizard
cp plugins/AccountsService/com.canonical.unity.AccountsService.xml %{buildroot}/usr/share/accountsservice/interfaces
cp build/data/unity8.desktop                                 %{buildroot}/usr/share/applications
cp plugins/AccountsService/com.canonical.unity.AccountsService.xml %{buildroot}/usr/share/dbus-1/interfaces
cp data/com.canonical.Unity.gschema.xml                      %{buildroot}/usr/share/glib-2.0/schemas
cp data/com.canonical.Unity8.gschema.xml                     %{buildroot}/usr/share/glib-2.0/schemas
cp build/po/locale/aa/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/aa/LC_MESSAGES
cp build/po/locale/am/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/am/LC_MESSAGES
cp build/po/locale/ar/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ar/LC_MESSAGES
cp build/po/locale/ast/LC_MESSAGES/unity8.mo                 %{buildroot}/usr/share/locale/ast/LC_MESSAGES
cp build/po/locale/az/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/az/LC_MESSAGES
cp build/po/locale/be/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/be/LC_MESSAGES
cp build/po/locale/bg/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/bg/LC_MESSAGES
cp build/po/locale/bn/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/bn/LC_MESSAGES
cp build/po/locale/br/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/br/LC_MESSAGES
cp build/po/locale/bs/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/bs/LC_MESSAGES
cp build/po/locale/ca/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ca/LC_MESSAGES
cp build/po/locale/ca@valencia/LC_MESSAGES/unity8.mo         %{buildroot}/usr/share/locale/ca@valencia/LC_MESSAGES
cp build/po/locale/ce/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ce/LC_MESSAGES
cp build/po/locale/ckb/LC_MESSAGES/unity8.mo                 %{buildroot}/usr/share/locale/ckb/LC_MESSAGES
cp build/po/locale/co/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/co/LC_MESSAGES
cp build/po/locale/cs/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/cs/LC_MESSAGES
cp build/po/locale/cy/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/cy/LC_MESSAGES
cp build/po/locale/da/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/da/LC_MESSAGES
cp build/po/locale/de/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/de/LC_MESSAGES
cp build/po/locale/el/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/el/LC_MESSAGES
cp build/po/locale/en_AU/LC_MESSAGES/unity8.mo               %{buildroot}/usr/share/locale/en_AU/LC_MESSAGES
cp build/po/locale/en_GB/LC_MESSAGES/unity8.mo               %{buildroot}/usr/share/locale/en_GB/LC_MESSAGES
cp build/po/locale/en_US/LC_MESSAGES/unity8.mo               %{buildroot}/usr/share/locale/en_US/LC_MESSAGES
cp build/po/locale/eo/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/eo/LC_MESSAGES
cp build/po/locale/es/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/es/LC_MESSAGES
cp build/po/locale/et/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/et/LC_MESSAGES
cp build/po/locale/eu/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/eu/LC_MESSAGES
cp build/po/locale/fa/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/fa/LC_MESSAGES
cp build/po/locale/fi/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/fi/LC_MESSAGES
cp build/po/locale/fo/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/fo/LC_MESSAGES
cp build/po/locale/fr/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/fr/LC_MESSAGES
cp build/po/locale/ga/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ga/LC_MESSAGES
cp build/po/locale/gd/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/gd/LC_MESSAGES
cp build/po/locale/gl/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/gl/LC_MESSAGES
cp build/po/locale/gu/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/gu/LC_MESSAGES
cp build/po/locale/gv/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/gv/LC_MESSAGES
cp build/po/locale/he/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/he/LC_MESSAGES
cp build/po/locale/hi/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/hi/LC_MESSAGES
cp build/po/locale/hr/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/hr/LC_MESSAGES
cp build/po/locale/hu/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/hu/LC_MESSAGES
cp build/po/locale/hy/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/hy/LC_MESSAGES
cp build/po/locale/ia/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ia/LC_MESSAGES
cp build/po/locale/id/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/id/LC_MESSAGES
cp build/po/locale/is/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/is/LC_MESSAGES
cp build/po/locale/it/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/it/LC_MESSAGES
cp build/po/locale/ja/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ja/LC_MESSAGES
cp build/po/locale/km/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/km/LC_MESSAGES
cp build/po/locale/kn/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/kn/LC_MESSAGES
cp build/po/locale/ko/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ko/LC_MESSAGES
cp build/po/locale/ku/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ku/LC_MESSAGES
cp build/po/locale/ky/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ky/LC_MESSAGES
cp build/po/locale/lo/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/lo/LC_MESSAGES
cp build/po/locale/lt/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/lt/LC_MESSAGES
cp build/po/locale/lv/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/lv/LC_MESSAGES
cp build/po/locale/ml/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ml/LC_MESSAGES
cp build/po/locale/mr/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/mr/LC_MESSAGES
cp build/po/locale/ms/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ms/LC_MESSAGES
cp build/po/locale/my/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/my/LC_MESSAGES
cp build/po/locale/nb/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/nb/LC_MESSAGES
cp build/po/locale/ne/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ne/LC_MESSAGES
cp build/po/locale/nl/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/nl/LC_MESSAGES
cp build/po/locale/nn/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/nn/LC_MESSAGES
cp build/po/locale/oc/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/oc/LC_MESSAGES
cp build/po/locale/or/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/or/LC_MESSAGES
cp build/po/locale/pa/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/pa/LC_MESSAGES
cp build/po/locale/pl/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/pl/LC_MESSAGES
cp build/po/locale/pt/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/pt/LC_MESSAGES
cp build/po/locale/pt_BR/LC_MESSAGES/unity8.mo               %{buildroot}/usr/share/locale/pt_BR/LC_MESSAGES
cp build/po/locale/ro/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ro/LC_MESSAGES
cp build/po/locale/ru/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ru/LC_MESSAGES
cp build/po/locale/sc/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/sc/LC_MESSAGES
cp build/po/locale/shn/LC_MESSAGES/unity8.mo                 %{buildroot}/usr/share/locale/shn/LC_MESSAGES
cp build/po/locale/si/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/si/LC_MESSAGES
cp build/po/locale/sk/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/sk/LC_MESSAGES
cp build/po/locale/sl/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/sl/LC_MESSAGES
cp build/po/locale/sq/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/sq/LC_MESSAGES
cp build/po/locale/sr/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/sr/LC_MESSAGES
cp build/po/locale/sv/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/sv/LC_MESSAGES
cp build/po/locale/sw/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/sw/LC_MESSAGES
cp build/po/locale/ta/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ta/LC_MESSAGES
cp build/po/locale/te/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/te/LC_MESSAGES
cp build/po/locale/th/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/th/LC_MESSAGES
cp build/po/locale/tr/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/tr/LC_MESSAGES
cp build/po/locale/ug/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ug/LC_MESSAGES
cp build/po/locale/uk/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/uk/LC_MESSAGES
cp build/po/locale/ur/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/ur/LC_MESSAGES
cp build/po/locale/uz/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/uz/LC_MESSAGES
cp build/po/locale/vi/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/vi/LC_MESSAGES
cp build/po/locale/wo/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/wo/LC_MESSAGES
cp build/po/locale/xh/LC_MESSAGES/unity8.mo                  %{buildroot}/usr/share/locale/xh/LC_MESSAGES
cp build/po/locale/zh_CN/LC_MESSAGES/unity8.mo               %{buildroot}/usr/share/locale/zh_CN/LC_MESSAGES
cp build/po/locale/zh_HK/LC_MESSAGES/unity8.mo               %{buildroot}/usr/share/locale/zh_HK/LC_MESSAGES
cp build/po/locale/zh_TW/LC_MESSAGES/unity8.mo               %{buildroot}/usr/share/locale/zh_TW/LC_MESSAGES
cp qml/ApplicationMenus/ApplicationMenuItemFactory.qml       %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/ApplicationMenus/ApplicationMenusLimits.qml           %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/ApplicationMenus/MenuBar.qml                          %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/ApplicationMenus/MenuItem.qml                         %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/ApplicationMenus/MenuNavigator.qml                    %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/ApplicationMenus/MenuPopup.qml                        %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/ApplicationMenus/RegisteredApplicationMenuModel.qml   %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/ApplicationMenus/qmldir                               %{buildroot}/usr/share/unity8/ApplicationMenus
cp qml/Components/Background.qml                             %{buildroot}/usr/share/unity8/Components
cp qml/Components/BlurLayer.qml                              %{buildroot}/usr/share/unity8/Components
cp qml/Components/BrightnessControl.qml                      %{buildroot}/usr/share/unity8/Components
cp qml/Components/Dialogs.qml                                %{buildroot}/usr/share/unity8/Components
cp qml/Components/DragHandle.qml                             %{buildroot}/usr/share/unity8/Components
cp qml/Components/DraggingArea.qml                           %{buildroot}/usr/share/unity8/Components
cp qml/Components/EdgeBarrier.qml                            %{buildroot}/usr/share/unity8/Components
cp qml/Components/EdgeBarrierController.qml                  %{buildroot}/usr/share/unity8/Components
cp qml/Components/EdgeDragEvaluator.qml                      %{buildroot}/usr/share/unity8/Components
cp qml/Components/FadingLabel.qml                            %{buildroot}/usr/share/unity8/Components
cp qml/Components/Flickable.qml                              %{buildroot}/usr/share/unity8/Components
cp qml/Components/FloatingFlickable.qml                      %{buildroot}/usr/share/unity8/Components
cp qml/Components/GridView.qml                               %{buildroot}/usr/share/unity8/Components
cp qml/Components/Handle.qml                                 %{buildroot}/usr/share/unity8/Components
cp qml/Components/InputMethod.qml                            %{buildroot}/usr/share/unity8/Components
cp qml/Components/ItemGrabber.qml                            %{buildroot}/usr/share/unity8/Components
cp qml/Components/ItemSnapshot.qml                           %{buildroot}/usr/share/unity8/Components
cp qml/Components/KeyboardShortcutsOverlay.qml               %{buildroot}/usr/share/unity8/Components
cp qml/Components/KeymapSwitcher.qml                         %{buildroot}/usr/share/unity8/Components
cp qml/Components/LazyImage.qml                              %{buildroot}/usr/share/unity8/Components
cp qml/Components/ListView.qml                               %{buildroot}/usr/share/unity8/Components
cp qml/Components/ListViewOSKScroller.qml                    %{buildroot}/usr/share/unity8/Components
cp qml/Components/Lockscreen.qml                             %{buildroot}/usr/share/unity8/Components
cp qml/Components/MediaServices/MediaServices.qml            %{buildroot}/usr/share/unity8/Components/MediaServices
cp qml/Components/MediaServices/MediaServicesControls.qml    %{buildroot}/usr/share/unity8/Components/MediaServices
cp qml/Components/MediaServices/MediaServicesHeader.qml      %{buildroot}/usr/share/unity8/Components/MediaServices
cp qml/Components/MediaServices/VideoPlayer.qml              %{buildroot}/usr/share/unity8/Components/MediaServices
cp qml/Components/MediaServices/VideoPlayerControls.qml      %{buildroot}/usr/share/unity8/Components/MediaServices
cp qml/Components/ModeSwitchWarningDialog.qml                %{buildroot}/usr/share/unity8/Components
cp qml/Components/NotificationAudio.qml                      %{buildroot}/usr/share/unity8/Components
cp qml/Components/Orientations.qml                           %{buildroot}/usr/share/unity8/Components
cp qml/Components/PanelState/PanelState.qml                  %{buildroot}/usr/share/unity8/Components/PanelState
cp qml/Components/PanelState/qmldir                          %{buildroot}/usr/share/unity8/Components/PanelState
cp qml/Components/PassphraseLockscreen.qml                   %{buildroot}/usr/share/unity8/Components
cp qml/Components/PhysicalKeysMapper.qml                     %{buildroot}/usr/share/unity8/Components
cp qml/Components/PinLockscreen.qml                          %{buildroot}/usr/share/unity8/Components
cp qml/Components/PinPadButton.qml                           %{buildroot}/usr/share/unity8/Components
cp qml/Components/Rating.qml                                 %{buildroot}/usr/share/unity8/Components
cp qml/Components/RatingStyle.qml                            %{buildroot}/usr/share/unity8/Components
cp qml/Components/ResponsiveGridView.qml                     %{buildroot}/usr/share/unity8/Components
cp qml/Components/ScrollCalculator.qml                       %{buildroot}/usr/share/unity8/Components
cp qml/Components/SearchHistoryModel/SearchHistoryModel.qml  %{buildroot}/usr/share/unity8/Components/SearchHistoryModel
cp qml/Components/SearchHistoryModel/qmldir                  %{buildroot}/usr/share/unity8/Components/SearchHistoryModel
cp qml/Components/SharingPicker.qml                          %{buildroot}/usr/share/unity8/Components
cp qml/Components/ShellDialog.qml                            %{buildroot}/usr/share/unity8/Components
cp qml/Components/Showable.qml                               %{buildroot}/usr/share/unity8/Components
cp qml/Components/StandardAnimation.qml                      %{buildroot}/usr/share/unity8/Components
cp qml/Components/VirtualTouchPad.qml                        %{buildroot}/usr/share/unity8/Components
cp qml/Components/VolumeControl.qml                          %{buildroot}/usr/share/unity8/Components
cp qml/Components/Wallpaper.qml                              %{buildroot}/usr/share/unity8/Components
cp qml/Components/WindowControlButtons.qml                   %{buildroot}/usr/share/unity8/Components
cp qml/Components/WrongPasswordAnimation.qml                 %{buildroot}/usr/share/unity8/Components
cp qml/Components/ZoomableImage.qml                          %{buildroot}/usr/share/unity8/Components
cp qml/Components/carousel.js                                %{buildroot}/usr/share/unity8/Components
cp qml/Components/flickableUtils.js                          %{buildroot}/usr/share/unity8/Components
cp qml/Components/graphics/close@20.png                      %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/icon_star_empty@20.png            %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/icon_star_full@20.png             %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/icon_star_half@20.png             %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/icon_star_off@20.png              %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/icon_star_on@20.png               %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/non-selected@18.png               %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/non-selected@18.sci               %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/window-close.svg                  %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/window-maximize.svg               %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/window-minimize.svg               %{buildroot}/usr/share/unity8/Components/graphics
cp qml/Components/graphics/window-window.svg                 %{buildroot}/usr/share/unity8/Components/graphics
cp qml/DeviceConfiguration.qml                               %{buildroot}/usr/share/unity8
cp qml/DisabledScreenNotice.qml                              %{buildroot}/usr/share/unity8
cp qml/Greeter/Circle.qml                                    %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/Clock.qml                                     %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/CoverPage.qml                                 %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/DelayedLockscreen.qml                         %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/Dot.qml                                       %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/FullLightDMImpl.qml                           %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/Gradient.js                                   %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/Greeter.qml                                   %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/GreeterPrompt.qml                             %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/Infographics.qml                              %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/IntegratedLightDMImpl.qml                     %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/LightDMService.qml                            %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/LoginAreaContainer.qml                        %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/LoginList.qml                                 %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/NarrowView.qml                                %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/ObjectPositioner.qml                          %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/PromptList.qml                                %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/SessionIcon.qml                               %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/SessionsList.qml                              %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/ShimGreeter.qml                               %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/WideView.qml                                  %{buildroot}/usr/share/unity8/Greeter
cp qml/Greeter/graphics/dot_empty.png                        %{buildroot}/usr/share/unity8/Greeter/graphics
cp qml/Greeter/graphics/dot_filled.png                       %{buildroot}/usr/share/unity8/Greeter/graphics
cp qml/Greeter/graphics/dot_pointer.png                      %{buildroot}/usr/share/unity8/Greeter/graphics
cp qml/Greeter/graphics/icon_arrow.png                       %{buildroot}/usr/share/unity8/Greeter/graphics
cp qml/Greeter/graphics/session_icons/gnome_badge.png        %{buildroot}/usr/share/unity8/Greeter/graphics/session_icons
cp qml/Greeter/graphics/session_icons/kde_badge.png          %{buildroot}/usr/share/unity8/Greeter/graphics/session_icons
cp qml/Greeter/graphics/session_icons/recovery_console_badge.png %{buildroot}/usr/share/unity8/Greeter/graphics/session_icons
cp qml/Greeter/graphics/session_icons/ubuntu_badge.png       %{buildroot}/usr/share/unity8/Greeter/graphics/session_icons
cp qml/Greeter/graphics/session_icons/unknown_badge.png      %{buildroot}/usr/share/unity8/Greeter/graphics/session_icons
cp qml/Greeter/qmldir                                        %{buildroot}/usr/share/unity8/Greeter
cp qml/Launcher/Drawer.qml                                   %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/DrawerGridView.qml                           %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/FoldingLauncherDelegate.qml                  %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/Launcher.qml                                 %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/LauncherDelegate.qml                         %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/LauncherPanel.qml                            %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/PullToRefreshScopeStyle.qml                  %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/Tooltip.qml                                  %{buildroot}/usr/share/unity8/Launcher
cp qml/Launcher/graphics/divider-line.png                    %{buildroot}/usr/share/unity8/Launcher/graphics
cp qml/Launcher/graphics/focused_app_arrow@30.png            %{buildroot}/usr/share/unity8/Launcher/graphics
cp qml/Launcher/graphics/home.svg                            %{buildroot}/usr/share/unity8/Launcher/graphics
cp qml/Launcher/graphics/quicklist_tooltip@30.png            %{buildroot}/usr/share/unity8/Launcher/graphics
cp qml/Notifications/Notification.qml                        %{buildroot}/usr/share/unity8/Notifications
cp qml/Notifications/NotificationButton.qml                  %{buildroot}/usr/share/unity8/Notifications
cp qml/Notifications/NotificationMenuItemFactory.qml         %{buildroot}/usr/share/unity8/Notifications
cp qml/Notifications/Notifications.qml                       %{buildroot}/usr/share/unity8/Notifications
cp qml/Notifications/OptionToggle.qml                        %{buildroot}/usr/share/unity8/Notifications
cp qml/Notifications/ShapedIcon.qml                          %{buildroot}/usr/share/unity8/Notifications
cp qml/Notifications/SwipeToAct.qml                          %{buildroot}/usr/share/unity8/Notifications
cp qml/OrientedShell.qml                                     %{buildroot}/usr/share/unity8
cp qml/Panel/ActiveCallHint.qml                              %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/FakePanelMenu.qml                               %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/Indicators/IndicatorBase.qml                    %{buildroot}/usr/share/unity8/Panel/Indicators
cp qml/Panel/Indicators/IndicatorDelegate.qml                %{buildroot}/usr/share/unity8/Panel/Indicators
cp qml/Panel/Indicators/IndicatorItem.qml                    %{buildroot}/usr/share/unity8/Panel/Indicators
cp qml/Panel/Indicators/IndicatorMenuItemFactory.qml         %{buildroot}/usr/share/unity8/Panel/Indicators
cp qml/Panel/Indicators/IndicatorsLight.qml                  %{buildroot}/usr/share/unity8/Panel/Indicators
cp qml/Panel/Indicators/MessageMenuItemFactory.qml           %{buildroot}/usr/share/unity8/Panel/Indicators
cp qml/Panel/Indicators/client/IndicatorRepresentation.qml   %{buildroot}/usr/share/unity8/Panel/Indicators/client
cp qml/Panel/Indicators/client/IndicatorsClient.qml          %{buildroot}/usr/share/unity8/Panel/Indicators/client
cp qml/Panel/Indicators/client/IndicatorsList.qml            %{buildroot}/usr/share/unity8/Panel/Indicators/client
cp qml/Panel/Indicators/client/IndicatorsTree.qml            %{buildroot}/usr/share/unity8/Panel/Indicators/client
cp qml/Panel/MenuContent.qml                                 %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/Panel.qml                                       %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/PanelBar.qml                                    %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/PanelItemRow.qml                                %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/PanelMenu.qml                                   %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/PanelMenuPage.qml                               %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/PanelVelocityCalculator.qml                     %{buildroot}/usr/share/unity8/Panel
cp qml/Panel/graphics/rectangular_dropshadow@30.png          %{buildroot}/usr/share/unity8/Panel/graphics
cp qml/Panel/graphics/rectangular_dropshadow@30.sci          %{buildroot}/usr/share/unity8/Panel/graphics
cp qml/Rotation/HalfLoopRotationAnimation.qml                %{buildroot}/usr/share/unity8/Rotation
cp qml/Rotation/ImmediateRotationAction.qml                  %{buildroot}/usr/share/unity8/Rotation
cp qml/Rotation/NinetyRotationAnimation.qml                  %{buildroot}/usr/share/unity8/Rotation
cp qml/Rotation/RotationStates.qml                           %{buildroot}/usr/share/unity8/Rotation
cp qml/Rotation/UpdateShellTransformations.qml               %{buildroot}/usr/share/unity8/Rotation
cp qml/Shell.qml                                             %{buildroot}/usr/share/unity8
cp qml/Stage/ApplicationWindow.qml                           %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/ChildWindow.qml                                 %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/ChildWindowRepeater.qml                         %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/ChildWindowTree.qml                             %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/DecoratedWindow.qml                             %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/FakeMaximizeDelegate.qml                        %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/MainViewStyle.qml                               %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/MoveHandler.qml                                 %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/OrientationChangeAnimation.qml                  %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/PromptSurfaceAnimations.qml                     %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/ResizeGrip.qml                                  %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/SideStage.qml                                   %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/Splash.qml                                      %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/Spread/BezierCurve.qml                          %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/KeySpline.js                             %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/MathUtils.js                             %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/OpacityMask.qml                          %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/Spread.qml                               %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/SpreadDelegateInputArea.qml              %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/SpreadMaths.qml                          %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/StagedRightEdgeMaths.qml                 %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/WindowedRightEdgeMaths.qml               %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Spread/cubic-bezier.js                          %{buildroot}/usr/share/unity8/Stage/Spread
cp qml/Stage/Stage.qml                                       %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/StageMaths.qml                                  %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/StagedFullscreenPolicy.qml                      %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/SurfaceContainer.qml                            %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/TabletSideStageTouchGesture.qml                 %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/WindowControlsOverlay.qml                       %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/WindowDecoration.qml                            %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/WindowInfoItem.qml                              %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/WindowResizeArea.qml                            %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/WindowStateSaver.qml                            %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/WindowedFullscreenPolicy.qml                    %{buildroot}/usr/share/unity8/Stage
cp qml/Stage/graphics/PageHeaderBaseDividerBottom@18.png     %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/PageHeaderBaseDividerLight@18.png      %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/PageHeaderBaseDividerLight@18.sci      %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/arrows-centre.png                      %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/arrows.png                             %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/sidestage_drag.svg                     %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/sidestage_handle@20.png                %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/sidestage_handle@20.sci                %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/sidestage_open.svg                     %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Stage/graphics/window-close.svg                       %{buildroot}/usr/share/unity8/Stage/graphics
cp qml/Tutorial/InactivityTimer.qml                          %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/Tutorial.qml                                 %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/TutorialContent.qml                          %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/TutorialLeft.qml                             %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/TutorialLeftLong.qml                         %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/TutorialPage.qml                             %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/TutorialRight.qml                            %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/TutorialTop.qml                              %{buildroot}/usr/share/unity8/Tutorial
cp qml/Tutorial/graphics/arrow.svg                           %{buildroot}/usr/share/unity8/Tutorial/graphics
cp qml/Tutorial/graphics/background1.png                     %{buildroot}/usr/share/unity8/Tutorial/graphics
cp qml/Tutorial/graphics/background2.png                     %{buildroot}/usr/share/unity8/Tutorial/graphics
cp qml/Wizard/CheckableSetting.qml                           %{buildroot}/usr/share/unity8/Wizard
cp qml/Wizard/Components/InputMethod.qml                     %{buildroot}/usr/share/unity8/Wizard/Components
cp qml/Wizard/Page.qml                                       %{buildroot}/usr/share/unity8/Wizard
cp qml/Wizard/Pages.qml                                      %{buildroot}/usr/share/unity8/Wizard
cp qml/Wizard/Pages/10-welcome-update.qml                    %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/10-welcome.qml                           %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/11-changelog.qml                         %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/20-keyboard.qml                          %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/30-wifi.qml                              %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/50-timezone.qml                          %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/60-account.qml                           %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/70-passwd-type.qml                       %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/78-firmware-update.qml.disabled          %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/79-system-update.qml                     %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/80-finished.qml                          %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/data/Desktop_header_bkg.png              %{buildroot}/usr/share/unity8/Wizard/Pages/data
cp qml/Wizard/Pages/data/Desktop_splash_screen_bkg.png       %{buildroot}/usr/share/unity8/Wizard/Pages/data
cp qml/Wizard/Pages/data/Phone_header_bkg.png                %{buildroot}/usr/share/unity8/Wizard/Pages/data
cp qml/Wizard/Pages/data/Phone_splash_screen_bkg.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data
cp qml/Wizard/Pages/data/Tick@30.png                         %{buildroot}/usr/share/unity8/Wizard/Pages/data
cp qml/Wizard/Pages/data/timezonemap/map.png                 %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/pin.png                 %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-1.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-10.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-11.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-2.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-3.5.png       %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-3.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-4.5.png       %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-4.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-5.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-6.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-7.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-8.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-9.5.png       %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_-9.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_0.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_1.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_10.5.png       %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_10.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_11.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_12.75.png      %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_12.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_13.png         %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_2.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_3.5.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_3.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_4.5.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_4.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_5.5.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_5.75.png       %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_5.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_6.5.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_6.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_7.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_8.5.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_8.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_9.5.png        %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/data/timezonemap/timezone_9.png          %{buildroot}/usr/share/unity8/Wizard/Pages/data/timezonemap
cp qml/Wizard/Pages/passcode-confirm.qml                     %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/passcode-desktop.qml                     %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/passcode-set.qml                         %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/password-set.qml                         %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/Pages/sim.qml                                  %{buildroot}/usr/share/unity8/Wizard/Pages
cp qml/Wizard/PasswordMeter.qml                              %{buildroot}/usr/share/unity8/Wizard
cp qml/Wizard/StackButton.qml                                %{buildroot}/usr/share/unity8/Wizard
cp qml/Wizard/Wizard.qml                                     %{buildroot}/usr/share/unity8/Wizard
cp qml/Wizard/WizardItemSelector.qml                         %{buildroot}/usr/share/unity8/Wizard
cp qml/Wizard/WizardTextField.qml                            %{buildroot}/usr/share/unity8/Wizard
cp qml/graphics/dropshadow2gu@30.png                         %{buildroot}/usr/share/unity8/graphics
cp qml/graphics/dropshadow2gu@30.sci                         %{buildroot}/usr/share/unity8/graphics
cp qml/graphics/dropshadow_left@20.png                       %{buildroot}/usr/share/unity8/graphics
cp qml/graphics/dropshadow_right@20.png                      %{buildroot}/usr/share/unity8/graphics
cp data/unity8-filewatcher.conf                              %{buildroot}/usr/share/upstart/sessions
cp data/unity8.conf                                          %{buildroot}/usr/share/upstart/sessions
cp plugins/Wizard/50-com.canonical.unity.wizard.pkla         %{buildroot}/var/lib/polkit-1/localauthority/10-vendor.d
cp build/data/version                                        %{buildroot}/var/lib/unity8
cd ..
cd gsettings-ubuntu-touch-schemas-b3bdf178e4226c91c567b84f1adf9202b2492ca2
cp accountsservice/com.ubuntu.AccountsService.Input.xml      %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.AccountsService.Sound.xml      %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Phone.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Sound.xml %{buildroot}/usr/share/accountsservice/interfaces
cp accountsservice/com.ubuntu.AccountsService.Input.xml      %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.AccountsService.Sound.xml      %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Phone.xml %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml %{buildroot}/usr/share/dbus-1/interfaces
cp accountsservice/com.ubuntu.touch.AccountsService.Sound.xml %{buildroot}/usr/share/dbus-1/interfaces
cp schemas/com.ubuntu.notifications.hub.gschema.xml          %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.notifications.settings.gschema.xml     %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.phone.gschema.xml                      %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.sound.gschema.xml                      %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.touch.network.gschema.xml              %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.touch.sound.gschema.xml                %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.touch.system.gschema.xml               %{buildroot}/usr/share/glib-2.0/schemas
cp schemas/com.ubuntu.user-interface.gschema.xml             %{buildroot}/usr/share/glib-2.0/schemas
cp accountsservice/com.ubuntu.AccountsService.policy         %{buildroot}/usr/share/polkit-1/actions
cp accountsservice/50-com.ubuntu.AccountsService.pkla        %{buildroot}/var/lib/polkit-1/localauthority/10-vendor.d
cd ..
cd unity-notifications-c6b8354b0c10bc11dc115e5cb4f552b24a074eb8
install -p -m 755 build/src/libnotifyclientplugin.so         %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
install -p -m 755 build/src/libnotifyplugin.so               %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
cp src/qmldir                                                %{buildroot}%{_libdir}/unity8/qml/Unity/Notifications
cd ..
cd unity8-11c06cfd10ce4e8ce22b9945b2eafad830845d9d
cp qml/Components/ImageResolver.qml  %{buildroot}/usr/share/unity8/Components

# Not Found:/lib/udev/rules.d/60-unity8-common.rules
# Not Found:/usr/lib/aarch64-linux-gnu/unity8/qml/Lights/libLights-qml.so
# Not Found:/usr/share/unity8/ApplicationMenus/ApplicationMenuItemFactory.qmlc
# Not Found:/usr/share/unity8/ApplicationMenus/ApplicationMenusLimits.qmlc
# Not Found:/usr/share/unity8/ApplicationMenus/MenuBar.qmlc
# Not Found:/usr/share/unity8/ApplicationMenus/MenuItem.qmlc
# Not Found:/usr/share/unity8/ApplicationMenus/MenuNavigator.qmlc
# Not Found:/usr/share/unity8/ApplicationMenus/MenuPopup.qmlc
# Not Found:/usr/share/unity8/ApplicationMenus/RegisteredApplicationMenuModel.qmlc
# Not Found:/usr/share/unity8/Components/Background.qmlc
# Not Found:/usr/share/unity8/Components/BlurLayer.qmlc
# Not Found:/usr/share/unity8/Components/BrightnessControl.qmlc
# Not Found:/usr/share/unity8/Components/Dialogs.qmlc
# Not Found:/usr/share/unity8/Components/DragHandle.qmlc
# Not Found:/usr/share/unity8/Components/DraggingArea.qmlc
# Not Found:/usr/share/unity8/Components/EdgeBarrier.qmlc
# Not Found:/usr/share/unity8/Components/EdgeBarrierController.qmlc
# Not Found:/usr/share/unity8/Components/EdgeDragEvaluator.qmlc
# Not Found:/usr/share/unity8/Components/FadingLabel.qmlc
# Not Found:/usr/share/unity8/Components/Flickable.qmlc
# Not Found:/usr/share/unity8/Components/FloatingFlickable.qmlc
# Not Found:/usr/share/unity8/Components/GridView.qmlc
# Not Found:/usr/share/unity8/Components/Handle.qmlc
# Not Found:/usr/share/unity8/Components/InputMethod.qmlc
# Not Found:/usr/share/unity8/Components/ItemGrabber.qmlc
# Not Found:/usr/share/unity8/Components/ItemSnapshot.qmlc
# Not Found:/usr/share/unity8/Components/KeyboardShortcutsOverlay.qmlc
# Not Found:/usr/share/unity8/Components/KeymapSwitcher.qmlc
# Not Found:/usr/share/unity8/Components/LazyImage.qmlc
# Not Found:/usr/share/unity8/Components/ListView.qmlc
# Not Found:/usr/share/unity8/Components/Lockscreen.qmlc
# Not Found:/usr/share/unity8/Components/MediaServices/MediaServices.qmlc
# Not Found:/usr/share/unity8/Components/MediaServices/MediaServicesControls.qmlc
# Not Found:/usr/share/unity8/Components/MediaServices/MediaServicesHeader.qmlc
# Not Found:/usr/share/unity8/Components/MediaServices/VideoPlayer.qmlc
# Not Found:/usr/share/unity8/Components/MediaServices/VideoPlayerControls.qmlc
# Not Found:/usr/share/unity8/Components/ModeSwitchWarningDialog.qmlc
# Not Found:/usr/share/unity8/Components/NotificationAudio.qmlc
# Not Found:/usr/share/unity8/Components/Orientations.qmlc
# Not Found:/usr/share/unity8/Components/PanelState/PanelState.qmlc
# Not Found:/usr/share/unity8/Components/PassphraseLockscreen.qmlc
# Not Found:/usr/share/unity8/Components/PhysicalKeysMapper.qmlc
# Not Found:/usr/share/unity8/Components/PinLockscreen.qmlc
# Not Found:/usr/share/unity8/Components/PinPadButton.qmlc
# Not Found:/usr/share/unity8/Components/Rating.qmlc
# Not Found:/usr/share/unity8/Components/RatingStyle.qmlc
# Not Found:/usr/share/unity8/Components/ResponsiveGridView.qmlc
# Not Found:/usr/share/unity8/Components/ScrollCalculator.qmlc
# Not Found:/usr/share/unity8/Components/SharingPicker.qmlc
# Not Found:/usr/share/unity8/Components/ShellDialog.qmlc
# Not Found:/usr/share/unity8/Components/Showable.qmlc
# Not Found:/usr/share/unity8/Components/StandardAnimation.qmlc
# Not Found:/usr/share/unity8/Components/VirtualTouchPad.qmlc
# Not Found:/usr/share/unity8/Components/VolumeControl.qmlc
# Not Found:/usr/share/unity8/Components/Wallpaper.qmlc
# Not Found:/usr/share/unity8/Components/WallpaperResolver.qml
# Not Found:/usr/share/unity8/Components/WallpaperResolver.qmlc
# Not Found:/usr/share/unity8/Components/WindowControlButtons.qmlc
# Not Found:/usr/share/unity8/Components/WrongPasswordAnimation.qmlc
# Not Found:/usr/share/unity8/Components/ZoomableImage.qmlc
# Not Found:/usr/share/unity8/Components/flickableUtils.jsc
# Not Found:/usr/share/unity8/Greeter/Circle.qmlc
# Not Found:/usr/share/unity8/Greeter/Clock.qmlc
# Not Found:/usr/share/unity8/Greeter/CoverPage.qmlc
# Not Found:/usr/share/unity8/Greeter/DelayedLockscreen.qmlc
# Not Found:/usr/share/unity8/Greeter/Dot.qmlc
# Not Found:/usr/share/unity8/Greeter/FullLightDMImpl.qmlc
# Not Found:/usr/share/unity8/Greeter/Gradient.jsc
# Not Found:/usr/share/unity8/Greeter/Greeter.qmlc
# Not Found:/usr/share/unity8/Greeter/GreeterPrompt.qmlc
# Not Found:/usr/share/unity8/Greeter/Infographics.qmlc
# Not Found:/usr/share/unity8/Greeter/LightDMService.qmlc
# Not Found:/usr/share/unity8/Greeter/LoginAreaContainer.qmlc
# Not Found:/usr/share/unity8/Greeter/LoginList.qmlc
# Not Found:/usr/share/unity8/Greeter/NarrowView.qmlc
# Not Found:/usr/share/unity8/Greeter/ObjectPositioner.qmlc
# Not Found:/usr/share/unity8/Greeter/PromptList.qmlc
# Not Found:/usr/share/unity8/Greeter/SessionIcon.qmlc
# Not Found:/usr/share/unity8/Greeter/SessionsList.qmlc
# Not Found:/usr/share/unity8/Greeter/WideView.qmlc
# Not Found:/usr/share/unity8/Launcher/Drawer.qmlc
# Not Found:/usr/share/unity8/Launcher/DrawerGridView.qmlc
# Not Found:/usr/share/unity8/Launcher/FoldingLauncherDelegate.qmlc
# Not Found:/usr/share/unity8/Launcher/Launcher.qmlc
# Not Found:/usr/share/unity8/Launcher/LauncherDelegate.qmlc
# Not Found:/usr/share/unity8/Launcher/LauncherPanel.qmlc
# Not Found:/usr/share/unity8/Launcher/PullToRefreshScopeStyle.qmlc
# Not Found:/usr/share/unity8/Launcher/Tooltip.qmlc
# Not Found:/usr/share/unity8/Notifications/Notification.qmlc
# Not Found:/usr/share/unity8/Notifications/NotificationButton.qmlc
# Not Found:/usr/share/unity8/Notifications/NotificationMenuItemFactory.qmlc
# Not Found:/usr/share/unity8/Notifications/Notifications.qmlc
# Not Found:/usr/share/unity8/Notifications/OptionToggle.qmlc
# Not Found:/usr/share/unity8/Notifications/ShapedIcon.qmlc
# Not Found:/usr/share/unity8/Notifications/SwipeToAct.qmlc
# Not Found:/usr/share/unity8/Panel/ActiveCallHint.qmlc
# Not Found:/usr/share/unity8/Panel/Indicators/IndicatorBase.qmlc
# Not Found:/usr/share/unity8/Panel/Indicators/IndicatorDelegate.qmlc
# Not Found:/usr/share/unity8/Panel/Indicators/IndicatorItem.qmlc
# Not Found:/usr/share/unity8/Panel/Indicators/IndicatorMenuItemFactory.qmlc
# Not Found:/usr/share/unity8/Panel/Indicators/IndicatorsLight.qmlc
# Not Found:/usr/share/unity8/Panel/Indicators/MessageMenuItemFactory.qmlc
# Not Found:/usr/share/unity8/Panel/MenuContent.qmlc
# Not Found:/usr/share/unity8/Panel/Panel.qmlc
# Not Found:/usr/share/unity8/Panel/PanelBar.qmlc
# Not Found:/usr/share/unity8/Panel/PanelItemRow.qmlc
# Not Found:/usr/share/unity8/Panel/PanelMenu.qmlc
# Not Found:/usr/share/unity8/Panel/PanelMenuPage.qmlc
# Not Found:/usr/share/unity8/Panel/PanelVelocityCalculator.qmlc
# Not Found:/usr/share/unity8/Rotation/HalfLoopRotationAnimation.qmlc
# Not Found:/usr/share/unity8/Rotation/ImmediateRotationAction.qmlc
# Not Found:/usr/share/unity8/Rotation/NinetyRotationAnimation.qmlc
# Not Found:/usr/share/unity8/Rotation/RotationStates.qmlc
# Not Found:/usr/share/unity8/Rotation/UpdateShellTransformations.qmlc
# Not Found:/usr/share/unity8/Stage/ApplicationWindow.qmlc
# Not Found:/usr/share/unity8/Stage/ChildWindow.qmlc
# Not Found:/usr/share/unity8/Stage/ChildWindowTree.qmlc
# Not Found:/usr/share/unity8/Stage/DecoratedWindow.qmlc
# Not Found:/usr/share/unity8/Stage/FakeMaximizeDelegate.qmlc
# Not Found:/usr/share/unity8/Stage/MainViewStyle.qmlc
# Not Found:/usr/share/unity8/Stage/MoveHandler.qmlc
# Not Found:/usr/share/unity8/Stage/PromptSurfaceAnimations.qmlc
# Not Found:/usr/share/unity8/Stage/ResizeGrip.qmlc
# Not Found:/usr/share/unity8/Stage/SideStage.qmlc
# Not Found:/usr/share/unity8/Stage/Splash.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/BezierCurve.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/KeySpline.jsc
# Not Found:/usr/share/unity8/Stage/Spread/MathUtils.jsc
# Not Found:/usr/share/unity8/Stage/Spread/OpacityMask.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/Spread.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/SpreadDelegateInputArea.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/SpreadMaths.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/StagedRightEdgeMaths.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/WindowedRightEdgeMaths.qmlc
# Not Found:/usr/share/unity8/Stage/Spread/cubic-bezier.jsc
# Not Found:/usr/share/unity8/Stage/Stage.qmlc
# Not Found:/usr/share/unity8/Stage/StageMaths.qmlc
# Not Found:/usr/share/unity8/Stage/StagedFullscreenPolicy.qmlc
# Not Found:/usr/share/unity8/Stage/SurfaceContainer.qmlc
# Not Found:/usr/share/unity8/Stage/TabletSideStageTouchGesture.qmlc
# Not Found:/usr/share/unity8/Stage/WindowControlsOverlay.qmlc
# Not Found:/usr/share/unity8/Stage/WindowDecoration.qmlc
# Not Found:/usr/share/unity8/Stage/WindowInfoItem.qmlc
# Not Found:/usr/share/unity8/Stage/WindowResizeArea.qmlc
# Not Found:/usr/share/unity8/Stage/WindowStateSaver.qmlc
# Not Found:/usr/share/unity8/Stage/WindowedFullscreenPolicy.qmlc
# Not Found:/usr/share/unity8/Tutorial/InactivityTimer.qmlc
# Not Found:/usr/share/unity8/Tutorial/Tutorial.qmlc
# Not Found:/usr/share/unity8/Tutorial/TutorialContent.qmlc
# Not Found:/usr/share/unity8/Tutorial/TutorialLeft.qmlc
# Not Found:/usr/share/unity8/Tutorial/TutorialLeftLong.qmlc
# Not Found:/usr/share/unity8/Tutorial/TutorialPage.qmlc
# Not Found:/usr/share/unity8/Tutorial/TutorialRight.qmlc
# Not Found:/usr/share/unity8/Tutorial/TutorialTop.qmlc
# Not Found:/usr/share/unity8/Wizard/Page.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/10-welcome-update.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/10-welcome.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/11-changelog.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/20-keyboard.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/30-wifi.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/50-timezone.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/60-account.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/70-passwd-type.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/79-system-update.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/80-finished.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/passcode-confirm.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/passcode-set.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/password-set.qmlc
# Not Found:/usr/share/unity8/Wizard/Pages/sim.qmlc
# Not Found:/usr/share/unity8/Wizard/PasswordMeter.qmlc
# Not Found:/usr/share/unity8/Wizard/StackButton.qmlc
# Not Found:/usr/share/unity8/Wizard/Wizard.qmlc
# Not Found:/usr/share/unity8/Wizard/WizardItemSelector.qmlc
# Not Found:/usr/share/unity8/Wizard/WizardTextField.qmlc




%files
/etc/ubuntu/devices.conf
/etc/ubuntusensors/test.sensors
%{_bindir}/unity8
%{_libdir}/libunity8-private.so
%{_libdir}/libunity8-private.so.0
%{_libdir}/libunity8-private.so.0.0.0
%{_libdir}/unity8/qml/AccountsService/libAccountsService-qml.so
%{_libdir}/unity8/qml/AccountsService/qmldir
%{_libdir}/unity8/qml/Cursor/Cursor.qml
%{_libdir}/unity8/qml/Cursor/libCursor-qml.so
%{_libdir}/unity8/qml/Cursor/qmldir
%{_libdir}/unity8/qml/GlobalShortcut/libGlobalShortcut-qml.so
%{_libdir}/unity8/qml/GlobalShortcut/qmldir
%{_libdir}/unity8/qml/Greeter/Unity/Launcher/libUnityLauncherAS-qml.so
%{_libdir}/unity8/qml/Greeter/Unity/Launcher/qmldir
%{_libdir}/unity8/qml/LightDM/FullLightDM/libFullLightDM-qml.so
%{_libdir}/unity8/qml/LightDM/FullLightDM/qmldir
%{_libdir}/unity8/qml/LightDM/IntegratedLightDM/libIntegratedLightDM-qml.so
%{_libdir}/unity8/qml/LightDM/IntegratedLightDM/qmldir
%{_libdir}/unity8/qml/Lights/qmldir
%{_libdir}/unity8/qml/Powerd/libPowerd-qml.so
%{_libdir}/unity8/qml/Powerd/qmldir
%{_libdir}/unity8/qml/ScreenshotDirectory/ScreenGrabber.qmltypes
%{_libdir}/unity8/qml/ScreenshotDirectory/libScreenshotDirectory-qml.so
%{_libdir}/unity8/qml/ScreenshotDirectory/qmldir
%{_libdir}/unity8/qml/SessionBroadcast/libSessionBroadcast-qml.so
%{_libdir}/unity8/qml/SessionBroadcast/qmldir
%{_libdir}/unity8/qml/UInput/libUInput-qml.so
%{_libdir}/unity8/qml/UInput/qmldir
%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener/libDownloadDaemonListener.so
%{_libdir}/unity8/qml/Ubuntu/DownloadDaemonListener/qmldir
%{_libdir}/unity8/qml/Ubuntu/Gestures/libUbuntuGesturesQml.so
%{_libdir}/unity8/qml/Ubuntu/Gestures/qmldir
%{_libdir}/unity8/qml/Unity/ApplicationMenu/libApplicationMenu-qml.so
%{_libdir}/unity8/qml/Unity/ApplicationMenu/qmldir
%{_libdir}/unity8/qml/Unity/Connectivity/libConnectivity.so
%{_libdir}/unity8/qml/Unity/Connectivity/qmldir
%{_libdir}/unity8/qml/Unity/Indicators/libIndicatorsQml.so
%{_libdir}/unity8/qml/Unity/Indicators/qmldir
%{_libdir}/unity8/qml/Unity/InputInfo/libInputInfo.so
%{_libdir}/unity8/qml/Unity/InputInfo/qmldir
%{_libdir}/unity8/qml/Unity/Launcher/libUnityLauncher-qml.so
%{_libdir}/unity8/qml/Unity/Launcher/qmldir
%{_libdir}/unity8/qml/Unity/Notifications/libnotifyclientplugin.so
%{_libdir}/unity8/qml/Unity/Notifications/libnotifyplugin.so
%{_libdir}/unity8/qml/Unity/Notifications/qmldir
%{_libdir}/unity8/qml/Unity/Platform/libPlatform-qml.so
%{_libdir}/unity8/qml/Unity/Platform/qmldir
%{_libdir}/unity8/qml/Unity/Session/libUnitySession-qml.so
%{_libdir}/unity8/qml/Unity/Session/qmldir
%{_libdir}/unity8/qml/Utils/EdgeBarrierSettings.qml
%{_libdir}/unity8/qml/Utils/Style.js
%{_libdir}/unity8/qml/Utils/libUtils-qml.so
%{_libdir}/unity8/qml/Utils/qmldir
%{_libdir}/unity8/qml/WindowManager/libwindowmanager-qml.so
%{_libdir}/unity8/qml/WindowManager/qmldir
%{_libdir}/unity8/qml/Wizard/libWizard-qml.so
%{_libdir}/unity8/qml/Wizard/qmldir
/usr/share/accountsservice/interfaces/com.canonical.unity.AccountsService.xml
/usr/share/accountsservice/interfaces/com.ubuntu.AccountsService.Input.xml
/usr/share/accountsservice/interfaces/com.ubuntu.AccountsService.SecurityPrivacy.xml
/usr/share/accountsservice/interfaces/com.ubuntu.AccountsService.Sound.xml
/usr/share/accountsservice/interfaces/com.ubuntu.touch.AccountsService.Phone.xml
/usr/share/accountsservice/interfaces/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml
/usr/share/accountsservice/interfaces/com.ubuntu.touch.AccountsService.Sound.xml
/usr/share/applications/unity8.desktop
/usr/share/dbus-1/interfaces/com.canonical.unity.AccountsService.xml
/usr/share/dbus-1/interfaces/com.ubuntu.AccountsService.Input.xml
/usr/share/dbus-1/interfaces/com.ubuntu.AccountsService.SecurityPrivacy.xml
/usr/share/dbus-1/interfaces/com.ubuntu.AccountsService.Sound.xml
/usr/share/dbus-1/interfaces/com.ubuntu.touch.AccountsService.Phone.xml
/usr/share/dbus-1/interfaces/com.ubuntu.touch.AccountsService.SecurityPrivacy.xml
/usr/share/dbus-1/interfaces/com.ubuntu.touch.AccountsService.Sound.xml
/usr/share/glib-2.0/schemas/com.canonical.Unity.gschema.xml
/usr/share/glib-2.0/schemas/com.canonical.Unity8.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.notifications.hub.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.notifications.settings.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.phone.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.sound.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.touch.network.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.touch.sound.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.touch.system.gschema.xml
/usr/share/glib-2.0/schemas/com.ubuntu.user-interface.gschema.xml
/usr/share/locale/aa/LC_MESSAGES/unity8.mo
/usr/share/locale/am/LC_MESSAGES/unity8.mo
/usr/share/locale/ar/LC_MESSAGES/unity8.mo
/usr/share/locale/ast/LC_MESSAGES/unity8.mo
/usr/share/locale/az/LC_MESSAGES/unity8.mo
/usr/share/locale/be/LC_MESSAGES/unity8.mo
/usr/share/locale/bg/LC_MESSAGES/unity8.mo
/usr/share/locale/bn/LC_MESSAGES/unity8.mo
/usr/share/locale/br/LC_MESSAGES/unity8.mo
/usr/share/locale/bs/LC_MESSAGES/unity8.mo
/usr/share/locale/ca/LC_MESSAGES/unity8.mo
/usr/share/locale/ca@valencia/LC_MESSAGES/unity8.mo
/usr/share/locale/ce/LC_MESSAGES/unity8.mo
/usr/share/locale/ckb/LC_MESSAGES/unity8.mo
/usr/share/locale/co/LC_MESSAGES/unity8.mo
/usr/share/locale/cs/LC_MESSAGES/unity8.mo
/usr/share/locale/cy/LC_MESSAGES/unity8.mo
/usr/share/locale/da/LC_MESSAGES/unity8.mo
/usr/share/locale/de/LC_MESSAGES/unity8.mo
/usr/share/locale/el/LC_MESSAGES/unity8.mo
/usr/share/locale/en_AU/LC_MESSAGES/unity8.mo
/usr/share/locale/en_GB/LC_MESSAGES/unity8.mo
/usr/share/locale/en_US/LC_MESSAGES/unity8.mo
/usr/share/locale/eo/LC_MESSAGES/unity8.mo
/usr/share/locale/es/LC_MESSAGES/unity8.mo
/usr/share/locale/et/LC_MESSAGES/unity8.mo
/usr/share/locale/eu/LC_MESSAGES/unity8.mo
/usr/share/locale/fa/LC_MESSAGES/unity8.mo
/usr/share/locale/fi/LC_MESSAGES/unity8.mo
/usr/share/locale/fo/LC_MESSAGES/unity8.mo
/usr/share/locale/fr/LC_MESSAGES/unity8.mo
/usr/share/locale/ga/LC_MESSAGES/unity8.mo
/usr/share/locale/gd/LC_MESSAGES/unity8.mo
/usr/share/locale/gl/LC_MESSAGES/unity8.mo
/usr/share/locale/gu/LC_MESSAGES/unity8.mo
/usr/share/locale/gv/LC_MESSAGES/unity8.mo
/usr/share/locale/he/LC_MESSAGES/unity8.mo
/usr/share/locale/hi/LC_MESSAGES/unity8.mo
/usr/share/locale/hr/LC_MESSAGES/unity8.mo
/usr/share/locale/hu/LC_MESSAGES/unity8.mo
/usr/share/locale/hy/LC_MESSAGES/unity8.mo
/usr/share/locale/ia/LC_MESSAGES/unity8.mo
/usr/share/locale/id/LC_MESSAGES/unity8.mo
/usr/share/locale/is/LC_MESSAGES/unity8.mo
/usr/share/locale/it/LC_MESSAGES/unity8.mo
/usr/share/locale/ja/LC_MESSAGES/unity8.mo
/usr/share/locale/km/LC_MESSAGES/unity8.mo
/usr/share/locale/kn/LC_MESSAGES/unity8.mo
/usr/share/locale/ko/LC_MESSAGES/unity8.mo
/usr/share/locale/ku/LC_MESSAGES/unity8.mo
/usr/share/locale/ky/LC_MESSAGES/unity8.mo
/usr/share/locale/lo/LC_MESSAGES/unity8.mo
/usr/share/locale/lt/LC_MESSAGES/unity8.mo
/usr/share/locale/lv/LC_MESSAGES/unity8.mo
/usr/share/locale/ml/LC_MESSAGES/unity8.mo
/usr/share/locale/mr/LC_MESSAGES/unity8.mo
/usr/share/locale/ms/LC_MESSAGES/unity8.mo
/usr/share/locale/my/LC_MESSAGES/unity8.mo
/usr/share/locale/nb/LC_MESSAGES/unity8.mo
/usr/share/locale/ne/LC_MESSAGES/unity8.mo
/usr/share/locale/nl/LC_MESSAGES/unity8.mo
/usr/share/locale/nn/LC_MESSAGES/unity8.mo
/usr/share/locale/oc/LC_MESSAGES/unity8.mo
/usr/share/locale/or/LC_MESSAGES/unity8.mo
/usr/share/locale/pa/LC_MESSAGES/unity8.mo
/usr/share/locale/pl/LC_MESSAGES/unity8.mo
/usr/share/locale/pt/LC_MESSAGES/unity8.mo
/usr/share/locale/pt_BR/LC_MESSAGES/unity8.mo
/usr/share/locale/ro/LC_MESSAGES/unity8.mo
/usr/share/locale/ru/LC_MESSAGES/unity8.mo
/usr/share/locale/sc/LC_MESSAGES/unity8.mo
/usr/share/locale/shn/LC_MESSAGES/unity8.mo
/usr/share/locale/si/LC_MESSAGES/unity8.mo
/usr/share/locale/sk/LC_MESSAGES/unity8.mo
/usr/share/locale/sl/LC_MESSAGES/unity8.mo
/usr/share/locale/sq/LC_MESSAGES/unity8.mo
/usr/share/locale/sr/LC_MESSAGES/unity8.mo
/usr/share/locale/sv/LC_MESSAGES/unity8.mo
/usr/share/locale/sw/LC_MESSAGES/unity8.mo
/usr/share/locale/ta/LC_MESSAGES/unity8.mo
/usr/share/locale/te/LC_MESSAGES/unity8.mo
/usr/share/locale/th/LC_MESSAGES/unity8.mo
/usr/share/locale/tr/LC_MESSAGES/unity8.mo
/usr/share/locale/ug/LC_MESSAGES/unity8.mo
/usr/share/locale/uk/LC_MESSAGES/unity8.mo
/usr/share/locale/ur/LC_MESSAGES/unity8.mo
/usr/share/locale/uz/LC_MESSAGES/unity8.mo
/usr/share/locale/vi/LC_MESSAGES/unity8.mo
/usr/share/locale/wo/LC_MESSAGES/unity8.mo
/usr/share/locale/xh/LC_MESSAGES/unity8.mo
/usr/share/locale/zh_CN/LC_MESSAGES/unity8.mo
/usr/share/locale/zh_HK/LC_MESSAGES/unity8.mo
/usr/share/locale/zh_TW/LC_MESSAGES/unity8.mo
/usr/share/polkit-1/actions/com.ubuntu.AccountsService.policy
/usr/share/unity8/ApplicationMenus/ApplicationMenuItemFactory.qml
/usr/share/unity8/ApplicationMenus/ApplicationMenusLimits.qml
/usr/share/unity8/ApplicationMenus/MenuBar.qml
/usr/share/unity8/ApplicationMenus/MenuItem.qml
/usr/share/unity8/ApplicationMenus/MenuNavigator.qml
/usr/share/unity8/ApplicationMenus/MenuPopup.qml
/usr/share/unity8/ApplicationMenus/RegisteredApplicationMenuModel.qml
/usr/share/unity8/ApplicationMenus/qmldir
/usr/share/unity8/Components/Background.qml
/usr/share/unity8/Components/BlurLayer.qml
/usr/share/unity8/Components/BrightnessControl.qml
/usr/share/unity8/Components/Dialogs.qml
/usr/share/unity8/Components/DragHandle.qml
/usr/share/unity8/Components/DraggingArea.qml
/usr/share/unity8/Components/EdgeBarrier.qml
/usr/share/unity8/Components/EdgeBarrierController.qml
/usr/share/unity8/Components/EdgeDragEvaluator.qml
/usr/share/unity8/Components/FadingLabel.qml
/usr/share/unity8/Components/Flickable.qml
/usr/share/unity8/Components/FloatingFlickable.qml
/usr/share/unity8/Components/GridView.qml
/usr/share/unity8/Components/Handle.qml
/usr/share/unity8/Components/InputMethod.qml
/usr/share/unity8/Components/ItemGrabber.qml
/usr/share/unity8/Components/ItemSnapshot.qml
/usr/share/unity8/Components/KeyboardShortcutsOverlay.qml
/usr/share/unity8/Components/KeymapSwitcher.qml
/usr/share/unity8/Components/LazyImage.qml
/usr/share/unity8/Components/ListView.qml
/usr/share/unity8/Components/ListViewOSKScroller.qml
/usr/share/unity8/Components/Lockscreen.qml
/usr/share/unity8/Components/MediaServices/MediaServices.qml
/usr/share/unity8/Components/MediaServices/MediaServicesControls.qml
/usr/share/unity8/Components/MediaServices/MediaServicesHeader.qml
/usr/share/unity8/Components/MediaServices/VideoPlayer.qml
/usr/share/unity8/Components/MediaServices/VideoPlayerControls.qml
/usr/share/unity8/Components/ModeSwitchWarningDialog.qml
/usr/share/unity8/Components/NotificationAudio.qml
/usr/share/unity8/Components/Orientations.qml
/usr/share/unity8/Components/PanelState/PanelState.qml
/usr/share/unity8/Components/PanelState/qmldir
/usr/share/unity8/Components/PassphraseLockscreen.qml
/usr/share/unity8/Components/PhysicalKeysMapper.qml
/usr/share/unity8/Components/PinLockscreen.qml
/usr/share/unity8/Components/PinPadButton.qml
/usr/share/unity8/Components/Rating.qml
/usr/share/unity8/Components/RatingStyle.qml
/usr/share/unity8/Components/ResponsiveGridView.qml
/usr/share/unity8/Components/ScrollCalculator.qml
/usr/share/unity8/Components/SearchHistoryModel/SearchHistoryModel.qml
/usr/share/unity8/Components/SearchHistoryModel/qmldir
/usr/share/unity8/Components/SharingPicker.qml
/usr/share/unity8/Components/ShellDialog.qml
/usr/share/unity8/Components/Showable.qml
/usr/share/unity8/Components/StandardAnimation.qml
/usr/share/unity8/Components/VirtualTouchPad.qml
/usr/share/unity8/Components/VolumeControl.qml
/usr/share/unity8/Components/Wallpaper.qml
/usr/share/unity8/Components/WindowControlButtons.qml
/usr/share/unity8/Components/WrongPasswordAnimation.qml
/usr/share/unity8/Components/ZoomableImage.qml
/usr/share/unity8/Components/carousel.js
/usr/share/unity8/Components/flickableUtils.js
/usr/share/unity8/Components/graphics/close@20.png
/usr/share/unity8/Components/graphics/icon_star_empty@20.png
/usr/share/unity8/Components/graphics/icon_star_full@20.png
/usr/share/unity8/Components/graphics/icon_star_half@20.png
/usr/share/unity8/Components/graphics/icon_star_off@20.png
/usr/share/unity8/Components/graphics/icon_star_on@20.png
/usr/share/unity8/Components/graphics/non-selected@18.png
/usr/share/unity8/Components/graphics/non-selected@18.sci
/usr/share/unity8/Components/graphics/window-close.svg
/usr/share/unity8/Components/graphics/window-maximize.svg
/usr/share/unity8/Components/graphics/window-minimize.svg
/usr/share/unity8/Components/graphics/window-window.svg
/usr/share/unity8/DeviceConfiguration.qml
/usr/share/unity8/DisabledScreenNotice.qml
/usr/share/unity8/Greeter/Circle.qml
/usr/share/unity8/Greeter/Clock.qml
/usr/share/unity8/Greeter/CoverPage.qml
/usr/share/unity8/Greeter/DelayedLockscreen.qml
/usr/share/unity8/Greeter/Dot.qml
/usr/share/unity8/Greeter/FullLightDMImpl.qml
/usr/share/unity8/Greeter/Gradient.js
/usr/share/unity8/Greeter/Greeter.qml
/usr/share/unity8/Greeter/GreeterPrompt.qml
/usr/share/unity8/Greeter/Infographics.qml
/usr/share/unity8/Greeter/IntegratedLightDMImpl.qml
/usr/share/unity8/Greeter/LightDMService.qml
/usr/share/unity8/Greeter/LoginAreaContainer.qml
/usr/share/unity8/Greeter/LoginList.qml
/usr/share/unity8/Greeter/NarrowView.qml
/usr/share/unity8/Greeter/ObjectPositioner.qml
/usr/share/unity8/Greeter/PromptList.qml
/usr/share/unity8/Greeter/SessionIcon.qml
/usr/share/unity8/Greeter/SessionsList.qml
/usr/share/unity8/Greeter/ShimGreeter.qml
/usr/share/unity8/Greeter/WideView.qml
/usr/share/unity8/Greeter/graphics/dot_empty.png
/usr/share/unity8/Greeter/graphics/dot_filled.png
/usr/share/unity8/Greeter/graphics/dot_pointer.png
/usr/share/unity8/Greeter/graphics/icon_arrow.png
/usr/share/unity8/Greeter/graphics/session_icons/gnome_badge.png
/usr/share/unity8/Greeter/graphics/session_icons/kde_badge.png
/usr/share/unity8/Greeter/graphics/session_icons/recovery_console_badge.png
/usr/share/unity8/Greeter/graphics/session_icons/ubuntu_badge.png
/usr/share/unity8/Greeter/graphics/session_icons/unknown_badge.png
/usr/share/unity8/Greeter/qmldir
/usr/share/unity8/Launcher/Drawer.qml
/usr/share/unity8/Launcher/DrawerGridView.qml
/usr/share/unity8/Launcher/FoldingLauncherDelegate.qml
/usr/share/unity8/Launcher/Launcher.qml
/usr/share/unity8/Launcher/LauncherDelegate.qml
/usr/share/unity8/Launcher/LauncherPanel.qml
/usr/share/unity8/Launcher/PullToRefreshScopeStyle.qml
/usr/share/unity8/Launcher/Tooltip.qml
/usr/share/unity8/Launcher/graphics/divider-line.png
/usr/share/unity8/Launcher/graphics/focused_app_arrow@30.png
/usr/share/unity8/Launcher/graphics/home.svg
/usr/share/unity8/Launcher/graphics/quicklist_tooltip@30.png
/usr/share/unity8/Notifications/Notification.qml
/usr/share/unity8/Notifications/NotificationButton.qml
/usr/share/unity8/Notifications/NotificationMenuItemFactory.qml
/usr/share/unity8/Notifications/Notifications.qml
/usr/share/unity8/Notifications/OptionToggle.qml
/usr/share/unity8/Notifications/ShapedIcon.qml
/usr/share/unity8/Notifications/SwipeToAct.qml
/usr/share/unity8/OrientedShell.qml
/usr/share/unity8/Panel/ActiveCallHint.qml
/usr/share/unity8/Panel/FakePanelMenu.qml
/usr/share/unity8/Panel/Indicators/IndicatorBase.qml
/usr/share/unity8/Panel/Indicators/IndicatorDelegate.qml
/usr/share/unity8/Panel/Indicators/IndicatorItem.qml
/usr/share/unity8/Panel/Indicators/IndicatorMenuItemFactory.qml
/usr/share/unity8/Panel/Indicators/IndicatorsLight.qml
/usr/share/unity8/Panel/Indicators/MessageMenuItemFactory.qml
/usr/share/unity8/Panel/Indicators/client/IndicatorRepresentation.qml
/usr/share/unity8/Panel/Indicators/client/IndicatorsClient.qml
/usr/share/unity8/Panel/Indicators/client/IndicatorsList.qml
/usr/share/unity8/Panel/Indicators/client/IndicatorsTree.qml
/usr/share/unity8/Panel/MenuContent.qml
/usr/share/unity8/Panel/Panel.qml
/usr/share/unity8/Panel/PanelBar.qml
/usr/share/unity8/Panel/PanelItemRow.qml
/usr/share/unity8/Panel/PanelMenu.qml
/usr/share/unity8/Panel/PanelMenuPage.qml
/usr/share/unity8/Panel/PanelVelocityCalculator.qml
/usr/share/unity8/Panel/graphics/rectangular_dropshadow@30.png
/usr/share/unity8/Panel/graphics/rectangular_dropshadow@30.sci
/usr/share/unity8/Rotation/HalfLoopRotationAnimation.qml
/usr/share/unity8/Rotation/ImmediateRotationAction.qml
/usr/share/unity8/Rotation/NinetyRotationAnimation.qml
/usr/share/unity8/Rotation/RotationStates.qml
/usr/share/unity8/Rotation/UpdateShellTransformations.qml
/usr/share/unity8/Shell.qml
/usr/share/unity8/Stage/ApplicationWindow.qml
/usr/share/unity8/Stage/ChildWindow.qml
/usr/share/unity8/Stage/ChildWindowRepeater.qml
/usr/share/unity8/Stage/ChildWindowTree.qml
/usr/share/unity8/Stage/DecoratedWindow.qml
/usr/share/unity8/Stage/FakeMaximizeDelegate.qml
/usr/share/unity8/Stage/MainViewStyle.qml
/usr/share/unity8/Stage/MoveHandler.qml
/usr/share/unity8/Stage/OrientationChangeAnimation.qml
/usr/share/unity8/Stage/PromptSurfaceAnimations.qml
/usr/share/unity8/Stage/ResizeGrip.qml
/usr/share/unity8/Stage/SideStage.qml
/usr/share/unity8/Stage/Splash.qml
/usr/share/unity8/Stage/Spread/BezierCurve.qml
/usr/share/unity8/Stage/Spread/KeySpline.js
/usr/share/unity8/Stage/Spread/MathUtils.js
/usr/share/unity8/Stage/Spread/OpacityMask.qml
/usr/share/unity8/Stage/Spread/Spread.qml
/usr/share/unity8/Stage/Spread/SpreadDelegateInputArea.qml
/usr/share/unity8/Stage/Spread/SpreadMaths.qml
/usr/share/unity8/Stage/Spread/StagedRightEdgeMaths.qml
/usr/share/unity8/Stage/Spread/WindowedRightEdgeMaths.qml
/usr/share/unity8/Stage/Spread/cubic-bezier.js
/usr/share/unity8/Stage/Stage.qml
/usr/share/unity8/Stage/StageMaths.qml
/usr/share/unity8/Stage/StagedFullscreenPolicy.qml
/usr/share/unity8/Stage/SurfaceContainer.qml
/usr/share/unity8/Stage/TabletSideStageTouchGesture.qml
/usr/share/unity8/Stage/WindowControlsOverlay.qml
/usr/share/unity8/Stage/WindowDecoration.qml
/usr/share/unity8/Stage/WindowInfoItem.qml
/usr/share/unity8/Stage/WindowResizeArea.qml
/usr/share/unity8/Stage/WindowStateSaver.qml
/usr/share/unity8/Stage/WindowedFullscreenPolicy.qml
/usr/share/unity8/Stage/graphics/PageHeaderBaseDividerBottom@18.png
/usr/share/unity8/Stage/graphics/PageHeaderBaseDividerLight@18.png
/usr/share/unity8/Stage/graphics/PageHeaderBaseDividerLight@18.sci
/usr/share/unity8/Stage/graphics/arrows-centre.png
/usr/share/unity8/Stage/graphics/arrows.png
/usr/share/unity8/Stage/graphics/sidestage_drag.svg
/usr/share/unity8/Stage/graphics/sidestage_handle@20.png
/usr/share/unity8/Stage/graphics/sidestage_handle@20.sci
/usr/share/unity8/Stage/graphics/sidestage_open.svg
/usr/share/unity8/Stage/graphics/window-close.svg
/usr/share/unity8/Tutorial/InactivityTimer.qml
/usr/share/unity8/Tutorial/Tutorial.qml
/usr/share/unity8/Tutorial/TutorialContent.qml
/usr/share/unity8/Tutorial/TutorialLeft.qml
/usr/share/unity8/Tutorial/TutorialLeftLong.qml
/usr/share/unity8/Tutorial/TutorialPage.qml
/usr/share/unity8/Tutorial/TutorialRight.qml
/usr/share/unity8/Tutorial/TutorialTop.qml
/usr/share/unity8/Tutorial/graphics/arrow.svg
/usr/share/unity8/Tutorial/graphics/background1.png
/usr/share/unity8/Tutorial/graphics/background2.png
/usr/share/unity8/Wizard/CheckableSetting.qml
/usr/share/unity8/Wizard/Components/InputMethod.qml
/usr/share/unity8/Wizard/Page.qml
/usr/share/unity8/Wizard/Pages.qml
/usr/share/unity8/Wizard/Pages/10-welcome-update.qml
/usr/share/unity8/Wizard/Pages/10-welcome.qml
/usr/share/unity8/Wizard/Pages/11-changelog.qml
/usr/share/unity8/Wizard/Pages/20-keyboard.qml
/usr/share/unity8/Wizard/Pages/30-wifi.qml
/usr/share/unity8/Wizard/Pages/50-timezone.qml
/usr/share/unity8/Wizard/Pages/60-account.qml
/usr/share/unity8/Wizard/Pages/70-passwd-type.qml
/usr/share/unity8/Wizard/Pages/78-firmware-update.qml.disabled
/usr/share/unity8/Wizard/Pages/79-system-update.qml
/usr/share/unity8/Wizard/Pages/80-finished.qml
/usr/share/unity8/Wizard/Pages/data/Desktop_header_bkg.png
/usr/share/unity8/Wizard/Pages/data/Desktop_splash_screen_bkg.png
/usr/share/unity8/Wizard/Pages/data/Phone_header_bkg.png
/usr/share/unity8/Wizard/Pages/data/Phone_splash_screen_bkg.png
/usr/share/unity8/Wizard/Pages/data/Tick@30.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/map.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/pin.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-1.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-10.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-11.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-2.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-3.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-3.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-4.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-4.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-6.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-7.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-8.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-9.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_-9.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_0.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_1.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_10.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_10.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_11.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_12.75.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_12.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_13.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_2.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_3.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_3.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_4.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_4.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_5.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_5.75.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_6.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_6.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_7.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_8.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_8.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_9.5.png
/usr/share/unity8/Wizard/Pages/data/timezonemap/timezone_9.png
/usr/share/unity8/Wizard/Pages/passcode-confirm.qml
/usr/share/unity8/Wizard/Pages/passcode-desktop.qml
/usr/share/unity8/Wizard/Pages/passcode-set.qml
/usr/share/unity8/Wizard/Pages/password-set.qml
/usr/share/unity8/Wizard/Pages/sim.qml
/usr/share/unity8/Wizard/PasswordMeter.qml
/usr/share/unity8/Wizard/StackButton.qml
/usr/share/unity8/Wizard/Wizard.qml
/usr/share/unity8/Wizard/WizardItemSelector.qml
/usr/share/unity8/Wizard/WizardTextField.qml
/usr/share/unity8/graphics/dropshadow2gu@30.png
/usr/share/unity8/graphics/dropshadow2gu@30.sci
/usr/share/unity8/graphics/dropshadow_left@20.png
/usr/share/unity8/graphics/dropshadow_right@20.png
/usr/share/upstart/sessions/unity8-filewatcher.conf
/usr/share/upstart/sessions/unity8.conf
/var/lib/polkit-1/localauthority/10-vendor.d/50-com.canonical.unity.wizard.pkla
/var/lib/polkit-1/localauthority/10-vendor.d/50-com.ubuntu.AccountsService.pkla
/var/lib/unity8/version
/usr/share/unity8/Components/ImageResolver.qml
