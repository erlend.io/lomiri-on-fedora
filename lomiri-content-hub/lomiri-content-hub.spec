Name:       lomiri-content-hub
Version:    1.0
Release:    1%{?dist}
Summary:    content sharing/picking service
License:    FIXME
URL:        https://github.com/ubports/content-hub
Source0:    https://github.com/ubports/content-hub/archive/642f45302a0ff153b07ce5d3e5b46db2a01a6c6c/content-hub.tar.gz
Patch0:     0001-removing-apparmor.patch

BuildRequires: cmake
BuildRequires: doxygen
BuildRequires: gcc-c++
BuildRequires: gettext-devel
BuildRequires: gsettings-qt-devel = 1.0
BuildRequires: liblomiri-app-launch-devel
BuildRequires: liblomiri-cpp-devel
BuildRequires: liblomiri-download-manager-devel
BuildRequires: liblomiri-libertine-devel
BuildRequires: lomiri-ui-toolkit-devel
BuildRequires: qt5-mobility-feedback-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: qt5-qtgraphicaleffects
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: pkgconfig(ubuntu-app-launch-2)
BuildRequires: pkgconfig(libertine)
BuildRequires: pkgconfig(libnih)
BuildRequires: pkgconfig(libnih-dbus)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(ubuntu-download-manager-client)
BuildRequires: pkgconfig(libnotify)

%description
content sharing/picking service
Content sharing/picking infrastructure and service, designed to allow apps to
securely and efficiently exchange content.
This package includes the content sharing service.
content sharing/picking library
Content sharing/picking infrastructure and service, designed to allow apps to
securely and efficiently exchange content.
This package includes the content sharing libraries.
content sharing/picking library - GLib bindings
Content sharing/picking infrastructure and service, designed to allow apps to
securely and efficiently exchange content.
.
This package includes GLib bindings of the content sharing libraries.
QML binding for libcontent-hub
QML bindings for libcontent-hub
Documentation files for libcontent-hub-dev
Documentation files for the libcontent-hub development
content sharing testability
Files and utilities needed for automated testing of content-hub

%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
mkdir -p /tmp/runtime-builduser
chmod 700 /tmp/runtime-builduser
export XDG_RUNTIME_DIR=/tmp/runtime-builduser
%autosetup -p1 -n content-hub-642f45302a0ff153b07ce5d3e5b46db2a01a6c6c

%build
mkdir build
mkdir -p build/import/Ubuntu/Content/
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr \
    -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
    -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
    -DENABLE_TESTS=OFF -DENABLE_DOC=OFF ..
XDG_RUNTIME_DIR=/tmp/runtime-builduser make -j1 V=1 VERBOSE=1
cd ..

%install
mkdir -p /tmp/runtime-builduser
chmod 700 /tmp/runtime-builduser
export XDG_RUNTIME_DIR=/tmp/runtime-builduser
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_includedir}/com/ubuntu/content/glib
mkdir -p %{buildroot}%{_libdir}/content-hub
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
mkdir -p %{buildroot}/etc/apparmor.d
mkdir -p %{buildroot}/usr/share/applications
mkdir -p %{buildroot}/usr/share/click/hooks
mkdir -p %{buildroot}/usr/share/content-hub/icons
mkdir -p %{buildroot}/usr/share/content-hub/peers
mkdir -p %{buildroot}/usr/share/content-hub/testability/data
mkdir -p %{buildroot}/usr/share/dbus-1/services
mkdir -p %{buildroot}/usr/share/doc/content-hub/cpp/html/search
mkdir -p %{buildroot}/usr/share/doc/content-hub/qml/html/style
mkdir -p %{buildroot}/usr/share/glib-2.0/schemas
mkdir -p %{buildroot}/usr/share/icons/hicolor/512x512/apps
mkdir -p %{buildroot}/usr/share/lintian/overrides
mkdir -p %{buildroot}/usr/share/locale/aa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/am/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ar/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ast/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/az/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/be/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bg/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/br/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/bs/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ca/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ca@valencia/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/cs/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/cy/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/da/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/de/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/el/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_AU/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/en_GB/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/eo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/es/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/et/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/eu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fo/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/fr_CA/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ga/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gd/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/gl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/he/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/hu/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ia/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/id/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/is/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/it/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ja/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/kk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/km/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/kn/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ko/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ln/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/lv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ms/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/my/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nb/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ne/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/nl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/oc/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/om/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pa/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/pt_BR/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ro/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ru/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/si/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sl/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sq/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/sv/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ta/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/th/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/tr/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/ug/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/uk/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/vi/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_CN/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_HK/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/locale/zh_TW/LC_MESSAGES
mkdir -p %{buildroot}/usr/share/url-dispatcher/urls
cp debian/apparmor/content-hub-testability                   %{buildroot}/etc/apparmor.d
install -p -m 755 build/tools/send/content-hub-send          %{buildroot}%{_bindir}
install -p -m 755 build/src/com/ubuntu/content/service/content-hub-service %{buildroot}%{_bindir}
cp build/src/com/ubuntu/content/glib/content-hub-glib.h      %{buildroot}%{_includedir}/com/ubuntu/content/glib
cp include/com/ubuntu/content/hub.h                          %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/import_export_handler.h        %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/item.h                         %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/paste.h                        %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/peer.h                         %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/scope.h                        %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/store.h                        %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/transfer.h                     %{buildroot}%{_includedir}/com/ubuntu/content
cp include/com/ubuntu/content/type.h                         %{buildroot}%{_includedir}/com/ubuntu/content
cp build/src/com/ubuntu/content/service/content-hub-peer-hook %{buildroot}%{_libdir}/content-hub
cp build/src/com/ubuntu/content/service/content-hub-peer-hook-wrapper %{buildroot}%{_libdir}/content-hub
ln -s libcontent-hub-glib.so.0 %{buildroot}%{_libdir}/libcontent-hub-glib.so
ln -s libcontent-hub-glib.so.0.2.0 %{buildroot}%{_libdir}/libcontent-hub-glib.so.0
install -p -m 755 build/src/com/ubuntu/content/libcontent-hub-glib.so.0.2.0 %{buildroot}%{_libdir}
ln -s libcontent-hub.so.0 %{buildroot}%{_libdir}/libcontent-hub.so
ln -s libcontent-hub.so.0.2.0 %{buildroot}%{_libdir}/libcontent-hub.so.0
install -p -m 755 build/src/com/ubuntu/content/libcontent-hub.so.0.2.0 %{buildroot}%{_libdir}
cp build/libcontent-hub-glib.pc                              %{buildroot}%{_libdir}/pkgconfig
cp build/libcontent-hub.pc                                   %{buildroot}%{_libdir}/pkgconfig
cp build/import/Ubuntu/Content/ContentPageHeader.qml         %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp build/import/Ubuntu/Content/ContentPeerPicker10.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp build/import/Ubuntu/Content/ContentPeerPicker11.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp build/import/Ubuntu/Content/ContentPeerPicker13.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp build/import/Ubuntu/Content/ContentTransferHint.qml       %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp build/import/Ubuntu/Content/ResponsiveGridView.qml        %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
install -p -m 755 build/import/Ubuntu/Content/libubuntu-content-hub-plugin.so %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp build/import/Ubuntu/Content/plugins.qmltypes              %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp build/import/Ubuntu/Content/qmldir                        %{buildroot}%{_libdir}/qt5/qml/Ubuntu/Content
cp tools/send/content-hub-send.desktop                       %{buildroot}/usr/share/applications
cp tests/peers/exporter/content-hub-test-exporter.desktop    %{buildroot}/usr/share/applications
cp tests/peers/importer/content-hub-test-importer.desktop    %{buildroot}/usr/share/applications
cp tests/peers/sharer/content-hub-test-sharer.desktop        %{buildroot}/usr/share/applications
cp build/src/com/ubuntu/content/service/content-hub.hook     %{buildroot}/usr/share/click/hooks
cp src/com/ubuntu/content/service/xorg.png                   %{buildroot}/usr/share/content-hub/icons
cp tests/peers/data/Joker.vcf                                %{buildroot}/usr/share/content-hub/testability/data
cp "tests/peers/data/Music Ringtone.ogg"                       %{buildroot}/usr/share/content-hub/testability/data
cp tests/peers/data/Stark,_Tony.vcf                          %{buildroot}/usr/share/content-hub/testability/data
cp tests/peers/data/clock.png                                %{buildroot}/usr/share/content-hub/testability/data
cp tests/peers/data/webbrowser-app.png                       %{buildroot}/usr/share/content-hub/testability/data
cp src/com/ubuntu/content/service/com.ubuntu.content.dbus.Service.service %{buildroot}/usr/share/dbus-1/services
cp doc/extra.css                                             %{buildroot}/usr/share/doc/content-hub/cpp/html
cp doc/qml/css/base.css                                      %{buildroot}/usr/share/doc/content-hub/qml/html/style
cp doc/qml/css/qtquick.css                                   %{buildroot}/usr/share/doc/content-hub/qml/html/style
cp doc/qml/css/reset.css                                     %{buildroot}/usr/share/doc/content-hub/qml/html/style
cp doc/qml/css/scratch.css                                   %{buildroot}/usr/share/doc/content-hub/qml/html/style
cp src/com/ubuntu/content/service/com.ubuntu.content.hub.gschema.xml %{buildroot}/usr/share/glib-2.0/schemas
cp tests/peers/exporter/content-hub-test-exporter.png        %{buildroot}/usr/share/icons/hicolor/512x512/apps
cp tests/peers/importer/content-hub-test-importer.png        %{buildroot}/usr/share/icons/hicolor/512x512/apps
cp tests/peers/sharer/content-hub-test-sharer.png            %{buildroot}/usr/share/icons/hicolor/512x512/apps
cp tools/send/content-hub-send.url-dispatcher                %{buildroot}/usr/share/url-dispatcher/urls

# Not Found:/usr/bin/content-hub-test-exporter
# Not Found:/usr/bin/content-hub-test-importer
# Not Found:/usr/bin/content-hub-test-sharer
# Not Found:/usr/share/content-hub/peers/content-hub-test-exporter
# Not Found:/usr/share/content-hub/peers/content-hub-test-importer
# Not Found:/usr/share/content-hub/peers/content-hub-test-sharer
# Not Found:/usr/share/doc/content-hub/cpp/html/CMakeLists_8txt.html
# Not Found:/usr/share/doc/content-hub/cpp/html/Mainpage_8md.html
# Not Found:/usr/share/doc/content-hub/cpp/html/Mainpage_8md_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/annotated.html
# Not Found:/usr/share/doc/content-hub/cpp/html/annotated_dup.js
# Not Found:/usr/share/doc/content-hub/cpp/html/arrowdown.png
# Not Found:/usr/share/doc/content-hub/cpp/html/arrowright.png
# Not Found:/usr/share/doc/content-hub/cpp/html/bc_s.png
# Not Found:/usr/share/doc/content-hub/cpp/html/bdwn.png
# Not Found:/usr/share/doc/content-hub/cpp/html/classQObject.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classQObject__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classQObject__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classQObject__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classQObject__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Hub-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Hub.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Hub.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Hub__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Hub__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Hub__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Hub__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1ImportExportHandler-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1ImportExportHandler.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1ImportExportHandler.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1ImportExportHandler__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1ImportExportHandler__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1ImportExportHandler__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1ImportExportHandler__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Item-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Item.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Item.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Item__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Item__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Item__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Item__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Paste-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Paste.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Paste.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Paste__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Paste__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Paste__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Paste__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Peer-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Peer.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Peer.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Peer__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Peer__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Peer__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Peer__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Store-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Store.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Store.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Store__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Store__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Store__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Store__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Transfer-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Transfer.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Transfer.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Transfer__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Transfer__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Transfer__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Transfer__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Type-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Type.html
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Type.js
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Type__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Type__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Type__inherit__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/classcom_1_1ubuntu_1_1content_1_1Type__inherit__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/classes.html
# Not Found:/usr/share/doc/content-hub/cpp/html/closed.png
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_0b3013440f5abe40898195c930b854ed.html
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_0b3013440f5abe40898195c930b854ed.js
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_aa09ea0eb8fcdf881c09ee68e73ef756.html
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_aa09ea0eb8fcdf881c09ee68e73ef756.js
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_cb4afd1fc165238ed0fcceae9881b796.html
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_cb4afd1fc165238ed0fcceae9881b796.js
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_d44c64559bbebec7f509842c48db8b23.html
# Not Found:/usr/share/doc/content-hub/cpp/html/dir_d44c64559bbebec7f509842c48db8b23.js
# Not Found:/usr/share/doc/content-hub/cpp/html/doc.png
# Not Found:/usr/share/doc/content-hub/cpp/html/doxygen.css
# Not Found:/usr/share/doc/content-hub/cpp/html/doxygen.png
# Not Found:/usr/share/doc/content-hub/cpp/html/dynsections.js
# Not Found:/usr/share/doc/content-hub/cpp/html/files.html
# Not Found:/usr/share/doc/content-hub/cpp/html/files.js
# Not Found:/usr/share/doc/content-hub/cpp/html/folderclosed.png
# Not Found:/usr/share/doc/content-hub/cpp/html/folderopen.png
# Not Found:/usr/share/doc/content-hub/cpp/html/functions.html
# Not Found:/usr/share/doc/content-hub/cpp/html/functions_enum.html
# Not Found:/usr/share/doc/content-hub/cpp/html/functions_eval.html
# Not Found:/usr/share/doc/content-hub/cpp/html/functions_func.html
# Not Found:/usr/share/doc/content-hub/cpp/html/functions_prop.html
# Not Found:/usr/share/doc/content-hub/cpp/html/functions_rela.html
# Not Found:/usr/share/doc/content-hub/cpp/html/functions_vars.html
# Not Found:/usr/share/doc/content-hub/cpp/html/globals.html
# Not Found:/usr/share/doc/content-hub/cpp/html/globals_func.html
# Not Found:/usr/share/doc/content-hub/cpp/html/graph_legend.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/graph_legend.html
# Not Found:/usr/share/doc/content-hub/cpp/html/graph_legend.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/hierarchy.html
# Not Found:/usr/share/doc/content-hub/cpp/html/hierarchy.js
# Not Found:/usr/share/doc/content-hub/cpp/html/hub_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/hub_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/hub_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/hub_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/import__export__handler_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/import__export__handler_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/import__export__handler_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/import__export__handler_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/index.html
# Not Found:/usr/share/doc/content-hub/cpp/html/inherit_graph_0.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/inherit_graph_0.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/inherit_graph_1.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/inherit_graph_1.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/inherit_graph_2.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/inherit_graph_2.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/inherits.html
# Not Found:/usr/share/doc/content-hub/cpp/html/item_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/item_8h.js
# Not Found:/usr/share/doc/content-hub/cpp/html/item_8h__dep__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/item_8h__dep__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/item_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/item_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/item_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/jquery.js
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacecom.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacecom.js
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacecom_1_1ubuntu.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacecom_1_1ubuntu.js
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacecom_1_1ubuntu_1_1content.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacecom_1_1ubuntu_1_1content.js
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacecom_1_1ubuntu_1_1content_1_1detail.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacemembers.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacemembers_enum.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespacemembers_eval.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespaces.html
# Not Found:/usr/share/doc/content-hub/cpp/html/namespaces.js
# Not Found:/usr/share/doc/content-hub/cpp/html/nav_f.png
# Not Found:/usr/share/doc/content-hub/cpp/html/nav_g.png
# Not Found:/usr/share/doc/content-hub/cpp/html/nav_h.png
# Not Found:/usr/share/doc/content-hub/cpp/html/navtree.css
# Not Found:/usr/share/doc/content-hub/cpp/html/navtree.js
# Not Found:/usr/share/doc/content-hub/cpp/html/navtreedata.js
# Not Found:/usr/share/doc/content-hub/cpp/html/navtreeindex0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/open.png
# Not Found:/usr/share/doc/content-hub/cpp/html/paste_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/paste_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/paste_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/paste_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/peer_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/peer_8h.js
# Not Found:/usr/share/doc/content-hub/cpp/html/peer_8h__dep__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/peer_8h__dep__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/peer_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/peer_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/peer_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/qml_2CMakeLists_8txt.html
# Not Found:/usr/share/doc/content-hub/cpp/html/qml_2CMakeLists_8txt_a1612e87651581bc4ad882a5c2ec23763_cgraph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/qml_2CMakeLists_8txt_a1612e87651581bc4ad882a5c2ec23763_cgraph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/resize.js
# Not Found:/usr/share/doc/content-hub/cpp/html/scope_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/scope_8h.js
# Not Found:/usr/share/doc/content-hub/cpp/html/scope_8h__dep__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/scope_8h__dep__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/scope_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/scope_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/scope_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_10.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_10.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_11.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_11.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_12.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_12.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_13.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_13.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_2.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_2.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_3.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_3.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_4.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_4.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_5.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_5.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_6.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_6.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_7.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_7.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_8.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_8.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_9.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_9.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_a.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_a.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_b.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_b.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_c.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_c.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_d.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_d.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_e.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_e.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_f.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/all_f.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_2.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_2.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_3.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_3.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_4.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_4.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_5.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_5.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_6.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_6.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_7.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/classes_7.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/close.png
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enums_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enums_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enums_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enums_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_2.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_2.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_3.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_3.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_4.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_4.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_5.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_5.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_6.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_6.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_7.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_7.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_8.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/enumvalues_8.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_2.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_2.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_3.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_3.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_4.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_4.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_5.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_5.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_6.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/files_6.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_10.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_10.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_11.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_11.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_12.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_12.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_13.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_13.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_2.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_2.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_3.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_3.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_4.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_4.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_5.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_5.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_6.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_6.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_7.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_7.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_8.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_8.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_9.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_9.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_a.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_a.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_b.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_b.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_c.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_c.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_d.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_d.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_e.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_e.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_f.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/functions_f.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/mag_sel.png
# Not Found:/usr/share/doc/content-hub/cpp/html/search/namespaces_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/namespaces_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/nomatches.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/pages_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/pages_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_2.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_2.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_3.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_3.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_4.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_4.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_5.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_5.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_6.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_6.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_7.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/properties_7.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_1.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_1.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_2.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_2.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_3.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_3.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_4.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/related_4.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/search.css
# Not Found:/usr/share/doc/content-hub/cpp/html/search/search.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/search_l.png
# Not Found:/usr/share/doc/content-hub/cpp/html/search/search_m.png
# Not Found:/usr/share/doc/content-hub/cpp/html/search/search_r.png
# Not Found:/usr/share/doc/content-hub/cpp/html/search/searchdata.js
# Not Found:/usr/share/doc/content-hub/cpp/html/search/variables_0.html
# Not Found:/usr/share/doc/content-hub/cpp/html/search/variables_0.js
# Not Found:/usr/share/doc/content-hub/cpp/html/splitbar.png
# Not Found:/usr/share/doc/content-hub/cpp/html/store_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/store_8h__dep__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/store_8h__dep__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/store_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/store_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/store_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Hub_1_1Client-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Hub_1_1Client.html
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Hub_1_1Client__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Hub_1_1Client__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Type_1_1Known-members.html
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Type_1_1Known.html
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Type_1_1Known__coll__graph.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/structcom_1_1ubuntu_1_1content_1_1Type_1_1Known__coll__graph.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/svgpan.js
# Not Found:/usr/share/doc/content-hub/cpp/html/sync_off.png
# Not Found:/usr/share/doc/content-hub/cpp/html/sync_on.png
# Not Found:/usr/share/doc/content-hub/cpp/html/tab_a.png
# Not Found:/usr/share/doc/content-hub/cpp/html/tab_b.png
# Not Found:/usr/share/doc/content-hub/cpp/html/tab_h.png
# Not Found:/usr/share/doc/content-hub/cpp/html/tab_s.png
# Not Found:/usr/share/doc/content-hub/cpp/html/tabs.css
# Not Found:/usr/share/doc/content-hub/cpp/html/transfer_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/transfer_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/transfer_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/transfer_8h_source.html
# Not Found:/usr/share/doc/content-hub/cpp/html/type_8h.html
# Not Found:/usr/share/doc/content-hub/cpp/html/type_8h__dep__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/type_8h__dep__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/type_8h__incl.dot
# Not Found:/usr/share/doc/content-hub/cpp/html/type_8h__incl.md5
# Not Found:/usr/share/doc/content-hub/cpp/html/type_8h_source.html
# Not Found:/usr/share/doc/content-hub/qml/html/index.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenthandler-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenthandler.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenthub-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenthub.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentitem-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentitem.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentpeer-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentpeer.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentpeermodel-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentpeermodel.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentpeerpicker-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentpeerpicker.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentscope-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentscope.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentstore-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contentstore.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenttransfer-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenttransfer.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenttransferhint-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenttransferhint.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenttype-members.html
# Not Found:/usr/share/doc/content-hub/qml/html/qml-ubuntu-content-contenttype.html
# Not Found:/usr/share/doc/content-hub/qml/html/ubuntu-content-qml-api.index.gz
# Not Found:/usr/share/doc/content-hub/qml/html/ubuntu-content-qmlmodule.html
# Not Found:/usr/share/lintian/overrides/qtdeclarative5-ubuntu-content1
# Not Found:/usr/share/locale/aa/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/am/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ar/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ast/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/az/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/be/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/bg/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/br/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/bs/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ca/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ca@valencia/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/cs/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/cy/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/da/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/de/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/el/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/en_AU/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/en_GB/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/eo/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/es/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/et/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/eu/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/fa/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/fi/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/fo/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/fr/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/fr_CA/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ga/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/gd/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/gl/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/he/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/hr/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/hu/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ia/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/id/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/is/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/it/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ja/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/kk/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/km/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/kn/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ko/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ln/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/lv/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ms/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/my/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/nb/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ne/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/nl/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/oc/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/om/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/pa/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/pl/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/pt/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/pt_BR/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ro/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ru/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/si/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/sk/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/sl/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/sq/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/sr/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/sv/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ta/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/th/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/tr/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/ug/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/uk/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/vi/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/zh_CN/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/zh_HK/LC_MESSAGES/content-hub.mo
# Not Found:/usr/share/locale/zh_TW/LC_MESSAGES/content-hub.mo


%files
/etc/apparmor.d/content-hub-testability
%{_bindir}/content-hub-send
%{_bindir}/content-hub-service
%{_libdir}/content-hub/content-hub-peer-hook
%{_libdir}/content-hub/content-hub-peer-hook-wrapper
%{_libdir}/libcontent-hub-glib.so.0
%{_libdir}/libcontent-hub-glib.so.0.2.0
%{_libdir}/libcontent-hub.so.0
%{_libdir}/libcontent-hub.so.0.2.0
%{_libdir}/qt5/qml/Ubuntu/Content/ContentPageHeader.qml
%{_libdir}/qt5/qml/Ubuntu/Content/ContentPeerPicker10.qml
%{_libdir}/qt5/qml/Ubuntu/Content/ContentPeerPicker11.qml
%{_libdir}/qt5/qml/Ubuntu/Content/ContentPeerPicker13.qml
%{_libdir}/qt5/qml/Ubuntu/Content/ContentTransferHint.qml
%{_libdir}/qt5/qml/Ubuntu/Content/ResponsiveGridView.qml
%{_libdir}/qt5/qml/Ubuntu/Content/libubuntu-content-hub-plugin.so
%{_libdir}/qt5/qml/Ubuntu/Content/plugins.qmltypes
%{_libdir}/qt5/qml/Ubuntu/Content/qmldir
/usr/share/applications/content-hub-send.desktop
/usr/share/applications/content-hub-test-exporter.desktop
/usr/share/applications/content-hub-test-importer.desktop
/usr/share/applications/content-hub-test-sharer.desktop
/usr/share/click/hooks/content-hub.hook
/usr/share/content-hub/icons/xorg.png
/usr/share/content-hub/testability/data/Joker.vcf
"/usr/share/content-hub/testability/data/Music Ringtone.ogg"
/usr/share/content-hub/testability/data/Stark,_Tony.vcf
/usr/share/content-hub/testability/data/clock.png
/usr/share/content-hub/testability/data/webbrowser-app.png
/usr/share/dbus-1/services/com.ubuntu.content.dbus.Service.service
/usr/share/doc/content-hub/cpp/html/extra.css
/usr/share/doc/content-hub/qml/html/style/base.css
/usr/share/doc/content-hub/qml/html/style/qtquick.css
/usr/share/doc/content-hub/qml/html/style/reset.css
/usr/share/doc/content-hub/qml/html/style/scratch.css
/usr/share/glib-2.0/schemas/com.ubuntu.content.hub.gschema.xml
/usr/share/icons/hicolor/512x512/apps/content-hub-test-exporter.png
/usr/share/icons/hicolor/512x512/apps/content-hub-test-importer.png
/usr/share/icons/hicolor/512x512/apps/content-hub-test-sharer.png
/usr/share/url-dispatcher/urls/content-hub-send.url-dispatcher

%files devel
%{_includedir}/com/ubuntu/content/glib/content-hub-glib.h
%{_includedir}/com/ubuntu/content/hub.h
%{_includedir}/com/ubuntu/content/import_export_handler.h
%{_includedir}/com/ubuntu/content/item.h
%{_includedir}/com/ubuntu/content/paste.h
%{_includedir}/com/ubuntu/content/peer.h
%{_includedir}/com/ubuntu/content/scope.h
%{_includedir}/com/ubuntu/content/store.h
%{_includedir}/com/ubuntu/content/transfer.h
%{_includedir}/com/ubuntu/content/type.h
%{_libdir}/libcontent-hub-glib.so
%{_libdir}/libcontent-hub.so
%{_libdir}/pkgconfig/libcontent-hub-glib.pc
%{_libdir}/pkgconfig/libcontent-hub.pc
