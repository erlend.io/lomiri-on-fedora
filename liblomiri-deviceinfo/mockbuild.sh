#!/bin/bash

# exit when any command fails
set -e

# Source setup
source ../settings/env

function createresultdir()
{
if [ -e "${RESULTDIR}" ]; then
    echo Hei
else
    mkdir $RESULTDIR
fi
}

# initialize mock
mock -r fedora-$TVERSION_NO-$TARCH --init

echo ---------------------------------------------------------------
echo Building : liblomiri-deviceinfo
echo Deps     : 
echo ---------------------------------------------------------------
createresultdir
cp $GIT_CHECKOUT/liblomiri-deviceinfo/liblomiri-deviceinfo.spec  $RPMBUILD_DIR/SPECS
spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-deviceinfo.spec
rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-deviceinfo.spec
mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-deviceinfo-0.1-2.fc32.src.rpm
cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-deviceinfo-0.1-2.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-deviceinfo-0.1-2.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH

