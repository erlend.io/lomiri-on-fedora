Name:       liblomiri-deviceinfo
Version:    0.1
Release:    2%{?dist}
Summary:    Library to detect and configure devices
License:    GPL-3
URL:        https://github.com/ubports/deviceinfo
Source0:    https://github.com/ubports/deviceinfo/archive/06e02d49a4e8100cfcca955d61eff0af3f6af4fb/deviceinfo.tar.gz
Patch0:     0001-dummy-patch.patch

BuildRequires: cmake
BuildRequires: gcc-c++

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Library to detect and configure devices
Library to detect and configure devices

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n deviceinfo-06e02d49a4e8100cfcca955d61eff0af3f6af4fb

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/deviceinfo
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/etc/device-info/sensorfw
cp configs/alias.conf                                        %{buildroot}/etc/device-info
cp configs/default.conf                                      %{buildroot}/etc/device-info
cp configs/librem5.conf                                      %{buildroot}/etc/device-info
cp configs/pinebook.conf                                     %{buildroot}/etc/device-info
cp configs/pinephone.conf                                    %{buildroot}/etc/device-info
cp configs/pinetab.conf                                      %{buildroot}/etc/device-info
cp configs/sensorfw/hybris.conf                              %{buildroot}/etc/device-info/sensorfw
cp configs/sensorfw/pinephone.conf                           %{buildroot}/etc/device-info/sensorfw
cp headers/deviceinfo.h                                      %{buildroot}%{_includedir}/deviceinfo
ln -s libdeviceinfo.so.0 %{buildroot}%{_libdir}/libdeviceinfo.so
ln -s libdeviceinfo.so.0.1.0 %{buildroot}%{_libdir}/libdeviceinfo.so.0
install -p -m 755 build/src/libdeviceinfo.so.0.1.0           %{buildroot}%{_libdir}
cp build/headers/deviceinfo.pc                               %{buildroot}%{_libdir}/pkgconfig

%files
/etc/device-info/alias.conf
/etc/device-info/default.conf
/etc/device-info/librem5.conf
/etc/device-info/pinebook.conf
/etc/device-info/pinephone.conf
/etc/device-info/pinetab.conf
/etc/device-info/sensorfw/hybris.conf
/etc/device-info/sensorfw/pinephone.conf
%{_libdir}/libdeviceinfo.so.0
%{_libdir}/libdeviceinfo.so.0.1.0

%files devel
%{_includedir}/deviceinfo/deviceinfo.h
%{_libdir}/libdeviceinfo.so
%{_libdir}/pkgconfig/deviceinfo.pc
