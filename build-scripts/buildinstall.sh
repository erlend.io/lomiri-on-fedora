#!/bin/bash
# exit when any command fails
set -e

# Source setup
if [ "$#" -eq 0 ]; then
    source ../settings/env
else
    source ./$1
fi

# Get package list
rpm -qa > $TEMP_FILE

function is_installed()
{
for pkg in $(cat $TEMP_FILE)
do
    if [ $pkg = $1 ]; then
        return 0
    fi
done
return 1
}

cd $RPMBUILD_DIR/SPECS
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-deviceinfo-0.1-2.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-deviceinfo
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-deviceinfo/liblomiri-deviceinfo.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-deviceinfo/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-deviceinfo.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-deviceinfo.spec
    rpmbuild -ba -vv liblomiri-deviceinfo.spec
fi

if is_installed liblomiri-deviceinfo-0.1-2.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-deviceinfo
else
    echo installing:liblomiri-deviceinfo
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-deviceinfo
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-deviceinfo-0.1-2.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-deviceinfo-devel-0.1-2.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-deviceinfo-devel
else
    echo installing:liblomiri-deviceinfo-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-deviceinfo-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-deviceinfo-devel-0.1-2.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-upstart-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-upstart
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-upstart/liblomiri-upstart.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-upstart/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-upstart.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-upstart.spec
    rpmbuild -ba -vv liblomiri-upstart.spec
fi

if is_installed liblomiri-upstart-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-upstart
else
    echo installing:liblomiri-upstart
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-upstart
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-upstart-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-upstart-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-upstart-devel
else
    echo installing:liblomiri-upstart-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-upstart-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-upstart-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-dbus-test-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qt-dbus-test
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-qt-dbus-test/liblomiri-qt-dbus-test.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qt-dbus-test/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-qt-dbus-test.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-qt-dbus-test.spec
    rpmbuild -ba -vv liblomiri-qt-dbus-test.spec
fi

if is_installed liblomiri-qt-dbus-test-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-qt-dbus-test
else
    echo installing:liblomiri-qt-dbus-test
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qt-dbus-test
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-dbus-test-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-qt-dbus-test-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-qt-dbus-test-devel
else
    echo installing:liblomiri-qt-dbus-test-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qt-dbus-test-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-dbus-test-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-libertine-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-libertine
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-libertine/liblomiri-libertine.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-libertine/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-libertine.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-libertine.spec
    rpmbuild -ba -vv liblomiri-libertine.spec
fi

if is_installed liblomiri-libertine-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-libertine
else
    echo installing:liblomiri-libertine
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-libertine
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-libertine-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-libertine-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-libertine-devel
else
    echo installing:liblomiri-libertine-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-libertine-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-libertine-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-dbusmock-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qt-dbusmock
    echo Deps     : liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-qt-dbusmock/liblomiri-qt-dbusmock.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qt-dbusmock/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-qt-dbusmock.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-qt-dbusmock.spec
    rpmbuild -ba -vv liblomiri-qt-dbusmock.spec
fi

if is_installed liblomiri-qt-dbusmock-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-qt-dbusmock
else
    echo installing:liblomiri-qt-dbusmock
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qt-dbusmock
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-dbusmock-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-qt-dbusmock-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-qt-dbusmock-devel
else
    echo installing:liblomiri-qt-dbusmock-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qt-dbusmock-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-dbusmock-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/qt5-mobility-feedback-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : qt5-mobility-feedback
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/qt5-mobility-feedback/qt5-mobility-feedback.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/qt5-mobility-feedback/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R qt5-mobility-feedback.spec
    echo $PASSWORD | sudo -S dnf builddep -y qt5-mobility-feedback.spec
    rpmbuild -ba -vv qt5-mobility-feedback.spec
fi

if is_installed qt5-mobility-feedback-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed qt5-mobility-feedback
else
    echo installing:qt5-mobility-feedback
    echo $PASSWORD | sudo -S dnf remove -y qt5-mobility-feedback
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/qt5-mobility-feedback-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed qt5-mobility-feedback-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed qt5-mobility-feedback-devel
else
    echo installing:qt5-mobility-feedback-devel
    echo $PASSWORD | sudo -S dnf remove -y qt5-mobility-feedback-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/qt5-mobility-feedback-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/qt5-mobility-pim-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : qt5-mobility-pim
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/qt5-mobility-pim/qt5-mobility-pim.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/qt5-mobility-pim/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R qt5-mobility-pim.spec
    echo $PASSWORD | sudo -S dnf builddep -y qt5-mobility-pim.spec
    rpmbuild -ba -vv qt5-mobility-pim.spec
fi

if is_installed qt5-mobility-pim-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed qt5-mobility-pim
else
    echo installing:qt5-mobility-pim
    echo $PASSWORD | sudo -S dnf remove -y qt5-mobility-pim
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/qt5-mobility-pim-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed qt5-mobility-pim-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed qt5-mobility-pim-devel
else
    echo installing:qt5-mobility-pim-devel
    echo $PASSWORD | sudo -S dnf remove -y qt5-mobility-pim-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/qt5-mobility-pim-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-dbus-test-runner-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-dbus-test-runner
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri-dbus-test-runner/lomiri-dbus-test-runner.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-dbus-test-runner/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R lomiri-dbus-test-runner.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri-dbus-test-runner.spec
    rpmbuild -ba -vv lomiri-dbus-test-runner.spec
fi

if is_installed lomiri-dbus-test-runner-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri-dbus-test-runner
else
    echo installing:lomiri-dbus-test-runner
    echo $PASSWORD | sudo -S dnf remove -y lomiri-dbus-test-runner
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-dbus-test-runner-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-session-0.1-2.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-session
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri-session/lomiri-session.spec  $RPMBUILD_DIR/SPECS
    spectool -g -R lomiri-session.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri-session.spec
    rpmbuild -ba -vv lomiri-session.spec
fi

if is_installed lomiri-session-0.1-2.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri-session
else
    echo installing:lomiri-session
    echo $PASSWORD | sudo -S dnf remove -y lomiri-session
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-session-0.1-2.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-download-manager-1.0-2.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-download-manager
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-download-manager/liblomiri-download-manager.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-download-manager/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-download-manager.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-download-manager.spec
    rpmbuild -ba -vv liblomiri-download-manager.spec
fi

if is_installed liblomiri-download-manager-1.0-2.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-download-manager
else
    echo installing:liblomiri-download-manager
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-download-manager
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-download-manager-1.0-2.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-download-manager-devel-1.0-2.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-download-manager-devel
else
    echo installing:liblomiri-download-manager-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-download-manager-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-download-manager-devel-1.0-2.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-api-0.1-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-api
    echo Deps     : liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-api/liblomiri-api.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-api/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-api.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-api.spec
    rpmbuild -ba -vv liblomiri-api.spec
fi

if is_installed liblomiri-api-0.1-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-api
else
    echo installing:liblomiri-api
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-api
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-api-0.1-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-api-devel-0.1-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-api-devel
else
    echo installing:liblomiri-api-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-api-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-api-devel-0.1-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-systems-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qt-systems
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-qt-systems/liblomiri-qt-systems.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qt-systems/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-qt-systems.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-qt-systems.spec
    rpmbuild -ba -vv liblomiri-qt-systems.spec
fi

if is_installed liblomiri-qt-systems-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-qt-systems
else
    echo installing:liblomiri-qt-systems
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qt-systems
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-systems-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-qt-systems-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-qt-systems-devel
else
    echo installing:liblomiri-qt-systems-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qt-systems-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qt-systems-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-ui-toolkit-0.9-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-ui-toolkit
    echo Deps     : liblomiri-qt-systems, qt5-mobility-pim, qt5-mobility-feedback
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri-ui-toolkit/lomiri-ui-toolkit.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-ui-toolkit/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R lomiri-ui-toolkit.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri-ui-toolkit.spec
    rpmbuild -ba -vv lomiri-ui-toolkit.spec
fi

if is_installed lomiri-ui-toolkit-0.9-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri-ui-toolkit
else
    echo installing:lomiri-ui-toolkit
    echo $PASSWORD | sudo -S dnf remove -y lomiri-ui-toolkit
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-ui-toolkit-0.9-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed lomiri-ui-toolkit-devel-0.9-1.$FEDORA_VERSION.$ARCH; then
    echo already installed lomiri-ui-toolkit-devel
else
    echo installing:lomiri-ui-toolkit-devel
    echo $PASSWORD | sudo -S dnf remove -y lomiri-ui-toolkit-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-ui-toolkit-devel-0.9-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-telefon-service-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-telefon-service
    echo Deps     : qt5-mobility-pim, qt5-mobility-feedback
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-telefon-service/liblomiri-telefon-service.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-telefon-service/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-telefon-service.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-telefon-service.spec
    rpmbuild -ba -vv liblomiri-telefon-service.spec
fi

if is_installed liblomiri-telefon-service-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-telefon-service
else
    echo installing:liblomiri-telefon-service
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-telefon-service
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-telefon-service-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-qt-dbus-test-runner-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-qt-dbus-test-runner
    echo Deps     : liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri-qt-dbus-test-runner/lomiri-qt-dbus-test-runner.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-qt-dbus-test-runner/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R lomiri-qt-dbus-test-runner.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri-qt-dbus-test-runner.spec
    rpmbuild -ba -vv lomiri-qt-dbus-test-runner.spec
fi

if is_installed lomiri-qt-dbus-test-runner-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri-qt-dbus-test-runner
else
    echo installing:lomiri-qt-dbus-test-runner
    echo $PASSWORD | sudo -S dnf remove -y lomiri-qt-dbus-test-runner
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-qt-dbus-test-runner-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-cpp-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-cpp
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-cpp/liblomiri-cpp.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-cpp/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-cpp.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-cpp.spec
    rpmbuild -ba -vv liblomiri-cpp.spec
fi

if is_installed liblomiri-cpp-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-cpp
else
    echo installing:liblomiri-cpp
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-cpp
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-cpp-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-cpp-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-cpp-devel
else
    echo installing:liblomiri-cpp-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-cpp-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-cpp-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-geonames-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-geonames
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-geonames/liblomiri-geonames.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-geonames/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-geonames.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-geonames.spec
    rpmbuild -ba -vv liblomiri-geonames.spec
fi

if is_installed liblomiri-geonames-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-geonames
else
    echo installing:liblomiri-geonames
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-geonames
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-geonames-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-geonames-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-geonames-devel
else
    echo installing:liblomiri-geonames-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-geonames-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-geonames-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qmenumodel-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qmenumodel
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-qmenumodel/liblomiri-qmenumodel.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qmenumodel/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-qmenumodel.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-qmenumodel.spec
    rpmbuild -ba -vv liblomiri-qmenumodel.spec
fi

if is_installed liblomiri-qmenumodel-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-qmenumodel
else
    echo installing:liblomiri-qmenumodel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qmenumodel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qmenumodel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-qmenumodel-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-qmenumodel-devel
else
    echo installing:liblomiri-qmenumodel-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qmenumodel-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qmenumodel-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-click-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-click
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-click/liblomiri-click.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-click/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-click.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-click.spec
    rpmbuild -ba -vv liblomiri-click.spec
fi

if is_installed liblomiri-click-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-click
else
    echo installing:liblomiri-click
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-click
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-click-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-click-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-click-devel
else
    echo installing:liblomiri-click-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-click-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-click-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-dbus-test-0.9-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-dbus-test
    echo Deps     : 
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-dbus-test/liblomiri-dbus-test.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-dbus-test/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-dbus-test.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-dbus-test.spec
    rpmbuild -ba -vv liblomiri-dbus-test.spec
fi

if is_installed liblomiri-dbus-test-0.9-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-dbus-test
else
    echo installing:liblomiri-dbus-test
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-dbus-test
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-dbus-test-0.9-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-dbus-test-devel-0.9-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-dbus-test-devel
else
    echo installing:liblomiri-dbus-test-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-dbus-test-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-dbus-test-devel-0.9-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-system-compositor-0.9-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-system-compositor
    echo Deps     : liblomiri-deviceinfo
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri-system-compositor/lomiri-system-compositor.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-system-compositor/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R lomiri-system-compositor.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri-system-compositor.spec
    rpmbuild -ba -vv lomiri-system-compositor.spec
fi

if is_installed lomiri-system-compositor-0.9-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri-system-compositor
else
    echo installing:lomiri-system-compositor
    echo $PASSWORD | sudo -S dnf remove -y lomiri-system-compositor
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-system-compositor-0.9-1.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-thumbnailer-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-thumbnailer
    echo Deps     : liblomiri-api, liblomiri-cpp, liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-thumbnailer/liblomiri-thumbnailer.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-thumbnailer/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-thumbnailer.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-thumbnailer.spec
    rpmbuild -ba -vv liblomiri-thumbnailer.spec
fi

if is_installed liblomiri-thumbnailer-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-thumbnailer
else
    echo installing:liblomiri-thumbnailer
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-thumbnailer
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-thumbnailer-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-thumbnailer-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-thumbnailer-devel
else
    echo installing:liblomiri-thumbnailer-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-thumbnailer-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-thumbnailer-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-app-launch-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-app-launch
    echo Deps     : liblomiri-cpp, liblomiri-click, liblomiri-upstart, liblomiri-dbus-test, liblomiri-libertine
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-app-launch/liblomiri-app-launch.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-app-launch/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-app-launch.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-app-launch.spec
    rpmbuild -ba -vv liblomiri-app-launch.spec
fi

if is_installed liblomiri-app-launch-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-app-launch
else
    echo installing:liblomiri-app-launch
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-app-launch
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-app-launch-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-app-launch-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-app-launch-devel
else
    echo installing:liblomiri-app-launch-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-app-launch-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-app-launch-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-dbus-cpp-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-dbus-cpp
    echo Deps     : liblomiri-cpp
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-dbus-cpp/liblomiri-dbus-cpp.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-dbus-cpp/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-dbus-cpp.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-dbus-cpp.spec
    rpmbuild -ba -vv liblomiri-dbus-cpp.spec
fi

if is_installed liblomiri-dbus-cpp-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-dbus-cpp
else
    echo installing:liblomiri-dbus-cpp
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-dbus-cpp
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-dbus-cpp-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-dbus-cpp-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-dbus-cpp-devel
else
    echo installing:liblomiri-dbus-cpp-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-dbus-cpp-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-dbus-cpp-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-biometryd-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-biometryd
    echo Deps     : liblomiri-dbus-cpp, liblomiri-cpp
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri-biometryd/lomiri-biometryd.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-biometryd/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R lomiri-biometryd.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri-biometryd.spec
    rpmbuild -ba -vv lomiri-biometryd.spec
fi

if is_installed lomiri-biometryd-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri-biometryd
else
    echo installing:lomiri-biometryd
    echo $PASSWORD | sudo -S dnf remove -y lomiri-biometryd
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-biometryd-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-messing-menu-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-messing-menu
    echo Deps     : liblomiri-dbus-test
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-messing-menu/liblomiri-messing-menu.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-messing-menu/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-messing-menu.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-messing-menu.spec
    rpmbuild -ba -vv liblomiri-messing-menu.spec
fi

if is_installed liblomiri-messing-menu-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-messing-menu
else
    echo installing:liblomiri-messing-menu
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-messing-menu
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-messing-menu-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-messing-menu-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-messing-menu-devel
else
    echo installing:liblomiri-messing-menu-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-messing-menu-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-messing-menu-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-history-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-history
    echo Deps     : qt5-mobility-pim
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-history/liblomiri-history.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-history/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-history.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-history.spec
    rpmbuild -ba -vv liblomiri-history.spec
fi

if is_installed liblomiri-history-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-history
else
    echo installing:liblomiri-history
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-history
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-history-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-history-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-history-devel
else
    echo installing:liblomiri-history-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-history-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-history-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-gmenuharness-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-gmenuharness
    echo Deps     : liblomiri-api, liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-gmenuharness/liblomiri-gmenuharness.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-gmenuharness/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-gmenuharness.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-gmenuharness.spec
    rpmbuild -ba -vv liblomiri-gmenuharness.spec
fi

if is_installed liblomiri-gmenuharness-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-gmenuharness
else
    echo installing:liblomiri-gmenuharness
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-gmenuharness
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-gmenuharness-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-gmenuharness-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-gmenuharness-devel
else
    echo installing:liblomiri-gmenuharness-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-gmenuharness-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-gmenuharness-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-content-hub-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-content-hub
    echo Deps     : liblomiri-cpp, lomiri-ui-toolkit, liblomiri-app-launch, liblomiri-libertine, liblomiri-download-manager
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-content-hub/liblomiri-content-hub.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-content-hub/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-content-hub.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-content-hub.spec
    rpmbuild -ba -vv liblomiri-content-hub.spec
fi

if is_installed liblomiri-content-hub-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-content-hub
else
    echo installing:liblomiri-content-hub
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-content-hub
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-content-hub-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-content-hub-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-content-hub-devel
else
    echo installing:liblomiri-content-hub-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-content-hub-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-content-hub-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-usermetrics-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-usermetrics
    echo Deps     : liblomiri-click, liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-usermetrics/liblomiri-usermetrics.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-usermetrics/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-usermetrics.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-usermetrics.spec
    rpmbuild -ba -vv liblomiri-usermetrics.spec
fi

if is_installed liblomiri-usermetrics-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-usermetrics
else
    echo installing:liblomiri-usermetrics
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-usermetrics
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-usermetrics-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-usermetrics-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-usermetrics-devel
else
    echo installing:liblomiri-usermetrics-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-usermetrics-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-usermetrics-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-biometryd-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-biometryd
    echo Deps     : liblomiri-dbus-cpp, liblomiri-cpp
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-biometryd/liblomiri-biometryd.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-biometryd/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-biometryd.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-biometryd.spec
    rpmbuild -ba -vv liblomiri-biometryd.spec
fi

if is_installed liblomiri-biometryd-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-biometryd
else
    echo installing:liblomiri-biometryd
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-biometryd
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-biometryd-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-biometryd-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-biometryd-devel
else
    echo installing:liblomiri-biometryd-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-biometryd-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-biometryd-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-url-dispatcher-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-url-dispatcher
    echo Deps     : liblomiri-app-launch, liblomiri-dbus-test, liblomiri-click
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-url-dispatcher/liblomiri-url-dispatcher.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-url-dispatcher/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-url-dispatcher.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-url-dispatcher.spec
    rpmbuild -ba -vv liblomiri-url-dispatcher.spec
fi

if is_installed liblomiri-url-dispatcher-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-url-dispatcher
else
    echo installing:liblomiri-url-dispatcher
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-url-dispatcher
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-url-dispatcher-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-url-dispatcher-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-url-dispatcher-devel
else
    echo installing:liblomiri-url-dispatcher-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-url-dispatcher-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-url-dispatcher-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-connectivity-qt-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-connectivity-qt
    echo Deps     : liblomiri-api, liblomiri-url-dispatcher, liblomiri-qt-dbus-test, liblomiri-qt-dbusmock, liblomiri-gmenuharness
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-connectivity-qt/liblomiri-connectivity-qt.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-connectivity-qt/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-connectivity-qt.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-connectivity-qt.spec
    rpmbuild -ba -vv liblomiri-connectivity-qt.spec
fi

if is_installed liblomiri-connectivity-qt-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-connectivity-qt
else
    echo installing:liblomiri-connectivity-qt
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-connectivity-qt
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-connectivity-qt-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if is_installed liblomiri-connectivity-qt-devel-1.0-1.$FEDORA_VERSION.$ARCH; then
    echo already installed liblomiri-connectivity-qt-devel
else
    echo installing:liblomiri-connectivity-qt-devel
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-connectivity-qt-devel
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-connectivity-qt-devel-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi
if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-qml-settings-components-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-qml-settings-components
    echo Deps     : liblomiri-thumbnailer
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri-qml-settings-components/lomiri-qml-settings-components.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-qml-settings-components/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R lomiri-qml-settings-components.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri-qml-settings-components.spec
    rpmbuild -ba -vv lomiri-qml-settings-components.spec
fi

if is_installed lomiri-qml-settings-components-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri-qml-settings-components
else
    echo installing:lomiri-qml-settings-components
    echo $PASSWORD | sudo -S dnf remove -y lomiri-qml-settings-components
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-qml-settings-components-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/lomiri-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri
    echo Deps     : lomiri-dbus-test-runner, liblomiri-cpp, liblomiri-deviceinfo, liblomiri-api, liblomiri-geonames, liblomiri-qmenumodel, liblomiri-app-launch, lomiri-ui-toolkit, liblomiri-qt-dbus-test, liblomiri-qt-dbusmock, liblomiri-usermetrics, liblomiri-download-manager, liblomiri-connectivity-qt
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/lomiri/lomiri.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R lomiri.spec
    echo $PASSWORD | sudo -S dnf builddep -y lomiri.spec
    rpmbuild -ba -vv lomiri.spec
fi

if is_installed lomiri-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed lomiri
else
    echo installing:lomiri
    echo $PASSWORD | sudo -S dnf remove -y lomiri
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/lomiri-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qtmir-1.0-1.$FEDORA_VERSION.$ARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qtmir
    echo Deps     : liblomiri-cpp, liblomiri-app-launch, liblomiri-url-dispatcher, liblomiri-qt-dbus-test, liblomiri-qt-dbusmock, liblomiri-api, liblomiri-content-hub
    echo ---------------------------------------------------------------
    cp $GIT_CHECKOUT/liblomiri-qtmir/liblomiri-qtmir.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qtmir/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R liblomiri-qtmir.spec
    echo $PASSWORD | sudo -S dnf builddep -y liblomiri-qtmir.spec
    rpmbuild -ba -vv liblomiri-qtmir.spec
fi

if is_installed liblomiri-qtmir-1.0-1.$FEDORA_VERSION.$ARCH ; then
    echo already installed liblomiri-qtmir
else
    echo installing:liblomiri-qtmir
    echo $PASSWORD | sudo -S dnf remove -y liblomiri-qtmir
    echo $PASSWORD | sudo -S rpm -i $RPMBUILD_DIR/RPMS/$ARCH/liblomiri-qtmir-1.0-1.$FEDORA_VERSION.$ARCH.rpm
fi

