#!/bin/bash

# exit when any command fails
set -e

# Source setup
if [ "$#" -eq 0 ]; then
    source ../settings/env
else
    source ./$1
fi

function createresultdir()
{
if [ -e "${RESULTDIR}" ]; then
    echo Hei
else
    mkdir $RESULTDIR
fi
}

# initialize mock
mock -r fedora-$TVERSION_NO-$TARCH --init

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-deviceinfo-0.1-2.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-deviceinfo
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-deviceinfo/liblomiri-deviceinfo.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-deviceinfo/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-deviceinfo.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-deviceinfo.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-deviceinfo-0.1-2.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-deviceinfo-0.1-2.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-deviceinfo-0.1-2.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-upstart-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-upstart
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-upstart/liblomiri-upstart.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-upstart/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-upstart.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-upstart.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-upstart-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-upstart-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-upstart-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-qt-dbus-test-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qt-dbus-test
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-qt-dbus-test/liblomiri-qt-dbus-test.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qt-dbus-test/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-qt-dbus-test.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-qt-dbus-test.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-qt-dbus-test-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qt-dbus-test-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qt-dbus-test-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-libertine-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-libertine
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-libertine/liblomiri-libertine.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-libertine/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-libertine.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-libertine.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-libertine-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-libertine-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-libertine-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-qt-dbusmock-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qt-dbusmock
    echo Deps     : liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-qt-dbusmock/liblomiri-qt-dbusmock.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qt-dbusmock/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-qt-dbusmock.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-qt-dbusmock.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-qt-dbusmock-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qt-dbusmock-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qt-dbusmock-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/qt5-mobility-feedback-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : qt5-mobility-feedback
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/qt5-mobility-feedback/qt5-mobility-feedback.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/qt5-mobility-feedback/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/qt5-mobility-feedback.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/qt5-mobility-feedback.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/qt5-mobility-feedback-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/qt5-mobility-feedback-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/qt5-mobility-feedback-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/qt5-mobility-pim-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : qt5-mobility-pim
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/qt5-mobility-pim/qt5-mobility-pim.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/qt5-mobility-pim/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/qt5-mobility-pim.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/qt5-mobility-pim.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/qt5-mobility-pim-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/qt5-mobility-pim-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/qt5-mobility-pim-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-dbus-test-runner-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-dbus-test-runner
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri-dbus-test-runner/lomiri-dbus-test-runner.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-dbus-test-runner/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri-dbus-test-runner.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri-dbus-test-runner.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-dbus-test-runner-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-dbus-test-runner-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-dbus-test-runner-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-session-0.1-2.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-session
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri-session/lomiri-session.spec  $RPMBUILD_DIR/SPECS
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri-session.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri-session.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-session-0.1-2.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-session-0.1-2.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-session-0.1-2.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-download-manager-1.0-2.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-download-manager
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-download-manager/liblomiri-download-manager.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-download-manager/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-download-manager.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-download-manager.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-download-manager-1.0-2.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-download-manager-1.0-2.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-download-manager-1.0-2.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-api-0.1-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-api
    echo Deps     : liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-api/liblomiri-api.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-api/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-api.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-api.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-api-0.1-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-api-0.1-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-api-0.1-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-qt-systems-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qt-systems
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-qt-systems/liblomiri-qt-systems.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qt-systems/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-qt-systems.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-qt-systems.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-qt-systems-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qt-systems-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qt-systems-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-ui-toolkit-0.9-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-ui-toolkit
    echo Deps     : liblomiri-qt-systems, qt5-mobility-pim, qt5-mobility-feedback
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri-ui-toolkit/lomiri-ui-toolkit.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-ui-toolkit/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri-ui-toolkit.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri-ui-toolkit.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-ui-toolkit-0.9-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-ui-toolkit-0.9-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-ui-toolkit-0.9-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-telefon-service-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-telefon-service
    echo Deps     : qt5-mobility-pim, qt5-mobility-feedback
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-telefon-service/liblomiri-telefon-service.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-telefon-service/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-telefon-service.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-telefon-service.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-telefon-service-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-telefon-service-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-telefon-service-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-qt-dbus-test-runner-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-qt-dbus-test-runner
    echo Deps     : liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri-qt-dbus-test-runner/lomiri-qt-dbus-test-runner.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-qt-dbus-test-runner/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri-qt-dbus-test-runner.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri-qt-dbus-test-runner.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-qt-dbus-test-runner-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-qt-dbus-test-runner-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-qt-dbus-test-runner-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-cpp-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-cpp
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-cpp/liblomiri-cpp.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-cpp/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-cpp.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-cpp.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-cpp-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-cpp-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-cpp-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-geonames-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-geonames
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-geonames/liblomiri-geonames.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-geonames/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-geonames.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-geonames.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-geonames-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-geonames-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-geonames-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-qmenumodel-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qmenumodel
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-qmenumodel/liblomiri-qmenumodel.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qmenumodel/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-qmenumodel.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-qmenumodel.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-qmenumodel-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qmenumodel-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qmenumodel-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-click-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-click
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-click/liblomiri-click.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-click/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-click.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-click.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-click-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-click-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-click-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-dbus-test-0.9-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-dbus-test
    echo Deps     : 
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-dbus-test/liblomiri-dbus-test.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-dbus-test/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-dbus-test.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-dbus-test.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-dbus-test-0.9-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-dbus-test-0.9-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-dbus-test-0.9-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-system-compositor-0.9-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-system-compositor
    echo Deps     : liblomiri-deviceinfo
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri-system-compositor/lomiri-system-compositor.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-system-compositor/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri-system-compositor.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri-system-compositor.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-system-compositor-0.9-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-system-compositor-0.9-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-system-compositor-0.9-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-thumbnailer-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-thumbnailer
    echo Deps     : liblomiri-api, liblomiri-cpp, liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-thumbnailer/liblomiri-thumbnailer.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-thumbnailer/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-thumbnailer.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-thumbnailer.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-thumbnailer-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-thumbnailer-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-thumbnailer-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-app-launch-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-app-launch
    echo Deps     : liblomiri-cpp, liblomiri-click, liblomiri-upstart, liblomiri-dbus-test, liblomiri-libertine
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-app-launch/liblomiri-app-launch.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-app-launch/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-app-launch.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-app-launch.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-app-launch-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-app-launch-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-app-launch-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-dbus-cpp-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-dbus-cpp
    echo Deps     : liblomiri-cpp
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-dbus-cpp/liblomiri-dbus-cpp.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-dbus-cpp/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-dbus-cpp.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-dbus-cpp.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-dbus-cpp-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-dbus-cpp-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-dbus-cpp-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-biometryd-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-biometryd
    echo Deps     : liblomiri-dbus-cpp, liblomiri-cpp
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri-biometryd/lomiri-biometryd.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-biometryd/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri-biometryd.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri-biometryd.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-biometryd-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-biometryd-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-biometryd-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-messing-menu-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-messing-menu
    echo Deps     : liblomiri-dbus-test
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-messing-menu/liblomiri-messing-menu.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-messing-menu/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-messing-menu.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-messing-menu.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-messing-menu-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-messing-menu-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-messing-menu-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-history-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-history
    echo Deps     : qt5-mobility-pim
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-history/liblomiri-history.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-history/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-history.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-history.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-history-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-history-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-history-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-gmenuharness-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-gmenuharness
    echo Deps     : liblomiri-api, liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-gmenuharness/liblomiri-gmenuharness.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-gmenuharness/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-gmenuharness.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-gmenuharness.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-gmenuharness-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-gmenuharness-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-gmenuharness-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-content-hub-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-content-hub
    echo Deps     : liblomiri-cpp, lomiri-ui-toolkit, liblomiri-app-launch, liblomiri-libertine, liblomiri-download-manager
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-content-hub/liblomiri-content-hub.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-content-hub/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-content-hub.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-content-hub.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-content-hub-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-content-hub-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-content-hub-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-usermetrics-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-usermetrics
    echo Deps     : liblomiri-click, liblomiri-qt-dbus-test
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-usermetrics/liblomiri-usermetrics.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-usermetrics/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-usermetrics.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-usermetrics.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-usermetrics-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-usermetrics-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-usermetrics-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-biometryd-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-biometryd
    echo Deps     : liblomiri-dbus-cpp, liblomiri-cpp
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-biometryd/liblomiri-biometryd.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-biometryd/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-biometryd.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-biometryd.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-biometryd-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-biometryd-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-biometryd-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-url-dispatcher-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-url-dispatcher
    echo Deps     : liblomiri-app-launch, liblomiri-dbus-test, liblomiri-click
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-url-dispatcher/liblomiri-url-dispatcher.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-url-dispatcher/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-url-dispatcher.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-url-dispatcher.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-url-dispatcher-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-url-dispatcher-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-url-dispatcher-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-connectivity-qt-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-connectivity-qt
    echo Deps     : liblomiri-api, liblomiri-url-dispatcher, liblomiri-qt-dbus-test, liblomiri-qt-dbusmock, liblomiri-gmenuharness
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-connectivity-qt/liblomiri-connectivity-qt.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-connectivity-qt/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-connectivity-qt.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-connectivity-qt.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-connectivity-qt-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-connectivity-qt-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-connectivity-qt-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-qml-settings-components-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri-qml-settings-components
    echo Deps     : liblomiri-thumbnailer
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri-qml-settings-components/lomiri-qml-settings-components.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri-qml-settings-components/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri-qml-settings-components.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri-qml-settings-components.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-qml-settings-components-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-qml-settings-components-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-qml-settings-components-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/lomiri-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : lomiri
    echo Deps     : lomiri-dbus-test-runner, liblomiri-cpp, liblomiri-deviceinfo, liblomiri-api, liblomiri-geonames, liblomiri-qmenumodel, liblomiri-app-launch, lomiri-ui-toolkit, liblomiri-qt-dbus-test, liblomiri-qt-dbusmock, liblomiri-usermetrics, liblomiri-download-manager, liblomiri-connectivity-qt
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/lomiri/lomiri.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/lomiri/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/lomiri.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/lomiri.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/lomiri-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/lomiri-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

if [ ! -e "$RPMBUILD_DIR/RPMS/$TARCH/liblomiri-qtmir-1.0-1.$TVERSION.$TARCH.rpm" ]; then
    echo ---------------------------------------------------------------
    echo Building : liblomiri-qtmir
    echo Deps     : liblomiri-cpp, liblomiri-app-launch, liblomiri-url-dispatcher, liblomiri-qt-dbus-test, liblomiri-qt-dbusmock, liblomiri-api, liblomiri-content-hub
    echo ---------------------------------------------------------------
    createresultdir
    cp $GIT_CHECKOUT/liblomiri-qtmir/liblomiri-qtmir.spec  $RPMBUILD_DIR/SPECS
    cp $GIT_CHECKOUT/liblomiri-qtmir/*.patch $RPMBUILD_DIR/SOURCES
    spectool -g -R $RPMBUILD_DIR/SPECS/liblomiri-qtmir.spec
    rpmbuild -bs $RPMBUILD_DIR/SPECS/liblomiri-qtmir.spec
    mock -r fedora-$TVERSION_NO-$TARCH --localrepo=$RESULTDIR --addrepo=file://$RPMBUILD_DIR/RPMS/$TARCH --chain $RPMBUILD_DIR/SRPMS/liblomiri-qtmir-1.0-1.fc32.src.rpm
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qtmir-1.0-1.$FEDORA_VERSION/*.src.rpm    $RPMBUILD_DIR/SRPMS/
    cp $RESULTDIR/results/fedora-$TVERSION_NO-$TARCH/liblomiri-qtmir-1.0-1.$FEDORA_VERSION/*.$TARCH.rpm $RPMBUILD_DIR/RPMS/$TARCH
fi

