#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial https://github.com/ubports/libqtdbusmock.git
cd libqtdbusmock
cat $SRC/0001-Compiling-on-fedora.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "Compiling-on-fedora"
cat $SRC/0002-Changing-CMakeLists.txt-files-to-compiling-on-fedora.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "Changing-CMakeLists.txt-files-to-compiling-on-fedora"
