Name:       liblomiri-qt-dbusmock
Version:    1.0
Release:    1%{?dist}
Summary:    Library for mocking DBus interactions using Qt
License:    FIXME
URL:        https://github.com/ubports/libqtdbusmock
Source0:    https://github.com/ubports/libqtdbusmock/archive/1e9fd84aa31c7b858b90e2bbbdcadf081549fa65/libqtdbusmock.tar.gz
Patch0:     0001-Compiling-on-fedora.patch
Patch1:     0002-Changing-CMakeLists.txt-files-to-compiling-on-fedora.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: pkgconfig(libqtdbustest-1)
BuildRequires: pkgconfig(libnm)
BuildRequires: pkgconfig(gmock)
BuildRequires: pkgconfig(gtest)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
Library for mocking DBus interactions using Qt
A simple library for mocking DBus services with a Qt API
.
This package contains the shared libraries.
Library for mocking DBus interactions using Qt
A simple library for mocking DBus services with a Qt API
.
This package contains extra dbusmock templates.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n libqtdbusmock-1e9fd84aa31c7b858b90e2bbbdcadf081549fa65

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}/usr/share/libqtdbusmock/templates
cp src/libqtdbusmock/DBusMock.h                              %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp src/libqtdbusmock/DeclareMetatypes.h                      %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp src/libqtdbusmock/ExportInterfaces.h                      %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp src/libqtdbusmock/Method.h                                %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp src/libqtdbusmock/MethodCall.h                            %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/MockInterface.h                   %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp src/libqtdbusmock/MockInterfaceClasses.h                  %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp src/libqtdbusmock/NamedMethodCall.h                       %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/NetworkManagerMockInterface.h     %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/NotificationDaemonMockInterface.h %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/OfonoConnectionManagerInterface.h %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/OfonoMockInterface.h              %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/OfonoModemInterface.h             %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/OfonoNetworkRegistrationInterface.h %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/OfonoSimManagerInterface.h        %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/PropertiesInterface.h             %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/URfkillDeviceInterface.h          %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/URfkillInterface.h                %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/URfkillKillswitchInterface.h      %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
cp build/src/libqtdbusmock/config.h                          %{buildroot}%{_includedir}/libqtdbusmock-1/libqtdbusmock
ln -s libqtdbusmock.so.1 %{buildroot}%{_libdir}/libqtdbusmock.so
ln -s libqtdbusmock.so.1.0.0 %{buildroot}%{_libdir}/libqtdbusmock.so.1
install -p -m 755 build/src/libqtdbusmock/libqtdbusmock.so.1.0.0 %{buildroot}%{_libdir}
cp build/src/libqtdbusmock/libqtdbusmock-1.pc                %{buildroot}%{_libdir}/pkgconfig
cp templates/org.freedesktop.hostname1.py                    %{buildroot}/usr/share/libqtdbusmock/templates
cp templates/org.freedesktop.login1.py                       %{buildroot}/usr/share/libqtdbusmock/templates

%files
%{_libdir}/libqtdbusmock.so.1
%{_libdir}/libqtdbusmock.so.1.0.0
/usr/share/libqtdbusmock/templates/org.freedesktop.hostname1.py
/usr/share/libqtdbusmock/templates/org.freedesktop.login1.py

%files devel
%{_includedir}/libqtdbusmock-1/libqtdbusmock/DBusMock.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/DeclareMetatypes.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/ExportInterfaces.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/Method.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/MethodCall.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/MockInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/MockInterfaceClasses.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/NamedMethodCall.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/NetworkManagerMockInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/NotificationDaemonMockInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/OfonoConnectionManagerInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/OfonoMockInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/OfonoModemInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/OfonoNetworkRegistrationInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/OfonoSimManagerInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/PropertiesInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/URfkillDeviceInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/URfkillInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/URfkillKillswitchInterface.h
%{_includedir}/libqtdbusmock-1/libqtdbusmock/config.h
%{_libdir}/libqtdbusmock.so
%{_libdir}/pkgconfig/libqtdbusmock-1.pc
