Name:       liblomiri-usermetrics
Version:    1.0
Release:    1%{?dist}
Summary:    library for retrieving anonymous metrics about users
License:    FIXME
URL:        https://github.com/ubports/libusermetrics
Source0:    https://github.com/ubports/libusermetrics/archive/8e28611ae8b2f392fbf4409f67b3fd0ca40827c8/libusermetrics.tar.gz
Patch0:     0001-disable-everything-except-libs.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: liblomiri-click-devel
BuildRequires: liblomiri-qt-dbus-test-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: qt5-qtxmlpatterns-devel
BuildRequires: pkgconfig(gsettings-qt)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(click-0.4)
BuildRequires: pkgconfig(libqtdbustest-1)

Requires: %{name}%{?_isa} = %{version}-%{release}
%description
library for retrieving anonymous metrics about users
This package contains shared libraries to be used by applications.
library for exporting anonymous metrics about users
This package contains shared libraries to be used by applications.

%package devel
Summary: Development files for %{name}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -p1 -n libusermetrics-8e28611ae8b2f392fbf4409f67b3fd0ca40827c8

%build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX:PATH=/usr \
      -DCMAKE_C_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
      -DCMAKE_CXX_FLAGS_RELEASE:STRING="-Og -g3 -DNDEBUG" \
 -DENABLE_TESTS=OFF ..
make -O -j4 V=1 VERBOSE=1
cd ..

%install
mkdir -p %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsinput
mkdir -p %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsoutput
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cp src/libusermetricsinput/Metric.h                          %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsinput
cp src/libusermetricsinput/MetricManager.h                   %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsinput
cp src/libusermetricsinput/MetricUpdate.h                    %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsinput
cp build/include/libusermetricsinput/UserMetricsInputExport.h %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsinput
cp src/libusermetricsinput/usermetricsinput.h                %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsinput
cp src/libusermetricsoutput/ColorTheme.h                     %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsoutput
cp src/libusermetricsoutput/UserMetrics.h                    %{buildroot}%{_includedir}/libusermetrics-1/libusermetricsoutput
ln -s libusermetricsinput.so.1 %{buildroot}%{_libdir}/libusermetricsinput.so
ln -s libusermetricsinput.so.1.0.0 %{buildroot}%{_libdir}/libusermetricsinput.so.1
install -p -m 755 build/src/libusermetricsinput/libusermetricsinput.so.1.0.0 %{buildroot}%{_libdir}
ln -s libusermetricsoutput.so.1 %{buildroot}%{_libdir}/libusermetricsoutput.so
ln -s libusermetricsoutput.so.1.0.0 %{buildroot}%{_libdir}/libusermetricsoutput.so.1
install -p -m 755 build/src/libusermetricsoutput/libusermetricsoutput.so.1.0.0 %{buildroot}%{_libdir}
cp build/src/libusermetricsinput/libusermetricsinput-1.pc    %{buildroot}%{_libdir}/pkgconfig
cp build/src/libusermetricsoutput/libusermetricsoutput-1.pc  %{buildroot}%{_libdir}/pkgconfig

%files
%{_libdir}/libusermetricsinput.so.1
%{_libdir}/libusermetricsinput.so.1.0.0
%{_libdir}/libusermetricsoutput.so.1
%{_libdir}/libusermetricsoutput.so.1.0.0

%files devel
%{_includedir}/libusermetrics-1/libusermetricsinput/Metric.h
%{_includedir}/libusermetrics-1/libusermetricsinput/MetricManager.h
%{_includedir}/libusermetrics-1/libusermetricsinput/MetricUpdate.h
%{_includedir}/libusermetrics-1/libusermetricsinput/UserMetricsInputExport.h
%{_includedir}/libusermetrics-1/libusermetricsinput/usermetricsinput.h
%{_includedir}/libusermetrics-1/libusermetricsoutput/ColorTheme.h
%{_includedir}/libusermetrics-1/libusermetricsoutput/UserMetrics.h
%{_libdir}/libusermetricsinput.so
%{_libdir}/libusermetricsoutput.so
%{_libdir}/pkgconfig/libusermetricsinput-1.pc
%{_libdir}/pkgconfig/libusermetricsoutput-1.pc
