#!/bin/bash
set -e
source ../settings/env
export SRC=$PWD
cd $CHECKOUTDIR
git clone --single-branch --branch xenial_-_edge_-_wayland_-_x11 https://github.com/ubports/qtmir.git
cd qtmir
cat $SRC/0001-adding-fixes.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "adding-fixes"
cat $SRC/0002-add-pthreads-to-qpa-mirserver.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "add-pthreads-to-qpa-mirserver"
cat $SRC/0003-attempt-to-fix-missing-context.patch | /usr/bin/patch -p1 -s --fuzz=0 --no-backup-if-mismatch
git add .
git commit -m "attempt-to-fix-missing-context"
